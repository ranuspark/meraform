package com.spark.meraform.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.spark.meraform.Activity.DashboardActivity12;
import com.spark.meraform.Activity.HomeActivity;
import com.spark.meraform.Activity.RecordEditorActivity;
import com.spark.meraform.Adapter.AlertAdapter;
import com.spark.meraform.Adapter.AlertAdapter1;
import com.spark.meraform.Adapter.ImageAdapter;
import com.spark.meraform.Model.DynamicDTO;
import com.spark.meraform.Model.Form;
import com.spark.meraform.Model.SpinerDTO;
import com.spark.meraform.R;
import com.spark.meraform.Utility.JSONUtils;
import com.spark.meraform.database.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class RecordListFragment extends Fragment implements View.OnClickListener ,LoaderManager.LoaderCallbacks<Cursor>{
    FloatingActionButton fab;
    RecyclerView recyclerView;
    DatabaseHandler db;
    String proId;
    ArrayList<Form> form_list=new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home, container, false);

        fab= view.findViewById(R.id.fab_btn);
        recyclerView= view.findViewById(R.id.rv);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        });

        fab.setOnClickListener(this);
        db=new DatabaseHandler(getActivity());
        Bundle bundle=getArguments();
        proId = bundle.getString("SelectProId",null);
        Log.e("ProId", proId);


        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fab_btn:
                Intent intent =new Intent(getActivity(),RecordEditorActivity.class);
                intent.putExtra("pro_id", proId);
                startActivity(intent);
          Cursor c = db.fetchFormSt(proId);
          getForm(c);

        }

    }


    public void getForm(Cursor cursor) {
        String form_json = null;
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {
                //Project_detail pp=new Project_detail();
                cursor.getString(0);
                String pro_id = cursor.getString(1);
                cursor.getString(2);
                form_json = cursor.getString(3);
                String created_at = cursor.getString(4);
                cursor.getString(5);

                Log.e("Fetch Form", cursor.getString(0) + " " + cursor.getString(1) + " " + cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4) + " " + cursor.getString(5));
            }

        }

        Form form=new Form();
        try {
            JSONArray jsonArray=new JSONArray(form_json);
            for(int i=0;i<jsonArray.length();i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                int controlId = jsonObject.getInt("ControlId");
                String ControlType = jsonObject.getString("ControlType");
                String IsMandatory = jsonObject.getString("IsMandatory");
                String ValidationName = jsonObject.getString("ValidationName");
                String DataType = jsonObject.getString("DataType");
                String Regex = jsonObject.getString("Regex");
                String ValidationMsg = jsonObject.getString("ValidationMessage");
                String FieldName = jsonObject.getString("FieldName");
                String LableName = jsonObject.getString("LableName");
                String DataList = jsonObject.getString("DataList");


                form.setControl_Id(controlId);
                form.setControlType(ControlType);
                form.setIsMandatory(IsMandatory);

                form.setDataType(DataType);
                form.setRegex(Regex);
                form.setValidationMsg(ValidationMsg);

                form.setFieldName(FieldName);
                form.setLableName(LableName);
                form.setDataList1(DataList);

                form_list.add(form);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    @NonNull
    @Override
    public Loader onCreateLoader(int i, @Nullable Bundle bundle) {

        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {

    }



    @Override
    public void onLoaderReset(@NonNull Loader loader) {

    }
}
