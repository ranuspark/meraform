package com.spark.meraform.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.spark.meraform.Activity.HomeActivity;
import com.spark.meraform.Model.Project_detail;
import com.spark.meraform.Model.SpinerDTO;
import com.spark.meraform.R;
import com.spark.meraform.database.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

public class FormPickerFragment extends Fragment {
    RecyclerView recyclerView;
    FormPickerAdapter formPickerAdapter;
     ArrayList<Project_detail>homelist=new ArrayList<>();
    private FormPickerFragmentListener mListener;


    public interface FormPickerFragmentListener {
        void onFormSelected(long j);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.drawer_form_picker, container, false);
        recyclerView = view.findViewById(R.id.rv);
       // homelist.add("abc");




//                Bundle args1 = getArguments();
//                if(args1!=null) {
//                    homelist = (ArrayList<Project_detail>) args1.getSerializable("project_list");
//                }


//        Project_detail project_detail=new Project_detail();
//        project_detail.setPro_id("1");
//        project_detail.setPro_name("Project 1");
//        project_detail.setPro_des("Description of project");
//        project_detail.setCount(2);
//        project_detail.setPro_create("17-01-2019");
//        homelist.add(project_detail);

        DatabaseHandler db=new DatabaseHandler(getActivity());
        homelist=  db.FetchProject();


        formPickerAdapter = new FormPickerAdapter(homelist,getActivity());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL);

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.setAdapter(formPickerAdapter);
        recyclerView.addItemDecoration(dividerItemDecoration);


        return view;


    }
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FormPickerFragmentListener) {
            this.mListener = (FormPickerFragmentListener) context;
            return;
        }
        throw new RuntimeException(context.toString() + " must implement " + FormPickerFragmentListener.class.getSimpleName());
    }
    public class FormPickerAdapter extends  RecyclerView.Adapter<FormPickerAdapter.MyViewHolder> {

        private List<Project_detail> lessonsList;
        private Activity activity;

        public FormPickerAdapter(ArrayList<Project_detail> lessonsList, Activity activity) {
            this.lessonsList = lessonsList;
            this.activity = activity;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adap_drawpi1, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
            Project_detail sdto = lessonsList.get(i);
            //  holder.courseName.setText(sdto.getCourseName());
            // holder.lessonName.setText(homeLastSeenLesson.getLessonName());
            // holder.itemView.setTag(homeLastSeenLesson.getLessonId());
            // Picasso.get ().load ( homeLastSeenLesson.getLessonImage () ).into ( holder.lessonImage );
          //  holder.titleName.setText(sdto.getCheckbox_text());
           // holder.tv_act.setText("last updated" + " " + "12:00Am");

            holder.project_name.setText(sdto.getPro_name());
            holder.pro_des.setVisibility(View.GONE);
            holder.pro_count.setVisibility(View.GONE);

            //   holder.pro_des.setText(sdto.getPro_des());
          //  holder.pro_count.setText(""+sdto.getCount());

            holder.project_name.setOnClickListener(v -> {

                mListener.onFormSelected(Long.parseLong(sdto.getPro_id()));

            });

        }




        @Override
        public int getItemCount() {
            return lessonsList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView project_name;
            public TextView pro_count;
            public TextView pro_des;
            ImageView Iv;

            //   public TextView courseName;

            public MyViewHolder(View view) {
                super(view);
                project_name = view.findViewById(R.id.tv_proname);
                pro_count = view.findViewById(R.id.tv_count);
                pro_des = view.findViewById(R.id.tv_des);
                Iv = view.findViewById(R.id.iv);



            }

        }

    }




}


