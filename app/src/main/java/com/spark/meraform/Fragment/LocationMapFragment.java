package com.spark.meraform.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.spark.meraform.R;

import static android.content.Context.LOCATION_SERVICE;

public class LocationMapFragment extends Fragment implements LocationListener ,OnMapReadyCallback{
    Toolbar mToolbar;
    GoogleMap googleMap;

    @SuppressLint("MissingPermission")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (!isGooglePlayServicesAvailable()) {
            getActivity().finish();
        }
        View view = inflater.inflate(R.layout.map_activity, container, false);
        //  this.mToolbar = view.findViewById(R.id.toolbar);


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);

//        googleMap = mapFragment.getMap();
//        googleMap.setMyLocationEnabled(true);
//        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        String bestProvider = locationManager.getBestProvider(criteria, true);
//
//        @SuppressLint("MissingPermission") Location location = locationManager.getLastKnownLocation(bestProvider);
//        if (location != null) {
//            onLocationChanged(location);
//        }
//        locationManager.requestLocationUpdates(bestProvider, 20000, 0, (LocationListener) getActivity());
        return view;

    }

    public void onLocationChanged(Location location) {
       // TextView locationTv = (TextView) findViewById(R.id.latlongLocation);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        Log.e("Location","Latitude:" + latitude + ", Longitude:" + longitude );
       // locationTv.setText("Latitude:" + latitude + ", Longitude:" + longitude);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }


   @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap = googleMap;
        googleMap.clear();
        Double lat = 28.595184;
        Double lng = 77.315566;
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng points = new LatLng(lat, lng);
//        googleMap.addMarker(new MarkerOptions().position(points).title("Domino's"));
//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(points));

       // googleMap.setMyLocationEnabled(true);
        Marker ham = googleMap.addMarker(new MarkerOptions().position(points).title("Current Location"));
//        googleMap.addMarker(new MarkerOptions()
//                .position(new LatLng(lat,lng))
//                .title("Facebook")
//                .snippet("Facebook HQ: Menlo Park"));

        ham.showInfoWindow();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(points,18.0F));
    }

}
