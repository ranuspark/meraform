package com.spark.meraform.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.spark.meraform.Activity.DashboardActivity12;
import com.spark.meraform.Activity.HomeActivity;
import com.spark.meraform.Adapter.AlertAdapter;
import com.spark.meraform.Adapter.AlertAdapter1;
import com.spark.meraform.Adapter.ImageAdapter;
import com.spark.meraform.Api.Client;
import com.spark.meraform.Model.DynamicDTO;
import com.spark.meraform.Model.Form;
import com.spark.meraform.Model.Project_detail;
import com.spark.meraform.Model.SpinerDTO;
import com.spark.meraform.R;
import com.spark.meraform.Utility.JSONUtils;
import com.spark.meraform.Utility.SharedPreferencesAdapter;
import com.spark.meraform.Utility.UserSettings;
import com.spark.meraform.database.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.spark.meraform.Utility.UserSettings.GetCurrentDateTime;

public class RecordEditorFragment extends Fragment {
    DatabaseHandler db;
    ArrayList<Form>form_list=new ArrayList<>();
    LinearLayout parentLayout,LinearLay;
    ArrayList<DynamicDTO> dynamicList;
    ArrayList<String> smsArrayList;
   // List<String> couponDenoList;
    ArrayList<Form> Datalist;
    int data_Id;
    DynamicDTO info ;
    ;
    ArrayList<SpinerDTO> spinnerList=new ArrayList<>();
    ArrayList<SpinerDTO> checkboxList=new ArrayList<>();
    ArrayList<SpinerDTO> radiobtnList=new ArrayList<>();
    ArrayList<SpinerDTO> imageList=new ArrayList<>();

    JsonObject json ;
    JSONArray JSONArray = new JSONArray();
    JSONObject JSON11 =new JSONObject();

    RadioGroup radioGroup;
    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_REQUEST = 2;
    ImageView dynamic_Image;
    LinearLayout LL_horizo;
    int image_id;
    Context context;
    ImageAdapter imageAdapter;
    //  Bitmap myphoto ;
     RecyclerView recyclerView;
    EditText dynamic_editText11,dynamic_editText12;
    private Toolbar mToolbar;
    private String pro_id,form_id,form_name;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_activity1, container, false);
        this.mToolbar = view.findViewById(R.id.toolbar);
     // setSupportActionBar(this.mToolbar);
     /*   this.mToolbar.setNavigationIcon(R.drawable.ic_chevron_left);
        this.mToolbar.setTitle("Mera Form");
        this.mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),HomeActivity.class));
                getActivity().finish();
            }
        }); */
        //this.mToolbar.setNavigationContentDescription("Mera Form");
        Bundle bundle=getArguments();
        pro_id = bundle.getString("SelectProId",null);
       Log.e("ProIIII", pro_id);

        db=new DatabaseHandler(getActivity());
        parentLayout = view.findViewById(R.id.linear_layout);

        ScrollView sv = new ScrollView(getActivity());
        sv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        LinearLay = new LinearLayout(getActivity());
        LinearLay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        LinearLay.setOrientation(LinearLayout.VERTICAL);

        sv.addView(LinearLay);
        parentLayout.addView(sv);
        //CreateView1();
      //  LinearLay.addView(submitButton());


        Cursor c = db.fetchFormSt(pro_id);
        getForm(c);



        return view;
    }


    public void getForm(Cursor cursor) {
        String form_json = null;
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {
                //Project_detail pp=new Project_detail();
                cursor.getString(0);
                String pro_id = cursor.getString(1);
                 form_id =cursor.getString(2);
                 form_name= cursor.getString(3);
                form_json = cursor.getString(4);
                String created_at = cursor.getString(5);
                cursor.getString(6);

                Log.e("Fetch Form", cursor.getString(0) + " " + cursor.getString(1) + " " + cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4) + " " + cursor.getString(5));
            }

        }
        dynamicList = new ArrayList<DynamicDTO>();

if(form_json!=null) {
    try {
         Log.e("FormData", form_json);
        JSONArray jsonArray = new JSONArray(form_json);

        for (int i = 0; i < jsonArray.length(); i++) {
            info = new DynamicDTO();

            JSONObject jsonObject = jsonArray.getJSONObject(i);

            int ControlId = jsonObject.getInt("ControlId");
            String ControlType = jsonObject.getString("ControlType");
            String IsMandatory = jsonObject.getString("IsMandatory");
            String ValidationName = jsonObject.getString("ValidationName");
            String DataType = jsonObject.getString("DataType");
            String Regex = jsonObject.getString("Regex");
            String ValidationMsg = jsonObject.getString("ValidationMessage");
            String FieldName = jsonObject.getString("FieldName");
            String LableName = jsonObject.getString("LableName");
            String DataList = jsonObject.getString("DataList");


            info.setControId(ControlId);
            info.setControlType(ControlType);
            info.setIsMandatory(IsMandatory);
            info.setValidationName(ValidationName);
            info.setRegex(Regex);
            info.setValidationMessage(ValidationMsg);
            info.setFieldName(FieldName);
            info.setDataList(DataList);




            //  couponDenoList = Arrays.asList(DataList.split("\\s*,\\s*"));


                if (ControlType.equalsIgnoreCase("textbox")) {

                    dynamic_textView(LableName);
                   EditText et1= dynamic_editText(ControlId);
                  // Log.e("AAAAA",et1.getText().toString());

                   info.setEditText(et1);


                } else if (ControlType.equalsIgnoreCase("dropdown")) {
                    Log.e("Abc", DataList);

                    Datalist = new ArrayList<>();
                    //  couponDenoList = Arrays.asList(DataList.split("\\s*,\\s*"));
                    if(!DataList.equals("[]")) {
                        JSONArray jj_array = new JSONArray(DataList);

                        for (int j = 0; j < jj_array.length(); j++) {
                            JSONObject jj_object = jj_array.getJSONObject(j);

                            int data_Id = jj_object.getInt("id");
                            String data_Val = jj_object.getString("item");


                            Form form = new Form();
                            form.setData_id(data_Id);
                            form.setData_val(data_Val);
                            Datalist.add(form);


                        }
                        dynamic_textView(LableName);

                        dynamic_spiner(ControlId, Datalist);

                    }
                } else if (ControlType.equalsIgnoreCase("checkbox")) {

                    Log.e("Pqr", DataList);
                    if(!DataList.equals("[]")) {
                        Datalist = new ArrayList<>();
                        JSONArray jj_array = new JSONArray(DataList);

                        for (int j = 0; j < jj_array.length(); j++) {
                            JSONObject jj_object = jj_array.getJSONObject(j);

                            int data_Id = jj_object.getInt("id");
                            String data_Val = jj_object.getString("item");

                            Log.e("Data", data_Id + " " + data_Val);

                            Form form = new Form();
                            form.setData_id(data_Id);
                            form.setData_val(data_Val);
                            Datalist.add(form);


                        }
                        dynamic_textView(LableName);
                        dynamic_checkbox(ControlId, Datalist);
                    }
                    //  couponDenoList = Arrays.asList(DataList.split("\\s*,\\s*"));



                } else if (ControlType.equalsIgnoreCase("radio_btn")) {

                    Log.e("Xyz", DataList);
                    if(!DataList.equals("[]")) {
                        Datalist = new ArrayList<>();
                        //creating dynamic edittext
                        // couponDenoList = Arrays.asList(DataList.split("\\s*,\\s*"));
                        JSONArray jj_array =  new JSONArray(DataList);

                        for (int j = 0; j < jj_array.length(); j++) {
                            JSONObject jj_object = jj_array.getJSONObject(j);

                            int data_Id = jj_object.getInt("id");
                            String data_Val = jj_object.getString("item");

                            Log.e("Data", data_Id + " " + data_Val);

                            Form form = new Form();
                            form.setData_id(data_Id);
                            form.setData_val(data_Val);
                            Datalist.add(form);



                        }
                        dynamic_textView(LableName);
                        dynamic_radiobutton(ControlId, Datalist);
                    }

                } else if (ControlType.equalsIgnoreCase("image")) {

                    dynamic_textView(LableName);
                    dynamic_image(ControlId);

                } else if (ControlType.equalsIgnoreCase("button")) {

                    dynamic_button(LableName);
                    // dynamic_image(ControlId);

                }
                Log.e("Dyanamic List Size111", String.valueOf(dynamicList.size()));
            dynamicList.add(info);


        }

        // LinearLay.addView(submitButton());
    } catch (JSONException e) {
        e.printStackTrace();
    }
}

    }

    private EditText dynamic_editText(int id) {

        final EditText dynamic_editText1 = new EditText(getActivity());
        dynamic_editText1.setId(id);
        LinearLayout.LayoutParams ss= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ss.setMargins(5, 5, 5, 5);
        dynamic_editText1.setLayoutParams(ss);
        info.setEditText(dynamic_editText1);
        dynamic_editText1.setPadding(16, 16, 16, 16);
        dynamic_editText1.setInputType ( InputType.TYPE_CLASS_NUMBER );
        dynamic_editText1.setBackgroundResource(R.drawable.rounded_edittext);
      //  dynamicList.add(info);
        Log.e("Id", String.valueOf(id));
        Log.e("Id12Val", dynamic_editText1.getText().toString());
        LinearLay.addView(dynamic_editText1);
        return dynamic_editText1;
    }

    // Return TextView
    private TextView dynamic_textView(String title) {
        TextView dynamic_textView1 = new TextView(getActivity());
        dynamic_textView1.setText(Html.fromHtml(title));
        dynamic_textView1.setTextColor(Color.parseColor("#000000"));
        dynamic_textView1.setTypeface(null, Typeface.BOLD);
        dynamic_textView1.setPadding(10, 10, 10, 10);
        LinearLay.addView(dynamic_textView1);
        return dynamic_textView1;
    }

    private Button dynamic_button(String title) {

        Button button = new Button(getActivity());
        button.setHeight(WRAP_CONTENT);
        button.setText(title);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(40, 20, 40, 5);
        button.setLayoutParams(params);
        button.setBackgroundResource(R.drawable.rounded);
        button.setOnClickListener(submitListener);

        LinearLay.addView(button);
        return button;
    }

    // Return Spinner
  //  private Spinner dynamic_spiner(int id, List<String> couponlist) {
      private Spinner dynamic_spiner(int id, ArrayList<Form> DataList) {

        Spinner dynamic_spinner1 = new Spinner(getActivity());
        dynamic_spinner1.setId(id);
        LinearLayout.LayoutParams ll= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ll.setMargins(10, 10, 10, 10);

        dynamic_spinner1.setLayoutParams(ll);
        dynamic_spinner1.setPadding(5, 5, 5, 5);
        dynamic_spinner1.setBackgroundResource(R.drawable.rounded_edittext);
        getCouponDenoListAdapter(dynamic_spinner1, DataList, id);
        info.setSpinner(dynamic_spinner1);
        LinearLay.addView(dynamic_spinner1);

        return dynamic_spinner1;
    }

    //private  void dynamic_checkbox(int id,List<String> couponlist)
    private  void dynamic_checkbox(int id,ArrayList<Form> datalist)
    {
        dynamic_editText12 = new EditText(getActivity());
        dynamic_editText12.setId(id);
        info.setEditText(dynamic_editText12);
        dynamic_editText12.setPadding(16, 16, 16, 16);
        dynamic_editText12.setFocusable(false);
        dynamic_editText12.setInputType(0);
        dynamic_editText12.setBackgroundResource(R.drawable.rounded_edittext);

        dynamic_editText12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dynamic_checkbox1(id,datalist);
            }
        });
        LinearLay.addView(dynamic_editText12);
    }
    private  void dynamic_checkbox1(int id,ArrayList<Form> datalist)
    {

       // final ArrayList<Integer> selectedList = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("List of Items");

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alertrec, null);
        builder.setView(alertLayout);
        RecyclerView recyclerView=alertLayout.findViewById(R.id.rv);
        LinearLayoutManager liLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(liLayoutManager);
        AlertAdapter alertAdapter=new AlertAdapter(datalist, getActivity());

        recyclerView.setAdapter(alertAdapter);
        //StringBuffer nn = new StringBuffer();
        builder.setPositiveButton("DONE", (dialogInterface, i) -> {

            StringBuffer text = alertAdapter.getSelectedItem();


            //   Log.e("CheckValue", String.valueOf(text));

           SharedPreferences sp=getActivity().getSharedPreferences("Form", 0);
            SharedPreferences.Editor editor=sp.edit();
            editor.putString("checkbox", String.valueOf(text));
            editor.commit();

            dynamic_editText12.setText(text);

            // For clear data from shareprefence
            sp=getActivity().getSharedPreferences("Form", 0);
            editor=sp.edit();
           editor.clear();
           editor.commit();

            SpinerDTO spinerInfo = new SpinerDTO();
            spinerInfo.setCheckbox_id(id);
            spinerInfo.setCheckbox_text(String.valueOf(text));
            checkboxList.add(spinerInfo);

        });

        builder.show();

    }
    private  EditText dynamic_radiobutton(int id,ArrayList<Form> datalist)
    {
        dynamic_editText11 = new EditText(getActivity());
        dynamic_editText11.setId(id);
        info.setEditText(dynamic_editText11);
        dynamic_editText11.setPadding(16, 16, 16, 16);
        dynamic_editText11.setFocusable(false);
        dynamic_editText11.setInputType(0);

        dynamic_editText11.setBackgroundResource(R.drawable.rounded_edittext);


        dynamic_editText11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Dynamic_Radio(id,datalist);

                dynamic_radiobutton1(id,datalist);
            }
        });
        LinearLay.addView(dynamic_editText11);
        return dynamic_editText11;
    }

/*    private void Dynamic_Radio(int id,ArrayList<Form> datalist) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Radio Button");

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alertrec1, null);
        builder.setView(alertLayout);

        RadioGroup rg = (RadioGroup) alertLayout.findViewById(R.id.radio_group);

        for(int i=0;i<datalist.size();i++){
            RadioButton rb=new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
            rb.setText(datalist.get(i).getData_val());
            rg.addView(rb);
        }
        rg.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = group.findViewById(checkedId);
            if (null != rb && checkedId > -1) {
                Toast.makeText(getActivity(), rb.getText(), Toast.LENGTH_SHORT).show();

                SpinerDTO spinerInfo = new SpinerDTO();
                spinerInfo.setRadio_id(id);
                spinerInfo.setRadio_text(rb.getText().toString());
                radiobtnList.add(spinerInfo);
                Log.e("Radio_Text", id+"  "+rb.getText().toString());
                dynamic_editText11.setText(rb.getText().toString());

            }


        });
        builder.show();


    }*/


    private  void dynamic_radiobutton1(int id,ArrayList<Form> datalist)
    {
        // final String[] items = {"Apple", "Banana", "Orange", "Grapes"};
        final ArrayList<Integer> selectedList = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Radio Button");

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alertrec, null);
        builder.setView(alertLayout);

        RecyclerView recyclerView=alertLayout.findViewById(R.id.rv);
        LinearLayoutManager liLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(liLayoutManager);
        AlertAdapter1 alertAdapter=new AlertAdapter1(datalist, getActivity());

        recyclerView.setAdapter(alertAdapter);



        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //if(i>0) {
                Log.e("Log i", String.valueOf(i));

                String choice = String.valueOf(alertAdapter.getSelectedItem());

                if(!TextUtils.isEmpty(choice)&&choice!=null) {
                    Log.e("Choice", choice);
                    dynamic_editText11.setText(choice);
//                    sp = getSharedPreferences("Form", 0);
//                    editor = sp.edit();
//                    editor.putString("rb", choice);
//                    editor.commit();


                    SpinerDTO spinerInfo = new SpinerDTO();
                    spinerInfo.setRadio_id(id);
                    spinerInfo.setRadio_text(choice);
                    radiobtnList.add(spinerInfo);
                }else {

                    Log.e("Log i1", String.valueOf(i));

//                    sp = getSharedPreferences("Form", 0);
//                    String choice1=  sp.getString("rb", null);
//                    Log.e("Choice11", choice1);

                    dynamic_editText11.setText(choice);
                    SpinerDTO spinerInfo = new SpinerDTO();
                    spinerInfo.setRadio_id(id);
                 //   spinerInfo.setRadio_text(choice1);
                    radiobtnList.add(spinerInfo);

                }
            }
        });

        builder.show();

    }



    public void getCouponDenoListAdapter(final Spinner spiner_coupon_deno,
                                         final ArrayList<Form> datalist, final int id) {

        ArrayAdapter<Form> programadapter = new ArrayAdapter<Form>(getActivity(), R.layout.spinnertext,R.id.tv, datalist);
        spiner_coupon_deno.setAdapter(programadapter);
        //  programadapter.setDropDownViewResource(R.layout.spinner_list_item);
        spiner_coupon_deno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentAdapter,
                                       View arg1, int position1, long arg3) {
                // TODO Auto-generated method stub
                ((TextView)arg1.findViewById(R.id.tv)).setTextColor(Color.rgb(0, 0, 1));

               // String  coupon_deno = (String) parentAdapter.getItemAtPosition(position1);

                Form form = (Form) parentAdapter.getItemAtPosition(position1);

                 data_Id=form.getData_id();
                String data_Val=form.getData_val();


                Log.e("Value", id+" "+data_Val);

                SpinerDTO spinerInfo = new SpinerDTO();
                spinerInfo.setId(id);
                spinerInfo.setData_id(data_Id);
                spinerInfo.setCoupon_deno(data_Val);

              //  spinnerList.add(spinerInfo);

                for(int k=0;k<datalist.size();k++)
                {
                    if(datalist.get(k).getData_id()==data_Id)
                    {
                        Log.e("avc", data_Id+" "+data_Val);
                        spinnerList.add(spinerInfo);



                    }
//                    else {
//                        Log.e("ayz", data_Id+" "+data_Val);
//                        if(k>datalist.size()) {
//                            spinnerList.remove(datalist.get(k));
//                        }
//                    }
                }




            //    spinnerList.add(spinerInfo);





            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) { //
                // TODO Auto-generated method stub

            }

        });


    }

    private ImageView dynamic_image(int id) {
        dynamic_Image = new ImageView( getActivity());
        dynamic_Image.setId(id);

        LL_horizo = new LinearLayout( getActivity());
        LinearLayout.LayoutParams layoutForInner1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LL_horizo.setLayoutParams(layoutForInner1);


        LinearLay.addView(LL_horizo);

        LinearLayout LL_Inner = new LinearLayout( getActivity());
        LL_Inner.setOrientation(LinearLayout.HORIZONTAL);
        //  LL_Inner.setBackgroundColor(android.R.color.transparent);
        LinearLayout.LayoutParams layoutForInner = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LL_Inner.setLayoutParams(layoutForInner);

        // Camera Images
        ImageView camera_Image = new ImageView( getActivity());
        camera_Image.setId(id);
        image_id=id;
        LinearLayout.LayoutParams vp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //  vp1.gravity = Gravity.LEFT;
        vp1.setMargins(10, 10,10 ,10);
        camera_Image.setLayoutParams(vp1);
        camera_Image.setMaxHeight(50);
        camera_Image.setMaxWidth(50);
        //  camera_Image.setPadding(10, 10, 10, 15);

        camera_Image.setImageDrawable( getActivity().getDrawable(R.drawable.cam));


        // Gallery Images
        ImageView gallery_Image = new ImageView( getActivity());
        gallery_Image.setId(id);

        LinearLayout.LayoutParams vp11 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        // vp11.gravity = Gravity.RIGHT;
        vp11.setMargins(10, 10,10 ,10 );

        gallery_Image.setLayoutParams(vp11);
        gallery_Image.setMaxHeight(100);
        gallery_Image.setMaxWidth(100);
        gallery_Image.setImageDrawable( getActivity().getDrawable(R.drawable.gallery));

        LL_Inner.addView(camera_Image);
        LL_Inner.addView(gallery_Image);

        LinearLay.addView(LL_Inner);

        //  Camera_Gallery(camera_Image,gallery_Image,dynamic_Image,LL_horizo);
        Camera_Gallery(camera_Image,gallery_Image,dynamic_Image);



        return dynamic_Image;
    }


    public void Camera_Gallery(ImageView camera_Img, ImageView gallery_Img,ImageView ImageView)
    {
        gallery_Img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( getActivity(), "Gallery", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_REQUEST);


            }
        });
        camera_Img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText( getActivity(), "Camera", Toast.LENGTH_SHORT).show();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);

            }
        });


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap mphoto = (Bitmap) data.getExtras().get("data");

            Uri imgUri = data.getData();
            // String realPath = getRealPathFromURI(DashboardActivity12.this,imgUri);
            // Log.e("ImagePath", realPath);

            if(imageList.size()<0)
            {

            }else {
                LL_horizo.removeView(recyclerView);
                SpinerDTO dto = new SpinerDTO();
                dto.setImage_id(image_id);
                dto.setBitmap(mphoto);
                imageList.add(dto);

            }

            imageAdapter = new ImageAdapter(imageList, getActivity());
            recyclerView = new RecyclerView( getActivity());
            GridLayoutManager gridLayoutManager = new GridLayoutManager( getActivity(),3);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(imageAdapter);
            LL_horizo.addView(recyclerView);

        }else if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {

            Uri photoUri = data.getData();
            Bitmap mphoto = null;
            try {
                mphoto = MediaStore.Images.Media.getBitmap( getActivity().getContentResolver(), photoUri);

                if(imageList.size()<0)
                {

                }else {
                    LL_horizo.removeView(recyclerView);
                    SpinerDTO dto = new SpinerDTO();
                    dto.setImage_id(image_id);
                    dto.setBitmap(mphoto);
                    imageList.add(dto);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }


            imageAdapter = new ImageAdapter(imageList, getActivity());
            recyclerView = new RecyclerView( getActivity());
            GridLayoutManager gridLayoutManager = new GridLayoutManager( getActivity(),3);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(imageAdapter);
            LL_horizo.addView(recyclerView);



        }



    }

    public boolean isValidate(EditText editText, String value, String regex,
                              String errMsg) {

        if (value.isEmpty()) {
            editText.setError("Required Field");
            return false;
        } else if (!Pattern.matches(regex, value)) {

            editText.setError(errMsg);
            return false;

        }
        return true;

    }


//    private Button submitButton() {
//        Button button = new Button(getActivity());
//        button.setHeight(WRAP_CONTENT);
//        button.setText("Submit");
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        params.setMargins(40, 20, 40, 5);
//        button.setLayoutParams(params);
//        button.setBackgroundResource(R.drawable.rounded);
//        button.setOnClickListener(submitListener);
//        return button;
//    }

    private View.OnClickListener submitListener = new View.OnClickListener() {
        public void onClick(View view) {
            boolean validate = true;
            smsArrayList = new ArrayList<String>();
            Log.e("Random Number", GenRandNum());

            for (int j = 0; j < dynamicList.size(); j++) {
                if (dynamicList.get(j).getControlType().equalsIgnoreCase("dropdown")) {
                    for (int k = 0; k < spinnerList.size(); k++) {
                        Log.e("Size",dynamicList.size() +" "+ spinnerList.size());

                        if (spinnerList.get(k).getId()== dynamicList.get(j).getControId()) {

                          //  json.addProperty(dynamicList.get(j).getFieldName(), spinnerList.get(k).getCoupon_deno());


                            try {
                                JSONObject JSON =new JSONObject();

                                JSON.put("Control_Id", dynamicList.get(j).getControId());
                                JSON.put("Value", spinnerList.get(k).getCoupon_deno());


                                JSONArray.put(JSON);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            smsArrayList.add(spinnerList.get(k).getCoupon_deno());
                        }

                    }

                }


                if (dynamicList.get(j).getControlType().equalsIgnoreCase("textbox")) {

                    if (dynamicList.get(j).getIsMandatory().equalsIgnoreCase("true")) {

                        Log.e("Dynamic Size", dynamicList.get(j).getEditText().getText().toString());



                        if (isValidate(dynamicList.get(j).getEditText(),
                                dynamicList.get(j).getEditText().getText().toString(),
                               GetRegex(dynamicList.get(j).getRegex()),
                                dynamicList.get(j).getValidationMessage())) {


//                            json.addProperty(dynamicList.get(j)
//                                    .getFieldName(), dynamicList.get(j)
//                                    .getEditText().getText().toString()); //getFieldName
                        try {
                            JSONObject JSON =new JSONObject();

                            JSON.put("Control_Id", dynamicList.get(j).getControId());
                            JSON.put("Value", dynamicList.get(j).getEditText().getText().toString());
                            JSONArray.put(JSON);

                            Log.e("C1",dynamicList.get(j).getControId()+" "+ dynamicList.get(j).getEditText().getText().toString());
                            Log.e("EditText",dynamicList.get(j).getEditText().getText().toString());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                            smsArrayList.add(dynamicList.get(j)
                                    .getEditText().getText()
                                    .toString());




                        } else {
                            validate = false;

                        }
                    } else {

//                        json.addProperty(dynamicList.get(j)
//                                .getFieldName(), dynamicList.get(j)
//                                .getEditText().getText().toString());

                        try {
                            JSONObject JSON =new JSONObject();

                            JSON.put("control_id", dynamicList.get(j).getControId());
                            JSON.put("value", dynamicList.get(j).getEditText().getText().toString());
                            JSON11.put("", JSON);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        smsArrayList.add(dynamicList.get(j)
                                .getEditText().getText().toString());


                    }

                }
                if(dynamicList.get(j).getControlType().equalsIgnoreCase("radio_btn")) {
                    Log.e("RadioButton List Size", String.valueOf(radiobtnList.size()));
                    for (int k = 0; k < radiobtnList.size(); k++) {

                        if (radiobtnList.get(k).getRadio_id()== dynamicList.get(j).getControId()) {

                            Log.e("RadioButton********", dynamicList.get(j).getFieldName()+" "+ radiobtnList.get(k).getRadio_text());

                           // json.addProperty("Radio_Button", radiobtnList.get(k).getRadio_text());

                            try {
                                JSONObject JSON =new JSONObject();

                                JSON.put("Control_Id", dynamicList.get(j).getControId());
                                JSON.put("Value", radiobtnList.get(k).getRadio_text());
                                JSONArray.put(JSON);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            smsArrayList.add(radiobtnList.get(k).getRadio_text());
                        }

                    }
                }
                if(dynamicList.get(j).getControlType().equalsIgnoreCase("checkbox")) {
                    Log.e("CheckBox List Size", String.valueOf(checkboxList.size()));
                    StringBuffer  res=new StringBuffer();
                    for (int k = 0; k < checkboxList.size(); k++) {

                        if (checkboxList.get(k).getCheckbox_id()== dynamicList.get(j).getControId()) {

                          //  json.addProperty("Check Box",checkboxList.get(k).getCheckbox_text());
                            try {
                                JSONObject JSON =new JSONObject();

                                JSON.put("Control_Id", dynamicList.get(j).getControId());
                                JSON.put("Value", checkboxList.get(k).getCheckbox_text());
                                JSONArray.put(JSON);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            smsArrayList.add(res.toString());
                        }

                    }
                }
                if(dynamicList.get(j).getControlType().equalsIgnoreCase("image")) {
                    Log.e("Image List Size", String.valueOf(imageList.size()));
                    StringBuffer  res=new StringBuffer();
                    for (int k = 0; k < imageList.size(); k++) {

                        if (imageList.get(k).getImage_id()== dynamicList.get(j).getControId()) {
                            //  + "")
                            //  Log.e("Image********", dynamicList.get(j).getFieldName()+" "+ imageList.get(k).getBitmap());
                            Bitmap bitmap=imageList.get(k).getBitmap();
                            // Convert bitmap in to Base 64 encode String
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream .toByteArray();
                            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

//                      Convert Base 64 encode String in to Bitmap
//                            byte[] decodedBytes = Base64.decode(
//                                    encoded.substring(encoded.indexOf(",")  + 1),
//                                    Base64.DEFAULT
//                            );
//
//                         Bitmap bitmap1= BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);


                            // iv.setImageDrawable(getDrawable(R.drawable.gallery1));


                            //Log.e("Image*_encode", encoded);
                            res.append(encoded+" "+",");
                            json.addProperty("Image",res.toString());

                            smsArrayList.add(res.toString());
                        }

                    }
                }


            }


            if (validate == true) {

                //values in json format
                Object []  mStringArray = smsArrayList.toArray();
              // String  smsBody = "9849496829";

//                Toast.makeText(getActivity(), json.toString(),
//                        Toast.LENGTH_LONG).show();

                Log.e("GprsJsonFormat===" , JSONArray.toString());


//                sp=getSharedPreferences("Form", 0);
//                editor=sp.edit();
//                editor.putString("json",json.toString());
//                editor.commit();


                //values from array
                for (int i = 0; i < mStringArray.length; i++) {

                    if (!mStringArray[i].equals("")) {
                       // smsBody += "*" + mStringArray[i];
                    }

                }



              //  SubmitForm(GenRandNum(), String.valueOf(JSONArray));

                Log.e("Jsonaa", JSONArray.toString());

               // db.AddFormDataUp(pro_id,form_id,form_name,GenRandNum(), String.valueOf(JSONArray), GetCurrentDateTime());
               // db.AddFormDataUp("1","2","Form 2",GenRandNum(), String.valueOf(JSONArray), GetCurrentDateTime());
               // SubmitForm();
            }

        }

    };
   public String GetRegex(String re)
   {
       String regex=re.replace("\\\\","\\");
        Log.e("Regex", regex);

       return regex;
   }

   public String GenRandNum()
   {
       Random rand = new Random();
       int rand_int1 = rand.nextInt(10000);   // Generate random integers in range 0 to 999
       Calendar c = Calendar.getInstance();

       SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
       String formattedDate = df.format(c.getTime());
       String random_num= formattedDate+"_"+rand_int1;
       return  random_num;
   }


    public void SubmitForm() {
      Cursor cursor = db.GetValueForServer("form_data");
        String pro_id = null,form_id= null,uniqueId = null,val=null;
      if(cursor.getCount()>0)
      {

          while (cursor.moveToNext()) {
              //Project_detail pp=new Project_detail();
               pro_id= cursor.getString(cursor.getColumnIndex("project_id"));
               form_id = cursor.getString(cursor.getColumnIndex("form_id"));
              uniqueId = cursor.getString(cursor.getColumnIndex("Unique_Id"));
               String form_name= cursor.getString(cursor.getColumnIndex("form_name"));
               val= cursor.getString(cursor.getColumnIndex("json"));
              cursor.getString(cursor.getColumnIndex("sync"));


              Log.e("Fetch SaveForm", pro_id+" "+form_id+" "+uniqueId+" "+form_name+" "+val+" ");
          }

      }
        Client _client = new Client (getActivity());
        try {
            String location="{\"lat\":24.09444494,\"lon\":78.950690}";
            String user_id =SharedPreferencesAdapter.getPreferences(getActivity()).getString("userId",null );
            _client.SubmitForm (user_id,pro_id,"1",location,form_id,uniqueId,val).enqueue (new Callback<HashMap<String, Object>>() {
                @Override
                public void onResponse(Call<HashMap<String, Object>> call, retrofit2.Response<HashMap<String, Object>> response) {
                    HashMap<String, Object> _response = response.body();
                    // Log.e("Map value11", _response.toString());
                    Log.e("Map11 ", "hfg" + _response);

                    int mStatus = JSONUtils.getInteger(_response, "success");
                    if (mStatus == 1) {

                        String message = JSONUtils.getString(_response, "message");
                      //  ArrayList jsonArray=JSONUtils.getArrayList(_response, "data");


                    }else {
                        String message = JSONUtils.getString(_response, "message");
                    }




                }
                @Override
                public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                }
            } );
        } catch (NullPointerException e) {
            Log.e("Exception", e.toString());
        }
    }

}
