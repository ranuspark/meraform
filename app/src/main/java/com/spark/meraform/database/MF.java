package com.spark.meraform.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;

public class MF extends AppCompatActivity {
    private static MF sSharedInstance;

    public static Context getInstance() {
        return sSharedInstance;
    }

    public static SQLiteDatabase getDatabase() {
        return PersistentStore.getInstance(getInstance()).getWritableDatabase();
    }

}
