package com.spark.meraform.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.spark.meraform.Model.Form;
import com.spark.meraform.Model.Project;
import com.spark.meraform.Model.Project_detail;
import com.spark.meraform.Utility.CursorUtils;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "MeraForm.db";


    // Contacts table name
    private static final String TABLE_FORMS = "forms";
    private static final String TABLE_FORM_DATA = "form_data";
    private static final String TABLE_PROJECTS = "projects";
    private static final String TABLE_USER = "user";



    // TABLE_PROJECTS Table Columns
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_FORMID = "form_id";
    private static final String COLUMN_PROID = "project_id";
    private static final String COLUMN_PRONAME = "pro_name";
    private static final String COLUMN_FORMNAME = "form_name";

    private static final String COLUMN_USERID = "user_id";
    private static final String COLUMN_USERNAME = "user_name";
    private static final String COLUMN_USEREMAIL = "user_email";
    private static final String COLUMN_USERPHONE = "user_phone";

    private static final String COLUMN_JSON= "json";
    private static final String COLUMN_CREATEAT = "created_at";
    private static final String COLUMN_UPDATEAT = "updated_at";
    private static final String COLUMN_FORM = "form";
    private static final String COLUMN_DESC = "description";
    private static final String COLUMN_SYNC = "sync";
    private static final String COLUMN_UNIQUEID = "Unique_Id";





    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {


        String CREATE_FORM_TABLE = "CREATE TABLE " + TABLE_FORMS + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," + COLUMN_PROID + " INTEGER," + COLUMN_FORMID + " INTEGER,"
                + COLUMN_FORMNAME + " TEXT," + COLUMN_JSON + " TEXT,"
                + COLUMN_CREATEAT + " REAL," +  COLUMN_UPDATEAT  + " REAL);";
        db.execSQL(CREATE_FORM_TABLE);

        String CREATE_FORMDATA_TABLE = "CREATE TABLE " + TABLE_FORM_DATA + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," + COLUMN_PROID + " INTEGER," + COLUMN_FORMID + " INTEGER," + COLUMN_UNIQUEID + " TEXT,"
                + COLUMN_FORMNAME + " TEXT," + COLUMN_JSON + " TEXT,"
                + COLUMN_CREATEAT + " REAL," +  COLUMN_UPDATEAT + " REAL," +  COLUMN_SYNC + " TEXT);";
        db.execSQL(CREATE_FORMDATA_TABLE);

        String CREATE_PROJECT_TABLE = "CREATE TABLE " + TABLE_PROJECTS + "("
                + COLUMN_PROID + " INTEGER,"
                + COLUMN_PRONAME + " TEXT," + COLUMN_USERID + " TEXT,"
                + COLUMN_DESC + " TEXT," +  COLUMN_UPDATEAT + " REAL," +  COLUMN_CREATEAT + " REAL);";
        db.execSQL(CREATE_PROJECT_TABLE);

        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + COLUMN_USERID + " INTEGER,"
                + COLUMN_PROID + " INTEGER,"
                + COLUMN_USERNAME + " TEXT," + COLUMN_USEREMAIL + " TEXT," + COLUMN_USERPHONE + " INTEGER,"
                +  COLUMN_UPDATEAT + " REAL," +  COLUMN_CREATEAT + " REAL);";
        db.execSQL(CREATE_USER_TABLE);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROJECTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORM_DATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        // Create tables again
        onCreate(db);
    }



   public void AddProjectUpdate(Project_detail project_detail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PROID, project_detail.getPro_id());
        values.put(COLUMN_PRONAME, project_detail.getPro_name());
        values.put(COLUMN_USERID, 5);
        values.put(COLUMN_DESC, project_detail.getPro_des());


        // Inserting Row
       // db.insert(TABLE_PROJECTS, null, values);
       int id = getProID(project_detail.getPro_id());
       if(id==-1) {
           values.put(COLUMN_CREATEAT, project_detail.getPro_create());
           db.insert(TABLE_PROJECTS, null, values);
       }  else if(id==Integer.parseInt(project_detail.getPro_id()))
       {
           values.put(COLUMN_UPDATEAT, project_detail.getPro_create());

           db.update(TABLE_PROJECTS, values, COLUMN_PROID +" = ?", new String[]{project_detail.getPro_id()});

       }else if(id!=Integer.parseInt(project_detail.getPro_id())) {
           values.put(COLUMN_CREATEAT, project_detail.getPro_create());
           db.insert(TABLE_PROJECTS, null, values);

       }



        db.close(); // Closing database connection
    }

    public ArrayList<Project_detail> FetchProject()
    {
            ArrayList<Project_detail> project_details=new ArrayList<>();
          //  String[] columns = new String[] { DatabaseHelper._ID, DatabaseHelper.SUBJECT, DatabaseHelper.DESC };
             String query="SELECT * FROM "+ TABLE_PROJECTS;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            int cnt = cursor.getCount();
            if (cnt > 0) {

                while(cursor.moveToNext())
                {
                    Project_detail pp=new Project_detail();

                    pp.setPro_id(cursor.getString(0));
                    pp.setPro_name(cursor.getString(1));
                    cursor.getString(2);
                    pp.setPro_des(cursor.getString(3));
                    cursor.getString(4);
                    pp.setPro_create(cursor.getString(5));

                    project_details.add(pp);

                    Log.e("Fetch Project", cursor.getString(0)+" "+cursor.getString(1)+" "+cursor.getString(2));
                }

            }
            return project_details;
    }

    public void AddForm(String pro_id , String json , String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PROID, pro_id);
        values.put(COLUMN_FORMNAME, "");
        values.put(COLUMN_JSON,json);
        values.put(COLUMN_UPDATEAT, "");
        values.put(COLUMN_CREATEAT, created_at);

        // Inserting Row
        db.insert(TABLE_FORMS, null, values);
        db.close(); // Closing database connection
    }
    public void AddFormUp(String pro_id , String form_id, String form_name, String json , String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PROID, pro_id);
        values.put(COLUMN_FORMID, form_id);
        values.put(COLUMN_FORMNAME, form_name);
        values.put(COLUMN_JSON,json);

        Log.e("PPP", form_id);
        int id = getID(form_id,"fdesign");
        Log.e("SSS", String.valueOf(id));

        if(id==-1) {
            values.put(COLUMN_CREATEAT, created_at);

            db.insert(TABLE_FORMS, null, values);
        }  else if(id==Integer.parseInt(form_id))
        {
            values.put(COLUMN_UPDATEAT, created_at);

            db.update(TABLE_FORMS, values, COLUMN_FORMID +" = ?", new String[]{form_id});

        }else if(id!=Integer.parseInt(form_id)) {
            values.put(COLUMN_CREATEAT, created_at);
            db.insert(TABLE_FORMS, null, values);

        }

        db.close();
    }

    public void AddFormDataUp(String pro_id,String form_id,String form_name,String random_num, String json , String created_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PROID, pro_id);
        values.put(COLUMN_FORMID, form_id);
        values.put(COLUMN_FORMNAME, form_name);
        values.put(COLUMN_UNIQUEID, random_num);
        values.put(COLUMN_JSON,json);
        values.put(COLUMN_SYNC,0);

        Log.e("PPP1", form_id);
        int id = getID(form_id,"fdata");
        Log.e("SSS1", String.valueOf(id));

        if(id==-1) {
            values.put(COLUMN_CREATEAT, created_at);

            db.insert(TABLE_FORM_DATA, null, values);
        }  else if(id==Integer.parseInt(form_id))
        {
            values.put(COLUMN_UPDATEAT, created_at);

            db.update(TABLE_FORM_DATA, values, COLUMN_FORMID +" = ?", new String[]{form_id});

        }else if(id!=Integer.parseInt(form_id)) {
            values.put(COLUMN_CREATEAT, created_at);
            db.insert(TABLE_FORM_DATA, null, values);

        }

        db.close();
    }



//    private int getID(String json, String created_at){
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor c = db.query(TABLE_FORMS,new String[]{COLUMN_PROID}, COLUMN_JSON + "=? AND "+COLUMN_CREATEAT +"=?",new String[]{json,created_at},null,null,null,null);
//        if (c.moveToFirst()) //if the row exist then return the id
//            return c.getInt(c.getColumnIndex(COLUMN_PROID));
//        return -1;
//    }

    private int getID(String form_id,String type){
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor c = db.query(TABLE_FORMS,new String[]{COLUMN_PROID}, COLUMN_JSON + "=? AND "+COLUMN_CREATEAT +"=?",new String[]{json,created_at},null,null,null,null);

        if(type.equalsIgnoreCase("fdesign")){
            String query="SELECT * FROM "+ TABLE_FORMS + " WHERE "+COLUMN_FORMID +" = "+form_id;
            Cursor c = db.rawQuery(query, null);
            if (c.moveToNext()) { //if the row exist then return the id
                return c.getInt(c.getColumnIndex(COLUMN_FORMID));

            }

        }else if (type.equalsIgnoreCase("fdata"))
        {
            String query="SELECT * FROM "+ TABLE_FORM_DATA + " WHERE "+COLUMN_FORMID +" = "+form_id;
            Cursor c = db.rawQuery(query, null);
            if (c.moveToNext()) { //if the row exist then return the id
                return c.getInt(c.getColumnIndex(COLUMN_FORMID));

            }

        }

        return -1;
    }
    private int getProID(String pro_id){
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor c = db.query(TABLE_FORMS,new String[]{COLUMN_PROID}, COLUMN_JSON + "=? AND "+COLUMN_CREATEAT +"=?",new String[]{json,created_at},null,null,null,null);
        String query="SELECT * FROM "+ TABLE_PROJECTS + " WHERE "+COLUMN_PROID +" = "+pro_id;
        Cursor c = db.rawQuery(query, null);
        if (c.moveToNext()) //if the row exist then return the id
            return c.getInt(c.getColumnIndex(COLUMN_PROID));
        return -1;
    }
    public void AddFormData(Form formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PROID, 0);
        values.put(COLUMN_FORMNAME, "");
        values.put(COLUMN_JSON, "");
        values.put(COLUMN_SYNC, "");
        values.put(COLUMN_UPDATEAT, "");
        values.put(COLUMN_CREATEAT, "");

        // Inserting Row
        db.insert(TABLE_FORM_DATA, null, values);
        db.close(); // Closing database connection
    }

    public Cursor fetchFormSt(String pro_id)
    {
        String query="SELECT * FROM "+ TABLE_FORMS + " WHERE "+COLUMN_PROID +" = "+pro_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

//        int cnt = cursor.getCount();
//        if (cnt > 0) {
//
//            while(cursor.moveToNext())
//            {
//                //Project_detail pp=new Project_detail();
//
//                    cursor.getString(0);
//                    cursor.getString(1);
//                    cursor.getString(2);
//                    cursor.getString(3);
//                    cursor.getString(4);
//                    cursor.getString(5);
//
//
//
//                Log.e("Fetch Form", cursor.getString(0)+" "+cursor.getString(1)+" "+cursor.getString(2)+" "+cursor.getString(3)+" "+cursor.getString(4)+" "+cursor.getString(5));
//            }
//
//        }
        return cursor;
    }

    public String fetchProid()
    {
        String pro_Id = null;
        String query="SELECT "+COLUMN_PROID+ " FROM "+ TABLE_FORMS ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int cnt = cursor.getCount();
        if (cnt > 0) {

            while (cursor.moveToNext()) {
                //Project_detail pp=new Project_detail();

                pro_Id=cursor.getString(0);
               // cursor.getString(1);

                Log.e("Fetch ProductId", cursor.getString(0));
            }
        }

        return pro_Id;
    }
    public Cursor GetValueForServer(String Table_name)
    {
        String query=" SELECT * FROM "+ Table_name +" WHERE "+ COLUMN_SYNC +" = "+0 ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int cnt = cursor.getCount();

        return cursor;
    }
    public void UpdateSync(String pro_id)
    {


        String query=" UPDATE "+ TABLE_FORM_DATA +" SET "+ COLUMN_SYNC +" = "+ 1 +" WHERE "+ COLUMN_PROID +" = "+ pro_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int cnt = cursor.getCount();
        //db.update(TABLE_NAME, contentValues, "ID = ?",new String[] { id });
       // return ;
    }
    public void DeleteProject()
    {
        String query="DELETE FROM "+ TABLE_PROJECTS;
        SQLiteDatabase db = this.getReadableDatabase();
         db.delete(TABLE_PROJECTS, null,null);
    }
    public void DeleteForm()
    {
        //String query="DELETE FROM "+ TABLE_FORMS;
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_FORM_DATA, null,null);
    }

}
