package com.spark.meraform.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.spark.meraform.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public class PersistentStore extends SQLiteOpenHelper {
    private static final String DEBUG_TAG = PersistentStore.class.getSimpleName();
    private static final String FILENAME = "mf.db";
    private static final int SCHEMA_VERSION = 1;
    private static PersistentStore sSharedInstance;
    private static Context Maincontext;





    public static boolean needUpgrade(Context context) {
        File _dbname  = getDatabasePath ( context );
        boolean _exits = _dbname.exists ();
        boolean needUpgrade;
        return _exits == false;
    }

    public static synchronized PersistentStore getInstance(Context context) {
        PersistentStore persistentStore;
        synchronized (PersistentStore.class) {
            if (sSharedInstance != null) {
                persistentStore = sSharedInstance;
            } else {
                sSharedInstance = new PersistentStore(context.getApplicationContext());
                Maincontext = context.getApplicationContext();
                sSharedInstance.getWritableDatabase();
                sSharedInstance.close();
                persistentStore = sSharedInstance;
            }
        }
        return persistentStore;
    }






    public static File getDatabasePath(Context context) {
        return context.getDatabasePath(FILENAME);
    }

    public PersistentStore(Context context) {
        super(context, FILENAME, null, 1);
    }

    public void onConfigure(SQLiteDatabase db) {
        db.enableWriteAheadLogging();
        db.execSQL("PRAGMA synchronous=NORMAL");
    }

    public void onCreate(SQLiteDatabase db) {
        Log.e(DEBUG_TAG, "Creating new database...");
        Context context1 = MF.getInstance ();

       // runSQL(db, context1, R.raw.db_c_table_projects);
       // runSQL(db, context1, R.raw.db_c_table_forms);
    //    runSQL(db, context1, R.raw.db_c_table_forms_data);
//        runSQL(db, context1, R.raw.db_c_table_choice_lists);
//        runSQL(db, context1, R.raw.db_c_table_classification_sets);
//        runSQL(db, context1, R.raw.db_c_table_records);
//        runSQL(db, context1, R.raw.db_c_table_map_layers);
//        runSQL(db, context1, R.raw.db_c_table_attachments);
//        runSQL(db, context1, R.raw.db_c_table_trash_can);
//        runSQL(db, context1, R.raw.db_c_table_storage);
//        runSQL(db, context1, R.raw.db_c_table_signature);
//        runSQL(db, context1, R.raw.db_c_table_sync_states);
//        runSingleSQL(db, context1, R.raw.db_c_trigger_record_cascade);
//        runSingleSQL(db, context1, R.raw.db_c_trigger_attachment_trash);
//        runSingleSQL(db, context1, R.raw.db_c_trigger_map_layer_trash);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.e(DEBUG_TAG, "Upgrading database schema version from... " + oldVersion +" to " + newVersion);

        Context context1 = Maincontext;
        switch (oldVersion) {
            default: return;
        }
    }
    private void runSQL(SQLiteDatabase db, Context context, int sqlFileID) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(sqlFileID)));
        try {
            ArrayList<String> statements = new ArrayList();
            StringBuilder builder = new StringBuilder();
            while (reader.ready()) {
                String line = reader.readLine();
                builder.append(line);
                if (line.endsWith(";")) {
                    statements.add(builder.toString());
                    builder = new StringBuilder();
                }
            }
            Iterator it = statements.iterator();
            while (it.hasNext()) {
                String statement = (String) it.next();
                Log.e(DEBUG_TAG,"Executing: " + statement);

                db.execSQL(statement);
            }
        } catch (Throwable e) {
            Log.e("Exception :", e.toString());
        }
    }

    private void runSingleSQL(SQLiteDatabase db, Context context, int sqlFileID) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(sqlFileID)));
        try {
            StringBuilder builder = new StringBuilder();
            while (reader.ready()) {
                builder.append(reader.readLine());
            }
            db.execSQL(builder.toString());
        } catch (Throwable e) {
            Log.e("Exception :", e.toString());
        }
    }

//    private ArrayList<String> getTableColumns(SQLiteDatabase db, String table) {
//        ArrayList<String> columns = new ArrayList();
//        Cursor c = db.rawQuery(String.format("PRAGMA table_info(%s)", new Object[]{table}), null);
//        if (c != null) {
//            c.moveToFirst();
//            while (!c.isAfterLast()) {
//                columns.add(CursorUtils.getString(c, "name"));
//                c.moveToNext();
//            }
//            c.close();
//        }
//        return columns;
//    }
//
//    private boolean columnExists(SQLiteDatabase db, String table, String column) {
//        return getTableColumns(db, table).contains(column);
//    }


}
