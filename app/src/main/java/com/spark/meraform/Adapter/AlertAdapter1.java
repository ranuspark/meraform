package com.spark.meraform.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.icu.util.DateInterval;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.spark.meraform.Activity.DashboardActivity1;
import com.spark.meraform.Model.Form;
import com.spark.meraform.R;

import java.util.ArrayList;
import java.util.List;

public class AlertAdapter1 extends RecyclerView.Adapter<AlertAdapter1.MyViewHolder>  {


    private ArrayList<Form> moviesList;
    Context context;
    private int selectedPosition = -1;
    Boolean val;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RadioButton radioButton;
        public RadioGroup radioGroup;

        public MyViewHolder(View view) {
            super(view);

            radioButton = view.findViewById(R.id.radioButton1);
            radioGroup = view.findViewById(R.id.radioGroup);


        }
    }



    public AlertAdapter1(ArrayList<Form> moviesList, Activity context) {

        this.moviesList = moviesList;
        this.context = context;

    }

    @Override
    public AlertAdapter1.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_single_choice, parent, false);



        return new AlertAdapter1.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AlertAdapter1.MyViewHolder holder, int position) {
      //  SpinerDTO spinerDTO = moviesList.get(position);
        Form aa=moviesList.get(position);
         holder.radioButton.setText(aa.getData_val());
        holder.radioButton.setChecked(position == selectedPosition);
        holder.radioButton.setTag(position);

       SharedPreferences sp=context.getSharedPreferences("Form", 0);
       String rb=sp.getString("rb", null);
      if(TextUtils.isEmpty(rb)&&rb==null) {

      }else{
          if (rb.equals(aa)) {
          holder.radioButton.setChecked(true);
              val=true;

      }

      }

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
            }
        });

    }
    public String getSelectedItem() {
        if (val)
            if (selectedPosition !=-1) {
                Toast.makeText(context, "Selected Item : " + moviesList.get(selectedPosition), Toast.LENGTH_SHORT).show();
                return moviesList.get(selectedPosition).getData_val();
            }

        return moviesList.get(selectedPosition).getData_val();
    }

    private void itemCheckChanged(View v) {

        selectedPosition = (Integer) v.getTag();
        val=true;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    {
    }
}
