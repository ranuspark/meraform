package com.spark.meraform.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.spark.meraform.Activity.DashboardActivity1;
import com.spark.meraform.Model.Form;
import com.spark.meraform.Model.SpinerDTO;
import com.spark.meraform.R;

import java.util.ArrayList;
import java.util.List;

public class AlertAdapter  extends RecyclerView.Adapter<AlertAdapter.MyViewHolder> {


    private ArrayList<Form> moviesList;
    Activity context;
    final String aa = null;
    boolean ss;
    StringBuffer che_val;
    StringBuffer result = new StringBuffer();
    String checkbox;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    ArrayList<SpinerDTO> ids= new ArrayList<>();
    SpinerDTO nn;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox checkBox;

        public MyViewHolder(View view) {
            super(view);

            checkBox = view.findViewById(R.id.text1);



        }
    }


    public AlertAdapter(ArrayList<Form> moviesList, Activity context) {

        this.moviesList = moviesList;
        this.context = context;

    }

    @Override
    public AlertAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_multi_choice, parent, false);


        return new AlertAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AlertAdapter.MyViewHolder holder, int position) {
        //  SpinerDTO spinerDTO = moviesList.get(position);
        String mm = null;
        //String checkbox = null;
        final Form aa = moviesList.get(position);
        holder.checkBox.setText(aa.getData_val());


       nn=new SpinerDTO();
//        if (ids.contains(holder.checkBox.getId() + "")) {
//            holder.checkBox.setChecked(true);
//        } else {
//            holder.checkBox.setChecked(false);
//        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {



                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        int id = buttonView.getId();
                        Log.e("id", String.valueOf(id));
                        if (isChecked) {
                            if (ids.contains(holder.checkBox.getText().toString())) {
//                                ids.remove(nn.getCheckbox_id() + "");
//                                Log.e("RemoveValue", holder.checkBox.getText().toString());

                            } else {
                                nn.setCheckbox_id(id);
                                nn.setCheckbox_text(holder.checkBox.getText().toString());
                                ids.add(nn);
                                //ids.add(id + "");
                               // id++;
                                System.out.println( "receipe" + id);
                                Log.e("GetValue", holder.checkBox.getText().toString());
                             //   che_val= result.append(holder.checkBox.getText().toString() + ",");

                            }
                        } else {
                            Log.e("RemoveValue", holder.checkBox.getText().toString());
                           // if (ids.contains(holder.checkBox.getId())) {
                                ids.remove(position);
                                System.out.println( "receipe" + id);
                                Log.e("RemoveValue1", holder.checkBox.getText().toString());
                               // che_val= result.append(holder.checkBox.getText().toString() + ",");
                              //    id--;
                          //  } else {
//                                        id = 0;
                           // }

                        }

                    }
                });







       SharedPreferences sp=context.getSharedPreferences("Form", 0);
        if(sp != null){
             checkbox=sp.getString("checkbox", null);
            if(TextUtils.isEmpty(checkbox)&&checkbox==null) {

            }else {
                Log.e("CCCC", checkbox);
                String a[] = checkbox.split("\\s*,\\s*");
                for (int i = 0; i < a.length; i++) {
                    mm = a[i];
                    if (mm.equals(aa)) {
                        holder.checkBox.setChecked(true);
                        // val=true;
                    }
                }

            }
        }


    

      /*  String finalMm = mm;

      //  String finalCheckbox = checkbox;
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.checkBox.isChecked()) {

                    ss = true;
                    if(checkbox !=null&&!TextUtils.isEmpty(checkbox)&&!checkbox.equals("null")){
                        if(holder.checkBox.getText().toString()==finalMm)
                        {
                            result.replace(0, aa.length(), "");
                        }else {

                            che_val = result.append(holder.checkBox.getText().toString() + "," + checkbox);

                        }
                    }else {
                        che_val= result.append(holder.checkBox.getText().toString() + ",");
                    }


                    holder.checkBox.setTag(true);
                    onListner.get_value(holder.checkBox.getText().toString());
                   // che_val = holder.checkBox.getText().toString();
                    Log.e("Checkbox value", holder.checkBox.getText().toString());

                } else {

//                   String nn= holder.checkBox.getText().toString();
//
//                  che_val=  result.replace(0, moviesList.size(), String.valueOf(che_val));

                }

            }

        });*/

    }

          public StringBuffer getSelectedItem() {
               // result.append(che_val+ ",");
              for(int i=0;i<ids.size();i++)
              {
                  ids.get(i).getCheckbox_id();
                  ids.get(i).getCheckbox_text();
                  che_val=result.append(ids.get(i).getCheckbox_text()+",");
              }
              sp=context.getSharedPreferences("Form", 0);
              editor=sp.edit();
              editor.putString("checkbox", String.valueOf(che_val));
              editor.commit();

                return che_val;
            }

//            public String getSelectedItem1() {
//
//            return  result;
//            } */
//    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}
