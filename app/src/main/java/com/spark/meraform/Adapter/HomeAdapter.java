package com.spark.meraform.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.spark.meraform.Activity.HomeActivity;
import com.spark.meraform.Model.SpinerDTO;
import com.spark.meraform.R;

import java.util.List;

public class HomeAdapter extends  RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    private List<SpinerDTO> lessonsList;
    private Activity activity;

    public HomeAdapter(List<SpinerDTO> lessonsList, Activity activity) {
        this.lessonsList = lessonsList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SpinerDTO sdto = lessonsList.get(position);
        //  holder.courseName.setText(sdto.getCourseName());
        // holder.lessonName.setText(homeLastSeenLesson.getLessonName());
        // holder.itemView.setTag(homeLastSeenLesson.getLessonId());
        // Picasso.get ().load ( homeLastSeenLesson.getLessonImage () ).into ( holder.lessonImage );
        holder.titleName.setText(sdto.getCheckbox_text());
        holder.tv_act.setText("last updated"+" "+"12:00Am");
    }


    @Override
    public int getItemCount() {
        return lessonsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titleName;
        public TextView tv_act;
     //   public TextView courseName;

        public MyViewHolder(View view) {
            super(view);
            titleName = view.findViewById(R.id.tv_title);
            tv_act = view.findViewById(R.id.tv_act);
         //   lessonImage = (ImageView) view.findViewById(R.id.lessonImage);


//            view.setOnClickListener(v -> {
//                Log.d("Tap", "Lesson Id: " + v.getTag().toString());
//                //((Home) activity).homeLessonSelected(v.getTag().toString());
//            });
        }

    }
}