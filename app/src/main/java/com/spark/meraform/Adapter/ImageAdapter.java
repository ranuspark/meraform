package com.spark.meraform.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.spark.meraform.Activity.DashboardActivity1;
import com.spark.meraform.Activity.FullImageActivity;
import com.spark.meraform.Model.SpinerDTO;
import com.spark.meraform.R;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    private List<SpinerDTO> moviesList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView,delete_Iv;

        public MyViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.iv);
            delete_Iv = view.findViewById(R.id.delete_iv);

        }
    }


    public ImageAdapter(List<SpinerDTO> moviesList, Activity context) {

        this.moviesList = moviesList;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.img_adap, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SpinerDTO spinerDTO = moviesList.get(position);
       // holder.imageView.setImageBitmap(spinerDTO.getBitmap());
        Bitmap bitmap=spinerDTO.getBitmap();
//        bitmap.getWidth();
//        bitmap.getHeight();
        Log.e("Origional",  bitmap.getWidth()+" "+ bitmap.getHeight());
      Bitmap compressedBitmap=  scaleDown(bitmap,100f,true);

        compressedBitmap.getWidth();
        compressedBitmap.getHeight();

        Log.e("Compress",  compressedBitmap.getWidth()+" "+ compressedBitmap.getHeight());

        BitmapDrawable ob = new BitmapDrawable(context.getResources(), compressedBitmap);
        holder.imageView.setBackgroundDrawable(ob);
        holder.delete_Iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moviesList.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] items = {"View"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Image")

                        .setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (items[which]=="View") {
                              Bitmap bit=scaleDownBitmap(spinerDTO.getBitmap(),200,context);
                                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                    bit.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                                 //   String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                                  //  Log.e("Bitmap", encoded);

                                    Intent intent = new Intent(context, FullImageActivity.class);
                                    intent.putExtra("image", byteArray);

                                    context.startActivity(intent);
                                   // Toast.makeText(context, items[which] + " is clicked", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

//                builder.setPositiveButton("OK", null);
//                builder.setNegativeButton("CANCEL", null);
//                builder.setNeutralButton("NEUTRAL", null);
               // builder.setPositiveButtonIcon(getResources().getDrawable(android.R.drawable.ic_menu_call, getTheme()));
             //   builder.setIcon(getResources().getDrawable(R.drawable.jd, getTheme()));

                AlertDialog alertDialog = builder.create();

                alertDialog.show();

//                Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
//               // button.setBackgroundColor(Color.BLACK);
//                button.setPadding(0, 0, 20, 0);
//              //  button.setTextColor(Color.WHITE);

            }
        });

    }
    public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight, Context context) {

        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        int h= (int) (newHeight*densityMultiplier);
        int w= (int) (h * photo.getWidth()/((double) photo.getHeight()));

        photo=Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }
    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                maxImageSize / realImage.getWidth(),
                maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }
    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
