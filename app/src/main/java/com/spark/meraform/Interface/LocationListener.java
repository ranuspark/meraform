package com.spark.meraform.Interface;

import android.location.Location;

public interface LocationListener {
    void onLocationChanged(Location var1);
}
