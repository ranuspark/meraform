package com.spark.meraform.Model;

import java.io.Serializable;

public class Project_detail implements Serializable {
    private String pro_id;
    private String pro_name;
    private String pro_des;
    private int count;
    private String pro_create;

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_des() {
        return pro_des;
    }

    public void setPro_des(String pro_des) {
        this.pro_des = pro_des;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPro_create() {
        return pro_create;
    }

    public void setPro_create(String pro_create) {
        this.pro_create = pro_create;
    }
}
