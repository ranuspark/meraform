package com.spark.meraform.Model;

import java.util.ArrayList;

public class Form {
    private int control_Id;
    private String ControlType,IsMandatory,ValidationName,DataType,Regex,ValidationMsg,FieldName,LableName;
    ArrayList<Form> DataList;
   String DataList1;


    private int data_id;
    private String data_val;




    public int getControl_Id() {
        return control_Id;
    }

    public void setControl_Id(int control_Id) {
        this.control_Id = control_Id;
    }

    public String getControlType() {
        return ControlType;
    }

    public void setControlType(String controlType) {
        ControlType = controlType;
    }

    public String getIsMandatory() {
        return IsMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        IsMandatory = isMandatory;
    }

    public String getValidationName() {
        return ValidationName;
    }

    public void setValidationName(String validationName) {
        ValidationName = validationName;
    }

    public String getDataType() {
        return DataType;
    }

    public void setDataType(String dataType) {
        DataType = dataType;
    }

    public String getRegex() {
        return Regex;
    }

    public void setRegex(String regex) {
        Regex = regex;
    }

    public String getValidationMsg() {
        return ValidationMsg;
    }

    public void setValidationMsg(String validationMsg) {
        ValidationMsg = validationMsg;
    }

    public String getFieldName() {
        return FieldName;
    }

    public void setFieldName(String fieldName) {
        FieldName = fieldName;
    }

    public String getLableName() {
        return LableName;
    }

    public void setLableName(String lableName) {
        LableName = lableName;
    }

    public ArrayList<Form> getDataList() {
        return DataList;
    }

    public void setDataList(ArrayList<Form> dataList) {
        DataList = dataList;
    }

    public int getData_id() {
        return data_id;
    }

    public void setData_id(int data_id) {
        this.data_id = data_id;
    }

    public String getData_val() {
        return data_val;
    }

    public void setData_val(String data_val) {
        this.data_val = data_val;
    }

    public String getDataList1() {
        return DataList1;
    }

    public void setDataList1(String dataList1) {
        DataList1 = dataList1;
    }

    @Override
    public String toString() {
        return data_val.toString();
    }
}
