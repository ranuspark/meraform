package com.spark.meraform.Model;

import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

public class DynamicDTO {

    private String ControlId;
    private String ControlType;
    private String IsMandatory;
    private String ValidationName;
    private String Regex;

    private String ValidationMessage;
    private String FieldName;
    private String LableName;
    private String DataList;
    private String spinnerValue;
    private int ControId;

    private EditText editText;
    private Spinner dynamic_spinner;
    private RadioButton dynamic_radiobtn;
    private String FormName;

public DynamicDTO()
{

}
    public  DynamicDTO (int ControlId,String ControlType,String IsMandatory,String ValidationName,String Regex ,String ValidationMessage ,String FieldName,String LableName,String DataList )
    {
        this.ControId=ControlId;
        this.ControlType=ControlType;
        this.IsMandatory=IsMandatory;
        this.ValidationName=ValidationName;
        this.Regex=Regex;
        this.ValidationMessage=ValidationMessage;
        this.FieldName=FieldName;
        this.LableName=LableName;
        this.DataList=DataList;



    }

    public String getControlId() {
        return ControlId;
    }

    public void setControlId(String controlId) {
        ControlId = controlId;
    }

    public String getControlType() {
        return ControlType;
    }

    public void setControlType(String controlType) {
        ControlType = controlType;
    }

    public String getIsMandatory() {
        return IsMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        IsMandatory = isMandatory;
    }

    public String getValidationName() {
        return ValidationName;
    }

    public void setValidationName(String validationName) {
        ValidationName = validationName;
    }

    public String getRegex() {
        return Regex;
    }

    public void setRegex(String regex) {
        Regex = regex;
    }

    public String getValidationMessage() {
        return ValidationMessage;
    }

    public void setValidationMessage(String validationMessage) {
        ValidationMessage = validationMessage;
    }

    public String getFieldName() {
        return FieldName;
    }

    public void setFieldName(String fieldName) {
        FieldName = fieldName;
    }

    public String getLableName() {
        return LableName;
    }

    public void setLableName(String lableName) {
        LableName = lableName;
    }

    public String getDataList() {
        return DataList;
    }

    public void setDataList(String dataList) {
        DataList = dataList;
    }

    public String getSpinnerValue() {
        return spinnerValue;
    }

    public void setSpinnerValue(String spinnerValue) {
        this.spinnerValue = spinnerValue;
    }

    public int getControId() {
        return ControId;
    }

    public void setControId(int controId) {
        ControId = controId;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public void setSpinner(Spinner dynamic_spinner) {
        this.dynamic_spinner=dynamic_spinner;

    }

    public RadioButton getDynamic_radiobtn() {
        return dynamic_radiobtn;
    }

    public void setDynamic_radiobtn(RadioButton dynamic_radiobtn) {
        this.dynamic_radiobtn = dynamic_radiobtn;
    }

    public String getFormName() {
        return FormName;
    }

    public void setFormName(String formName) {
        FormName = formName;
    }
}
