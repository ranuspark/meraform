package com.spark.meraform.Model;

import android.content.Context;
import android.text.TextUtils;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.spark.meraform.Utility.JSONUtils;
import com.spark.meraform.Utility.UserSettings;


import java.util.Map;

public class Account {
    public static final String TYPE_USER = "user";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_GENDER = "gender";
    public static final String COLUMN_BIO = "bio";
    public static final String COLUMN_THUMBNAIL_URL = "profile_pic_thumbnails";
    public static final String COLUMN_ADDRESS_1 = "address_line_1";
    public static final String COLUMN_ADDRESS_2 = "address_line_2";
    public static final String COLUMN_STATE_ID = "state_id";
    public static final String COLUMN_CITY_ID = "city_id";
    public static final String COLUMN_PIN_CODE = "pin_code";
    public static final String COLUMN_ID_PROOF_TYPE = "id_proof_type";
    public static final String COLUMN_ID_PROOF_NUMBER = "id_proof_number";
    public static final String COLUMN__ID_PROOF_IMAGE_URL = "id_proof_image_url";
    public static final String COLUMN_IS_MOBILE_VERIFIED = "is_mobile_verified";
    public static final String COLUMN_IS_EMAIL_VERIFIES = "is_email_verified";
    public static final String COLUMN_USER_EMAIL = "user_email";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_TOKEN = "token";
    private String mFirstName;
    private String mLastName;
    private String mUserEmail;
    private String mUserID;
    private String mAuthenticationToken;
    private Integer mContextID;
    private String mContextName;
    private String mContextType;
    private String mGender;
    private String mBio;
    private String mProfilePicture;
    private String mAddressLine1;
    private String mAddressLine2;
    private String mStateId;
    private String mCityId;
    private String mPinCode;
    private String mProofType;
    private String mProofNumber;
    private String mProofImage;
    private boolean mIsMobileVerified;
    private boolean mIsEmailVerified;
    private long mRowID;
    private static Account sActiveAccount;
    private Context context;

    public Account() {
    }

    public Account(Context context) {
        this.context = context;
    }


    public static Account getAccount(Map user, Map context) {
        String userId = JSONUtils.getString ( context, "user_id" );
        Integer contextId = JSONUtils.getInt ( context, "context_id" );
        Account account = null;
        if (account == null) {
            account = new Account ();
        }
        account.setAttributesFromJSON ( user, context );
        return account;
    }

    public static Account getAccount(long id, boolean forceQuery) {
        if (sActiveAccount != null && sActiveAccount.getRowID () == id && !forceQuery) {
            return sActiveAccount;
        }
        String[] subArgs = new String[]{String.valueOf ( id )};
        Account account = null;
        return account;
    }


    public static synchronized void setActiveAccount(Account account) {
        synchronized (Account.class) {
            //setActiveAccount ( TestDarpanApplication.getInstance (), account );
        }
    }

    public static synchronized void setActiveAccount(Context context, Account account) {
        synchronized (Account.class) {
            setActiveAccount ( context, account, true );
        }
    }

    private static void setActiveAccount(Context context, Account account, boolean saveToPreferences) {
        sActiveAccount = account;
      //  TestdarpanLogger.setActiveAccount ( account );
        if (saveToPreferences) {
            UserSettings.setActiveAccount ( context, account );
        }
    }

    public void setAttributesFromJSON(Map user, Map context) {
        this.mUserID = JSONUtils.getString ( context, COLUMN_USER_ID );
        this.mFirstName = JSONUtils.getString ( context, COLUMN_FIRST_NAME );
        this.mLastName = JSONUtils.getString ( context, COLUMN_LAST_NAME );
        this.mUserEmail = JSONUtils.getString ( context, COLUMN_USER_EMAIL );
        this.mGender = JSONUtils.getString ( context, COLUMN_GENDER );
        this.mBio = JSONUtils.getString ( context, COLUMN_BIO );
        this.mProfilePicture = JSONUtils.getString ( context, COLUMN_THUMBNAIL_URL );
        this.mAddressLine1 = JSONUtils.getString ( context, COLUMN_ADDRESS_1 );
        this.mAddressLine2 = JSONUtils.getString ( context, COLUMN_ADDRESS_2 );
        this.mStateId = String.valueOf ( JSONUtils.getInt ( context, COLUMN_STATE_ID ) );
        this.mCityId = String.valueOf ( JSONUtils.getInt ( context, COLUMN_CITY_ID ) );
        this.mPinCode = String.valueOf ( JSONUtils.getInt ( context, COLUMN_PIN_CODE ) );
        this.mProofType = JSONUtils.getString ( context, COLUMN_ID_PROOF_TYPE );
        //this.mProofNumber = JSONUtils.getString ( context, COLUMN_ID_PROOF_NUMBER );
        //this.mProofImage = JSONUtils.getString ( context, COLUMN__ID_PROOF_IMAGE_URL );
        this.mIsMobileVerified = JSONUtils.getBoolean ( context, COLUMN_IS_MOBILE_VERIFIED );
        this.mIsEmailVerified = JSONUtils.getBoolean ( context, COLUMN_IS_EMAIL_VERIFIES );
        this.mContextID = JSONUtils.getInt ( context, "context_id" );
        this.mAuthenticationToken = JSONUtils.getString ( context, COLUMN_TOKEN );
    }

    public long getRowID() {
        return this.mRowID = Long.valueOf ( this.mContextID );
    }

    public String getUserEmailAddress() {
        return this.mUserEmail;
    }

    public String getContextID() {
        return String.valueOf ( this.mContextID );
    }

    public String getContextName() {
        return this.mContextName = "user";
    }

    public String getToken() {
        return this.mAuthenticationToken;
    }

    public String getUserID() {
        return this.mUserID;
    }

    public String getFirstName() {
        return this.mFirstName;
    }

    public String getLastName() {
        return this.mLastName;
    }

    public String getUserName() {
        if (!TextUtils.isEmpty ( this.mFirstName ) && !TextUtils.isEmpty ( this.mLastName )) {
            return this.mFirstName + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.mLastName;
        }
        if (!TextUtils.isEmpty ( this.mFirstName )) {
            return this.mFirstName;
        }
        if (TextUtils.isEmpty ( this.mLastName )) {
            return "";
        }
        return this.mLastName;
    }

    public static synchronized Account getActiveAccount(Context context) {
        Account account;
        synchronized (Account.class) {
            if (sActiveAccount != null) {
                account = sActiveAccount;
            } else {
                account = UserSettings.getActiveAccount ( context );
                setActiveAccount ( context, account, false );
            }
        }
        return account;
    }

    public static Account getAccount(long id) {
        return getAccount ( id, false );
    }

    public static Account upsert(Map user, Map context) {
        Account account = getAccount ( user, context );
        return account;
    }
}
