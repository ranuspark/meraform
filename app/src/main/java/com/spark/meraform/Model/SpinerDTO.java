package com.spark.meraform.Model;

import android.graphics.Bitmap;

public class SpinerDTO {

      String coupon_deno,spinerValue;
      int id;
      int radio_id;
      String radio_text;
    int checkbox_id;
    String checkbox_text;
    int image_id;
    Bitmap bitmap;
    int Data_id;
    public SpinerDTO()
    {

    }
//    public SpinerDTO(String s, String coupon_deno) {
//        this.id=s;
//        this.coupon_deno=coupon_deno;
//    }


    public int getData_id() {
        return Data_id;
    }

    public void setData_id(int data_id) {
        Data_id = data_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCoupon_deno() {
        return coupon_deno;
    }

    public void setCoupon_deno(String coupon_deno) {
        this.coupon_deno = coupon_deno;
    }

    public String getSpinerValue() {
        return spinerValue;
    }

    public void setSpinerValue(String spinerValue) {
        this.spinerValue = spinerValue;
    }

    public int getRadio_id() {
        return radio_id;
    }

    public void setRadio_id(int radio_id) {
        this.radio_id = radio_id;
    }

    public String getRadio_text() {
        return radio_text;
    }

    public void setRadio_text(String radio_text) {
        this.radio_text = radio_text;
    }

    public int getCheckbox_id() {
        return checkbox_id;
    }

    public void setCheckbox_id(int checkbox_id) {
        this.checkbox_id = checkbox_id;
    }

    public String getCheckbox_text() {
        return checkbox_text;
    }

    public void setCheckbox_text(String checkbox_text) {
        this.checkbox_text = checkbox_text;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
