package com.spark.meraform.Model;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;

import com.spark.meraform.Utility.CursorUtils;
import com.spark.meraform.Utility.DateUtils;
import com.spark.meraform.Utility.JSONUtils;
import com.spark.meraform.database.MF;
import com.spark.meraform.database.PersistentObject;
import com.spark.meraform.database.PersistentStore;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

@SuppressLint("ParcelCreator")
public class Project extends PersistentObject {

    public static final String COLUMN_ACCOUNT_ID = "account_id";
    public static final String COLUMN_DELETED_AT = "deleted_at";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_REMOTE_ID = "remote_id";
    public static final Creator<Project> CREATOR = new C11691();
    public static final String TABLE_NAME = "Projects";
    private long mAccountID;
    private Date mDeletedAt;
    private String mDescription;
    private String mName;
    private String mRemoteID;

    static class C11691 implements Creator<Project> {
        C11691() {
        }

        public Project createFromParcel(Parcel source) {
            return new Project(source);
        }

        public Project[] newArray(int size) {
            return new Project[size];
        }
    }

    public static Project find(Long id, String remoteID, Account account) {
        SQLiteDatabase db = MF.getDatabase();
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> valueList = new ArrayList();
        if (id != null) {
            predicates.add("_id = ?");
            valueList.add(String.valueOf(id));
        }
        if (remoteID != null) {
            predicates.add("remote_id = ?");
            valueList.add(remoteID);
        }
        if (account != null) {
            predicates.add("account_id = ?");
            valueList.add(String.valueOf(account.getRowID()));
        }
        Cursor cursor = db.query(TABLE_NAME, null, TextUtils.join(" AND ", predicates), valueList.toArray(new String[valueList.size()]), null, null, null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            cursor.close();
            return null;
        }
        Project project = new Project(cursor);
        cursor.close();
        return project;
    }

    public static Project findByID(long id) {
        Cursor cursor = MF.getDatabase().query(TABLE_NAME, null, "_id = ?", new String[]{String.valueOf(id)}, null, null, null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            cursor.close();
            return null;
        }
        Project project = new Project(cursor);
        cursor.close();
        return project;
    }

    public static Project findByRemoteID(Account account, String projectRemoteID) {
        return findByRemoteID(account, projectRemoteID, MF.getDatabase());
    }

    public static Project findByRemoteID(Account account, String projectRemoteID, SQLiteDatabase db) {
        if ((account == null ? null : String.valueOf(account.getRowID())) == null) {
            return null;
        }
        Cursor cursor = db.query(TABLE_NAME, null, "remote_id = ? AND account_id = ?", new String[]{projectRemoteID, account == null ? null : String.valueOf(account.getRowID())}, null, null, null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            cursor.close();
            return null;
        }
        Project project = new Project(cursor);
        cursor.close();
        return project;
    }

    public static Project getProject(Account anAccount, String name) {
        ArrayList<String> predicateList = new ArrayList();
        ArrayList<String> paramsList = new ArrayList();
        if (anAccount != null) {
            predicateList.add("account_id = ?");
            paramsList.add(String.valueOf(anAccount.getRowID()));
        }
        predicateList.add("deleted_at IS NULL");
        predicateList.add("name = ?");
        paramsList.add(name);
        String[] params = paramsList.toArray(new String[paramsList.size()]);
        Cursor cursor = MF.getDatabase().query(TABLE_NAME, null, TextUtils.join(" AND ", predicateList), params, null, null, "name ASC");
        cursor.moveToFirst();
        Project project = null;
        if (!cursor.isAfterLast()) {
            project = new Project(cursor);
        }
        cursor.close();
        return project;
    }

    public static ArrayList<Project> getProjects(Account anAccount) {
        ArrayList<Project> projects = new ArrayList();
        ArrayList<String> predicateList = new ArrayList();
        ArrayList<String> paramsList = new ArrayList();
        if (anAccount != null) {
            predicateList.add("account_id = ?");
            paramsList.add(String.valueOf(anAccount.getRowID()));
        }
        predicateList.add("deleted_at IS NULL");
        String[] params = paramsList.toArray(new String[paramsList.size()]);
        Cursor cursor = MF.getDatabase().query(TABLE_NAME, null, TextUtils.join(" AND ", predicateList), params, null, null, "name ASC");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            projects.add(new Project(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return projects;
    }

    public static void truncate(ArrayList<String> excludedRemoteIDs, Account account) {
        SQLiteDatabase db = MF.getDatabase();
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> paramsList = new ArrayList();
        if (account != null) {
            predicates.add("account_id = ?");
            paramsList.add(String.valueOf(account.getRowID()));
        }
        if (excludedRemoteIDs != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("remote_id NOT IN (");
            for (int i = 0; i < excludedRemoteIDs.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("'").append(excludedRemoteIDs.get(i)).append("'");
            }
            sb.append(")");
            predicates.add(sb.toString());
        }
        String predicate = TextUtils.join(" AND ", predicates);
        String[] subArgs = paramsList.toArray(new String[paramsList.size()]);
        ContentValues values = new ContentValues();
        values.put("deleted_at", DateUtils.getTimestampString(new Date()));
        db.update(TABLE_NAME, values, predicate, subArgs);
    }

    public Project(Map jsonRepresentation) {
        setAttributesFromJSON(jsonRepresentation);
    }

    public Project(Cursor cursor) {
        super(cursor);
        this.mRemoteID = CursorUtils.getString(cursor, "remote_id");
        this.mAccountID = CursorUtils.getLong(cursor, "account_id");
        this.mName = CursorUtils.getString(cursor, "name");
        this.mDescription = CursorUtils.getString(cursor, "description");
        int idxDeletedAt = cursor.getColumnIndex("deleted_at");
        if (!cursor.isNull(idxDeletedAt)) {
            this.mDeletedAt = new Date(cursor.getLong(idxDeletedAt));
        }
    }

    private Project(Parcel source) {
        super(source);
        boolean isDate = true;
        this.mRemoteID = source.readString();
        this.mAccountID = source.readLong();
        this.mName = source.readString();
        this.mDescription = source.readString();
        if (source.readInt() != 1) {
            isDate = false;
        }
        if (isDate) {
            this.mDeletedAt = new Date(source.readLong());
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(getRemoteID());
        dest.writeLong(getAccountID());
        dest.writeString(getName());
        dest.writeString(getDescription());
        if (this.mDeletedAt != null) {
            dest.writeInt(1);
            dest.writeLong(this.mDeletedAt.getTime());
            return;
        }
        dest.writeInt(0);
    }

    public String getRemoteID() {
        return this.mRemoteID;
    }

    public long getAccountID() {
        return this.mAccountID;
    }

    public String getName() {
        return this.mName;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public Date getDeletedAt() {
        return this.mDeletedAt;
    }

    public void setAttributesFromJSON(Map json) {
        //TODO:REMOVE THIS FIELD
        this.mRemoteID = JSONUtils.getString(json, "Blank Field");
        this.mName = JSONUtils.getString(json, "name");
        this.mDescription = JSONUtils.getString(json, "description");
    }

    public void setAccount(Account account) {
        if (account != null) {
            this.mAccountID = account.getRowID();
        } else {
            this.mAccountID = 0;
        }
    }

    public String toString() {
        return this.mName;
    }

    public void delete(SQLiteDatabase db, Bundle options) {
        softDelete();
    }

    public void clearDeletedAt() {
        this.mDeletedAt = null;
    }

    protected ContentValues getContentValues() {
        ContentValues values = super.getContentValues();
        values.put("project_id", 2);//getRemoteID()
        values.put("user_id",5);// Long.valueOf(getAccountID())
        values.put("name","Project Name" );//getName()
        values.put("description","Description of Project" );//getDescription()
        values.put("created_at","18-01-2018" );//getDescription()
        values.put("updated_at","18-01-2018" );//getDescription()

//        if (this.mDeletedAt == null) {
//            values.putNull("deleted_at");
//        } else {
//            values.put("deleted_at", Long.valueOf(this.mDeletedAt.getTime()));
//        }
        return values;
    }

    protected  String getTableName() {
        return TABLE_NAME;
    }

    private void softDelete() {
        this.mDeletedAt = new Date();
        save();
    }


    public  String insert(SQLiteDatabase db)
    {
        String c= "1";
       String table =  getTableName();
       ContentValues cv = getContentValues();
        if(db!=null)
        {
            db.insert(table, null, cv);
        }
        return c;
    }

        public Project()
        {}









}
