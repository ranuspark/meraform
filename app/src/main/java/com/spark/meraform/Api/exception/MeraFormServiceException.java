package com.spark.meraform.Api.exception;

import android.content.Context;

import java.io.IOException;

public class MeraFormServiceException extends IOException {

    public MeraFormServiceException(Throwable throwable) {
        super ( throwable.getMessage () == null ? throwable.toString () : throwable.getMessage () );
    }

    public MeraFormServiceException(String message) {
        super ( message );
    }

    public String getUserFriendlyMessage(Context context) {
        return getMessage ();
    }
}
