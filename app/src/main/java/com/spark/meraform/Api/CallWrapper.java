package com.spark.meraform.Api;

import android.util.Log;

import com.spark.meraform.Api.exception.MeraFormServiceException;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallWrapper<T> {

    public CallWrapper() {
    }

    public Response<T>[] EnExecute(Call paramCall) throws MeraFormServiceException {
        final Response<T>[] UserResponse = null;
        try {
            paramCall.enqueue ( new Callback () {
                @Override
                public void onResponse(Call call, Response response) {
                    UserResponse[0] = response;
                }

                @Override
                public void onFailure(retrofit2.Call call, Throwable t) {

                }
            } );

        } catch (RuntimeException _runTimeExpection) {
            testDarpanServiceException ( _runTimeExpection );
            if (UserResponse[0].isSuccessful ()) {
                handleUnsuccessful ( UserResponse[0] );
            }
            return UserResponse;
        }
        if (UserResponse[0].isSuccessful ()) {
            handleUnsuccessful ( UserResponse[0] );
        }
        return UserResponse;
    }

    public Response execute(Call paramCall) throws MeraFormServiceException {
        Response<T> response = null;
        try {
            response = paramCall.execute ();
        } catch (IOException _expection) {
            testDarpanServiceException ( _expection );
            if (!response.isSuccessful ()) {
                handleUnsuccessful ( response );
            }
            return response;
        } catch (RuntimeException _runTimeExpection) {
            testDarpanServiceException ( _runTimeExpection );
            if (response.isSuccessful ()) {
                handleUnsuccessful ( response );
            }
            return response;
        }
        if (response.isSuccessful ()) {
            handleUnsuccessful ( response );
        }
        Log.e("Response", String.valueOf(response));
        return response;
    }

    private void handleUnsuccessful(Response paramResponse) {

    }

    private void testDarpanServiceException(Exception paramException)
            throws MeraFormServiceException {
        throw new MeraFormServiceException ( paramException );
    }

  /*  private void invalidResponseException(int paramInt, String paramString)
            throws InvalidResponseException {
        throw new InvalidResponseException ( paramInt, paramString );
    }

    private void badRequestException(String paramString)
            throws BadRequestException {
        throw new BadRequestException ( paramString );
    }

    private void resourceNotFoundException()
            throws ResourceNotFoundException {
        throw new ResourceNotFoundException ();
    }

    private void serverMaintenanceException()
            throws ServerMaintenanceException {
        throw new ServerMaintenanceException ();
    }

    private void unprocessableEntityException(String paramString)
            throws UnprocessableEntityException {
        throw new UnprocessableEntityException ( paramString );
    }*/


}
