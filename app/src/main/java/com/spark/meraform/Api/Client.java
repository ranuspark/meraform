package com.spark.meraform.Api;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;


import com.spark.meraform.Model.Account;
import com.spark.meraform.Utility.ApplicationUtils;
import com.spark.meraform.Utility.DeviceInfo;
import com.spark.meraform.Utility.JSONUtils;
import com.spark.meraform.Utility.UserSettings;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Call;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Client {
   // public static final String DEFAULT_HOSTNAME = "35.154.179.66:6061";
  //  public static final String DEFAULT_HOSTNAME = "api.testdarpan.com";
  public static final String DEFAULT_HOSTNAME = "hiteshrohilla.com";
   // public static final String DEFAULT_HOSTNAME = "128.199.213.46";


   // http://hiteshrohilla.com/ranu/index.php?action=login

    private static final MediaType MULTIPART_FORM_DATA = MediaType.parse ( "multipart/form-data" );
    private final String mHostname;
    private final int mPort;
    private final String mProtocol;
    private MeraFormService mAPI;
    private String mApplicationBuildNumber;
    private String mApplicationVersion;
    private String mDeviceID;
    private String mDeviceManufacturer;
    private String mDeviceModel;
    private OkHttpClient mTDServiceClient;
    private MFServiceInterceptor mTDServiceInterceptor;
    private OkHttpClient mGeneralServiceClient;
    private String mPlatformVersion;
    private String[] pieces;

    public Client(Context context) {
        this ( context, "" );
    }

    public Client(Context context, Account account) {
        this ( context, account.getToken () );
    }

    public Client(Context context, String accountToken) {
        this.mHostname = UserSettings.getHostname ( context );
        boolean httpsEnabled = UserSettings.isHttpsEnabled ( context );
        this.mDeviceID = DeviceInfo.getDeviceIdentifier ( context );
        this.mDeviceModel = DeviceInfo.getDeviceModel ();
        this.mDeviceManufacturer = DeviceInfo.getDeviceManufacturer ();
        this.mPlatformVersion = DeviceInfo.getSystemVersion ();
        this.mApplicationVersion = ApplicationUtils.getVersion ( context );
        this.mApplicationBuildNumber = "" + ApplicationUtils.getBuildNumber ( context );
        int portIndex = getHostname ().lastIndexOf ( ":" );
        if (httpsEnabled) {
            this.mPort = 443;
            this.mProtocol = "https://";
        } else if (portIndex > -1) {
            int port;
            try {
                port = Integer.parseInt ( getHostname ().substring ( portIndex + 1 ) );
            } catch (Throwable e) {
                Log.e("Exception", e.toString());
                port = 80;
            }
            this.mPort = port;
            this.mProtocol = "http://";
        } else {
            this.mPort = 80;
            this.mProtocol = "http://";
        }
        this.mTDServiceInterceptor = new MFServiceInterceptor ( ApplicationUtils.getUserAgentString ( context ), accountToken, this.mApplicationVersion, this.mApplicationBuildNumber, this.mDeviceID, this.mDeviceModel, this.mDeviceManufacturer, this.mPlatformVersion );
        this.mAPI = createTestDarpanService ( true );
    }

    private void addRequestBody(HashMap<String, RequestBody> partMap, String key, String value) {
        if (!TextUtils.isEmpty ( value )) {
            partMap.put ( key, RequestBody.create ( MULTIPART_FORM_DATA, value ) );
        }
    }

    private String[] processUrl(URL url) {
        if (url == null) {
            return new String[0];
        }
        pieces = new String[3];
        String urlWithoutQuery = url.getProtocol () + "://" + url.getAuthority () + url.getPath ();
        int index = urlWithoutQuery.lastIndexOf ( "/" );
        pieces[0] = urlWithoutQuery.substring ( 0, index + 1 );
        pieces[1] = urlWithoutQuery.substring ( index + 1 );
        return pieces;
    }

    private HashMap<String, String> processQuery(String query) {
        HashMap<String, String> map = new HashMap();
        if (query != null) {
            for (String kvp : query.split ( "&" )) {
                String[] kv = kvp.split ( "=" );
                map.put ( kv[0], kv[1] );
            }
        }
        return map;
    }

    private GeneralService createGeneralService(String endpoint) {
        if (this.mGeneralServiceClient == null) {
            this.mGeneralServiceClient = createOkHttpClientBuilder ().build ();
        }
        return new Builder ().client ( this.mGeneralServiceClient ).baseUrl ( endpoint ).build ().create ( GeneralService.class );
    }

    private MeraFormService createTestDarpanService(boolean convertResponseToJSON) {
        return createTestDarpanService ( getProtocol () + getHostname (), convertResponseToJSON );
    }

    private MeraFormService createTestDarpanService(String endpoint, boolean convertResponseToJSON) {
        if (this.mTDServiceClient == null) {
            OkHttpClient.Builder okHttpClientBuilder = createOkHttpClientBuilder ();
            okHttpClientBuilder.addInterceptor ( this.mTDServiceInterceptor );
            this.mTDServiceClient = okHttpClientBuilder.build ();
        }
        Builder builder = new Builder ();
        builder.client ( this.mTDServiceClient ).baseUrl ( endpoint );
        if (convertResponseToJSON) {
            builder.addConverterFactory ( JacksonConverterFactory.create () );
        }
        return builder.build ().create ( MeraFormService.class );
    }

    private OkHttpClient.Builder createOkHttpClientBuilder() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder ();
        builder.connectTimeout ( 15, TimeUnit.SECONDS );
        builder.readTimeout ( 60, TimeUnit.SECONDS );
        builder.writeTimeout ( 60, TimeUnit.SECONDS );
        builder.addInterceptor ( new HttpLoggingInterceptor ().setLevel ( Level.BASIC ) );
        return builder;
    }

    public String getProtocol() {
        return this.mProtocol;
    }

    public String getHostname() {
        return this.mHostname;
    }

    public int getPort() {
        return this.mPort;
    }



    public Call<HashMap<String, Object>> getOtp_Login(String phone) {
        return this.mAPI.getOtp_Login (phone);
    }//varifyOTP

    public Call<HashMap<String, Object>> getVerifyOtp(String phone,String user_id,String otp) {
        return this.mAPI.getVerifyOtp (phone,user_id,otp);
    }

    public Call<HashMap<String, Object>> getProjectData(String user_id) {
        return this.mAPI.getProjectData (user_id);
    }
    public Call<HashMap<String, Object>> SubmitForm(String user_id,String project_id,String type,String location,String form_id,String unique_id,String value) {
        return this.mAPI.SubmitForm (user_id,project_id,type,location,form_id,unique_id,value);
    }
}
