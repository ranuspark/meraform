package com.spark.meraform.Api;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface MeraFormService {

    @GET("/api/student/getUser")
    Call<HashMap<String, Object>> getAccount();

    @POST("/api/v1/login")
    Call<HashMap<String, Object>> getAccount(@Header("Authorization") String str, @Body Map map);

    @POST("/api/v1/social-login")
    Call<HashMap<String, Object>> getAutoLogin(@Body Map map);



    @GET("/ranu/index.php?action=login")
    Call<HashMap<String, Object>> getOtp_Login(@Query("phone") String phone );

    @POST("/ranu/index.php?action=varifyOTP")
    Call<HashMap<String, Object>> getVerifyOtp(@Query("phone") String phone,@Query("user_id") String user_id,@Query("otp") String otp );

    @POST("/ranu/index.php?action=getProjects")
    Call<HashMap<String, Object>> getProjectData(@Query("user_id") String user_id);

    @POST("/ranu/index.php?action=saveProjectForm")
    Call<HashMap<String, Object>> SubmitForm(@Query("user_id") String user_id,@Query("project_id") String project_id,@Query("type") String type,@Query("location") String location,@Query("form_id") String form_id,@Query("unique_id") String unique_id,@Query("value") String value);

}
