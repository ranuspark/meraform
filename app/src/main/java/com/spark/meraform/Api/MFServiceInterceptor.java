package com.spark.meraform.Api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class MFServiceInterceptor implements Interceptor {
    private final String mAccountToken;
    private final String mApplicationBuildNumber;
    private final String mApplicationVersion;
    private final String mDeviceID;
    private final String mDeviceManufacturer;
    private final String mDeviceModel;
    private final String mPlatformVersion;
    private final String mUserAgent;


    public MFServiceInterceptor(String userAgent, String accountToken, String applicationVersion, String applicationBuildNumber, String deviceID, String deviceModel, String deviceManufacturer, String platformVersion) {
        this.mUserAgent = userAgent;
        this.mAccountToken = accountToken;
        this.mApplicationVersion = applicationVersion;
        this.mApplicationBuildNumber = applicationBuildNumber;
        this.mDeviceID = deviceID;
        this.mDeviceModel = deviceModel;
        this.mDeviceManufacturer = deviceManufacturer;
        this.mPlatformVersion = platformVersion;
    }

    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request ();
        return chain.proceed ( original.newBuilder ().header ( "User-Agent", this.mUserAgent ).header ( "Accept", "application/json" ).header ( "X-Platform", "Android" ).header ( "X-ApiToken", this.mAccountToken ).header ( "X-Application-Version", this.mApplicationVersion ).header ( "X-Application-Build-Number", this.mApplicationBuildNumber ).header ( "X-Device-Identifier", this.mDeviceID ).header ( "X-Device-Model", this.mDeviceModel ).header ( "X-Device-Manufacturer", this.mDeviceManufacturer ).header ( "X-Platform-Version", this.mPlatformVersion ).method ( original.method (), original.body () ).build () );
    }
}

