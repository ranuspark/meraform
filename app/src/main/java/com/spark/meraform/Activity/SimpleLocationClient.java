package com.spark.meraform.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class SimpleLocationClient implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    private static final String DEBUG_TAG = SimpleLocationClient.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationListener mLocationListener;
    private final LocationRequest mLocationRequest;
    private boolean mUpdatesEnabled;
    Context mcontext;

    public SimpleLocationClient(Context context, LocationRequest locationRequest) {
        this.mGoogleApiClient = new Builder(context, this, this).addApi(LocationServices.API).build();
        mcontext=context;
        if (locationRequest == null) {
            this.mLocationRequest = LocationRequest.create();
            this.mLocationRequest.setPriority(100);
            this.mLocationRequest.setInterval(0);
            return;
        }
        this.mLocationRequest = locationRequest;
    }


    public void onConnected(Bundle extras) {
        if (this.mUpdatesEnabled) {
            if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        }
    }

    public void onConnectionSuspended(int cause) {
        String causeString;
        if (cause == 2) {
            causeString = "CAUSE_NETWORK_LOST";
        } else if (cause == 1) {
            causeString = "CAUSE_SERVICE_DISCONNECTED";
        } else {
            causeString = String.valueOf(cause);
        }
        Log.d(DEBUG_TAG, "onConnectionSuspended: " + causeString);
    }

    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.d(DEBUG_TAG, "onConnectionFailed: " + result.getErrorCode());
    }

    public void onLocationChanged(Location location) {
        this.mLastLocation = location;
        if (this.mLocationListener != null) {
            this.mLocationListener.onLocationChanged(location);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public void connect() {
        this.mGoogleApiClient.connect();
    }

    public void disconnect() {
        ceaseUpdates();
        this.mGoogleApiClient.disconnect();
    }

    public void beginUpdates() {
        this.mUpdatesEnabled = true;
        if (this.mGoogleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        } else {
            this.mGoogleApiClient.connect();
        }
    }

    public void ceaseUpdates() {
        this.mUpdatesEnabled = false;
        if (this.mGoogleApiClient.isConnected()) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (com.google.android.gms.location.LocationListener) mcontext);
            } catch (Throwable ex) {
                // GISLogger.log(ex);
                Log.e("Exception", ex.toString());
            }
        }
    }

    public Location getLastLocation() {

        @SuppressLint("MissingPermission") Location location = LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        if (location == null) {
            return this.mLastLocation;
        }
        return location;

    }

    public void setListener(LocationListener listener) {
        this.mLocationListener = listener;
    }
}

