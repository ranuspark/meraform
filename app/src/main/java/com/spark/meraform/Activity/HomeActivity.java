package com.spark.meraform.Activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.spark.meraform.Adapter.HomeAdapter;
import com.spark.meraform.Api.Client;
import com.spark.meraform.Fragment.FormPickerFragment;
import com.spark.meraform.Fragment.RecordEditorFragment;
import com.spark.meraform.Fragment.RecordListFragment;
import com.spark.meraform.Fragment.RecordMapFragment;
import com.spark.meraform.Model.Form;
import com.spark.meraform.Model.Project;
import com.spark.meraform.Model.Project_detail;
import com.spark.meraform.Model.SpinerDTO;
import com.spark.meraform.R;
import com.spark.meraform.Utility.CursorUtils;
import com.spark.meraform.Utility.JSONUtils;
import com.spark.meraform.Utility.NetworkUtils;
import com.spark.meraform.Utility.SharedPreferencesAdapter;
import com.spark.meraform.database.DatabaseHandler;
import com.spark.meraform.database.MF;
import com.spark.meraform.database.PersistentStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

public class HomeActivity extends AppCompatActivity implements DrawerLayout.DrawerListener,FormPickerFragment.FormPickerFragmentListener {
    RecyclerView recyclerView;
    FloatingActionButton fab;
    HomeAdapter homeAdapter;
    ArrayList<SpinerDTO> homelist=new ArrayList<>();
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    DatabaseHandler db;
    Context context;
    private Toolbar mToolbar;
    private FormPickerFragment mFormPickerFragment;
    private DrawerLayout mFormDrawer;
    ArrayList<Project_detail> pro_list=new ArrayList<>();
    ArrayList<Form> form_list=new ArrayList<>();
    public static final int VIEW_MODE_LIST = 2;
    public static final int VIEW_MODE_MAP = 1;
    private int mActiveViewMode;
    private MenuItem mViewToggleItem;
    private String SelectProId;
    FragmentManager fm;

      /*  private final ServiceConnection mSyncConnection = new ServiceConnection () {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                if (binder instanceof SyncServiceBinder) {
                    SyncService svc = ((SyncServiceBinder) binder).getService();
                    svc.addListener(HomeActivity.this);
                    if (svc.isSynchronizing()) {
                        HomeActivity.this.showSyncAnimation();
                    } else {
                        HomeActivity.this.hideSyncAnimation();
                    }
                    HomeActivity.this.mSyncService = svc;
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                if (HomeActivity.this.mSyncService != null) {
                    HomeActivity.this.mSyncService.removeListener(HomeActivity.this);
                }
                HomeActivity.this.mSyncService = null;
            }
        };*/

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_dashboard);
            this.mToolbar = findViewById(R.id.toolbar);
            setSupportActionBar(this.mToolbar);
//            this.mActiveFormID = PatronSettings.getActiveFormID(this);
//            this.mActiveFormID = PatronSettings.getActiveFormID(this);
            this.mActiveViewMode = SharedPreferencesAdapter.getActiveViewMode(this, 2);
             fm = getSupportFragmentManager();
//            if (savedInstanceState != null) {
//                this.mSearchQuery = savedInstanceState.getString(STATE_SEARCH_QUERY);
//                this.mSearchVisible = savedInstanceState.getBoolean(STATE_SEARCH_VISIBLE, false);
//            }
            Fragment collectionView = fm.findFragmentById(R.id.container);
//            if (collectionView instanceof RecordCollectionView) {
//                this.mCollectionView = (RecordCollectionView) collectionView;
//            }
          //  configureCollectionView();
            updateFormButton(getResources().getString(R.string.app_name));

            if(NetworkUtils.isConnected(this))
            {
                getProjectData();
            }else {
                Toast.makeText(this, "Network is not Available", Toast.LENGTH_SHORT).show();
            }


           // Bundle bundle = new Bundle();
           // bundle.putSerializable("project_list", pro_list);
            this.mFormPickerFragment = (FormPickerFragment) getSupportFragmentManager().findFragmentById(R.id.form_picker_drawer);
          //  this.mFormPickerFragment.setArguments(bundle);

            this.mFormDrawer = findViewById(R.id.drawer_layout);
            this.mFormDrawer.addDrawerListener(HomeActivity.this);
            db=new DatabaseHandler(HomeActivity.this);
           // db.DeleteProject();
           // db.FetchProject();
          //  db.DeleteForm();

           // db.fetchProid();
          // reloadForms();
            if(SelectProId==null)
            {
                SelectProId="1";
            }
            Bundle bundle=new Bundle();
            bundle.putString("SelectProId", SelectProId);
            Log.e("SelectProIdv", SelectProId);

//        Go for Recylerview show data i list form
//                FragmentTransaction ft = fm.beginTransaction();
//                RecordListFragment list = new RecordListFragment();
//                list.setArguments(bundle);
//                ft.replace(R.id.container, list);
//                ft.commit();

            FragmentTransaction ft = fm.beginTransaction();
            RecordEditorFragment list = new RecordEditorFragment();
            list.setArguments(bundle);
            ft.replace(R.id.container, list);
            ft.commit();


        }

   /*    public boolean onCreateOptionsMenu(Menu menu) {
            hideSyncAnimation();
            getMenuInflater().inflate(R.menu.activity_dashboard, menu);//
            this.mAdvance_s = menu.findItem(R.id.search1);
            this.mFilterItem = menu.findItem(R.id.menu_item_filter);
            this.mViewToggleItem = menu.findItem(R.id.menu_item_view_toggle);
            this.mSearchRecordsItem = menu.findItem(R.id.menu_item_search);
            MenuItem synchronizeItem = menu.findItem(R.id.menu_item_synchronize);
            final SearchRelativeLayout searchView = (SearchRelativeLayout) MenuItemCompat.getActionView(this.mSearchRecordsItem);
            this.mSearchActionView = searchView.getSearchEditText();
            searchView.setOnSearchQueryChangedListener ( new SearchRelativeLayout.OnSearchQueryChangedListener () {
                @Override
                public void onSearchQueryChanged(String query) {
                    DashboardActivity.this.mSearchQuery = query;
                    if (DashboardActivity.this.mCollectionView != null) {
                        DashboardActivity.this.mCollectionView.setSearchQuery(query);
                    }
                }
            } );

            MenuItemCompat.setOnActionExpandListener(this.mSearchRecordsItem, new MenuItemCompat.OnActionExpandListener() {
                public boolean onMenuItemActionExpand(MenuItem item) {
                    DashboardActivity.this.mSearchVisible = true;
                    searchView.onMenuItemActionExpand(item);
                    return true;
                }

                public boolean onMenuItemActionCollapse(MenuItem item) {
                    DashboardActivity.this.mSearchVisible = false;
                    searchView.onMenuItemActionCollapse(item);
                    return true;
                }
            });

            this.mSearchActionView.setText(this.mSearchQuery);
            if (this.mSearchVisible) {
                this.mSearchRecordsItem.expandActionView();
                if (this.mSearchQuery != null) {
                    this.mSearchActionView.setSelection(this.mSearchQuery.length());
                }
            }
            this.mSyncActionView = MenuItemCompat.getActionView(synchronizeItem);
            this.mSyncActionView.setOnClickListener ( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DashboardActivity.this.onSynchronizeDataOptionSelected();
                }
            } );
            if (isCurrentlySynchronizing()) {
                showSyncAnimation();
            } else {
                hideSyncAnimation();
            }
            reloadMenuItemVisibility(this.mMenuItemVisibility);
            return true;
        }

        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.search1:
                    onAdvanceSearch();
                    return true;
                case R.id.menu_item_synchronize:
                    onSynchronizeDataOptionSelected();
                    return true;
                case R.id.menu_item_view_toggle:
                    onToggleViewModeOptionSelected();
                    return true;
                case R.id.menu_item_settings:
                    onAppSettingsMenuItemSelected();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }

        protected void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putString(STATE_SEARCH_QUERY, this.mSearchQuery);
            outState.putBoolean(STATE_SEARCH_VISIBLE, this.mSearchVisible);
        }

        protected void onStart() {
            super.onStart();
            this.mSyncServiceBound = SyncService.bind(this, this.mSyncConnection);
        }

        protected void onResume() {
            super.onResume();
            GIS.activityResumed();
            if (this.mSyncResultWhileAway != null) {
                if (((Boolean) this.mSyncResultWhileAway.first).booleanValue()) {
                    onSyncFinished();
                } else {
                    onSyncFailed((String) this.mSyncResultWhileAway.second);
                }
                this.mSyncResultWhileAway = null;
            }
        }

        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (resultCode == -1) {
                switch (requestCode) {
                    case ActivityRequestCodes.REQUEST_RECORD_BARCODE:
                        String contents = data.getStringExtra( Intents.Scan.RESULT);
                        if (contents != null) {
                            if (this.mSearchActionView == null) {
                                this.mSearchQuery = contents;
                                break;
                            }
                            this.mSearchActionView.setText(contents);
                            this.mSearchActionView.setSelection(contents.length());
                            break;
                        }
                        break;
                    case ActivityRequestCodes.REQUEST_APP_GALLERY:
                        onSynchronizeDataOptionSelected();
                        break;
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }

        public void onBackPressed() {
            if (!this.mFormDrawer.isDrawerOpen((int) GravityCompat.START) || this.mFormDrawer.getDrawerLockMode((int) GravityCompat.START) == 2) {
                super.onBackPressed();
            } else {
                this.mFormDrawer.closeDrawer((int) GravityCompat.START);
            }
        }

        protected void onPause() {
            super.onPause();
            GIS.activityPaused();
        }

        protected void onStop() {
            super.onStop();
            if (this.mSyncServiceBound) {
                this.mSyncServiceBound = false;
                unbindService(this.mSyncConnection);
            }
        }

        protected void onDestroy() {
            super.onDestroy();
            if (this.mFormDrawer != null) {
                this.mFormDrawer.removeDrawerListener(this);
            }
        }*/

       public void onFormSelected(long selectedFormID) {
           // Form form = Form.getForm(selectedFormID);
            try {
                //this.mActiveFormID = form.getRowID();
               // this.mAccountID = form.getAccountID();
                updateFormButton(getResources().getString(R.string.app_name));
            } catch (NullPointerException e) {
               // GISLogger.log("No form found for selectedFormID: " + selectedFormID);
            }
//            PatronSettings.setActiveFormID(this, this.mActiveFormID);
//            if (this.mCollectionView != null) {
//                this.mCollectionView.setFormID(this.mActiveFormID);
//            }
            SelectProId = String.valueOf(selectedFormID);
           Bundle bundle=new Bundle();
           bundle.putString("SelectProId", SelectProId);
//           FragmentTransaction ft = fm.beginTransaction();
//           RecordListFragment list = new RecordListFragment();
//           list.setArguments(bundle);
//           ft.replace(R.id.container, list);
//           ft.commit();

           FragmentTransaction ft = fm.beginTransaction();
           RecordEditorFragment list = new RecordEditorFragment();
           list.setArguments(bundle);
           ft.replace(R.id.container, list);
           ft.commit();
            this.mFormDrawer.setDrawerLockMode(0);
            this.mFormDrawer.closeDrawer(GravityCompat.START);

        }

       /* public void onSyncFailed(String reason) {
            if (!GIS.isActivityVisible()) {
                this.mSyncResultWhileAway = new Pair(Boolean.valueOf(false), reason);
            }
        }

        public void onSyncFinished() {
            if (GIS.isActivityVisible()) {
                reloadForms();
                if (this.mCollectionView != null) {
                    this.mCollectionView.onSyncFinished();
                    this.mCollectionView.reloadData();
                }
                hideSyncAnimation();
                Intent oldIntent = getIntent();
                if (oldIntent.hasExtra(EXTRA_ACTION)) {
                    Intent newIntent = new Intent(this, RecordEditorActivity.class);
                    newIntent.putExtras(oldIntent);
                    startActivity(newIntent);
                    finish();
                    return;
                }
                return;
            }
            this.mSyncResultWhileAway = new Pair(Boolean.valueOf(true), null);
        }

        public void onRecordSelected(long recordID) {
            if (isCurrentlySynchronizing()) {
                AlertUtils.showSynchronizingAlert(this);
            } else {
                startActivity(RecordEditorActivity.getEditRecordIntent(this, recordID));
            }
        }

        public void onShowRecordLocation(long recordID, double latitude, double longitude) {
            LatLng location = new LatLng(latitude, longitude);
            if (this.mActiveViewMode != VIEW_MODE_MAP) {
                setViewMode(VIEW_MODE_MAP, false);
            }
            ((MapViewFragment) this.mCollectionView).zoomToFeature(recordID, location);
        }

        public void onDuplicateRecord(long recordID, boolean location, boolean values) {
            if (isCurrentlySynchronizing()) {
                AlertUtils.showSynchronizingAlert(this);
            } else {
                startActivity(RecordEditorActivity.getDuplicateRecordIntent(this, recordID, location, values));
            }
        }

        public void onGenerateReport(long record) {
        }

        public void onDeleteRecord(long recordID) {
            if (isCurrentlySynchronizing()) {
                AlertUtils.showSynchronizingAlert(this);
                return;
            }
            Record record = Record.getRecord(recordID);
            if (record != null) {
                record.deleteWithTransaction();
                Toast.makeText(this, R.string.record_deleted_success, Toast.LENGTH_SHORT).show();
                if (this.mCollectionView != null) {
                    this.mCollectionView.reloadData();
                }
                Form form = record.getForm();
                if (form != null) {
                    form.decrementRecordCount();
                }
            }
            TrashCan.empty(this);
        }

        public void onNewRecordOptionSelected() {
            Account account = Account.getActiveAccount();
            if (account == null) {
                return;
            }
            if (!account.getRole().canCreateRecords()) {
                AlertUtils.showPermissionsAlert(this, Role.PERMISSION_CREATE_RECORDS);
            } else if (isCurrentlySynchronizing()) {
                AlertUtils.showSynchronizingAlert(this);
            } else {
                startActivity(RecordEditorActivity.getNewRecordIntent(this, getActiveFormID()));
            }
        }*/

        public void onDrawerSlide(View drawerView, float slideOffset) {
        }

        public void onDrawerOpened(View drawerView) {
            //logDebugEvent("onDrawerOpened -> formPickerFragment.reloadForms()");
         //   this.mFormPickerFragment.reloadForms();
        }

        public void onDrawerClosed(View drawerView) {
           // this.mFormPickerFragment.clearSearch();
        }

        public void onDrawerStateChanged(int newState) {
        }

      /*  public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new FormLoader(this, "_id", "name");
        }

        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            onFormsCursorLoaded(data);
        }

        public void onLoaderReset(Loader<Cursor> loader) {
        }

        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            int i = 0;
            boolean granted;
            int length;
            if ((requestCode & 64) == 64) {
                if (grantResults.length > 0) {
                    granted = true;
                    length = grantResults.length;
                    while (i < length) {
                        if (grantResults[i] != 0) {
                            granted = false;
                        }
                        i++;
                    }
                    if (granted) {
                        Fragment frag = getSupportFragmentManager().findFragmentById(R.id.container);
                        if (frag != null && (frag instanceof MapViewFragment)) {
                            ((MapViewFragment) frag).setMyLocationEnabled(null);
                        }
                    }
                }
            } else if ((requestCode & 16) == 16) {
                if (grantResults.length > 0) {
                    granted = true;
                    for (int result : grantResults) {
                        if (result != 0) {
                            granted = false;
                        }
                    }
                    if (granted) {
                        return;
                    }
                }
                Toast.makeText(this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
            }
        }

        private void onSynchronizeDataOptionSelected() {
            if (!isCurrentlySynchronizing()) {
                showSyncAnimation();
                SyncService.synchronizeData(this, Account.getActiveAccount());
            }
        }

        private void onAdvanceSearch() {
//        if (!isCurrentlySynchronizing()) {
//            showSyncAnimation();
//            SyncService.synchronizeData(this, Account.getActiveAccount());
//        }
            Intent intent = new Intent(DashboardActivity.this,Search_Activity.class);
            intent.putExtra("project_Id", mActiveFormID);
            intent.putExtra("account_Id", mAccountID);
            intent.putExtra("project_name", project_name);
            startActivity(intent);

        }

        private void showSyncAnimation() {
            if (this.mSyncActionView != null) {
                Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
                rotation.setRepeatCount(-1);
                this.mSyncActionView.startAnimation(rotation);
            }
            ((TextView) findViewById(R.id.empty_text)).setText(R.string.synchronizing_forms_message);
            this.mFormPickerFragment.setSynchronizing(true);
        }

        private void hideSyncAnimation() {
            if (this.mSyncActionView != null) {
                this.mSyncActionView.clearAnimation();
            }
            ((TextView) findViewById(R.id.empty_text)).setText(R.string.missing_forms_message);
            this.mFormPickerFragment.setSynchronizing(false);
        }*/

       /* private void onToggleViewModeOptionSelected() {
            switch (this.mActiveViewMode) {
                case VIEW_MODE_MAP:
                    setViewMode(VIEW_MODE_LIST);
                    return;

                case VIEW_MODE_LIST:
                    setViewMode(VIEW_MODE_MAP);
                    return;
                default:
                    return;
            }
        }*/


     /*   private void onAppSettingsMenuItemSelected() {
            SettingsActivity.start(this);
        }


        private long getActiveFormID() {
            return this.mActiveFormID;
        }*/
/* Active Code 22-01-2019 abhi hide kiya hai
        private void setViewMode(int mode) {
            setViewMode(mode, true);
        }

        private void setViewMode(int mode, boolean async) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            switch (mode) {
                case VIEW_MODE_MAP : //1
                   if (this.mViewToggleItem != null) {
                        this.mViewToggleItem.setIcon(R.drawable.ic_action_list);
                        this.mViewToggleItem.setTitle(R.string.list_view);
                    }
                 //   if (!(this.mCollectionView instanceof RecordMapFragment)) {
                        RecordMapFragment map = new RecordMapFragment();
                        ft.replace(R.id.container, map);
                      //  this.mCollectionView = map;
                        break;
                 //   }
                    //break;
                case VIEW_MODE_LIST : //2
                    if (this.mViewToggleItem != null) {
                        this.mViewToggleItem.setIcon(R.drawable.ic_action_map);
                        this.mViewToggleItem.setTitle(R.string.map_view);
                    }
                  //  if (!(this.mCollectionView instanceof RecordListFragment)) {
                        RecordListFragment list = new RecordListFragment();
                        ft.replace(R.id.container, list);
                      //  this.mCollectionView = list;
                        break;
                 //   }
                    //break;
            }
            try {
                ft.commitAllowingStateLoss();
            } catch (IllegalStateException e) {
            }
          //  configureCollectionView();
            this.mActiveViewMode = mode;
            if (!async) {
                fm.executePendingTransactions();
            }
            SharedPreferencesAdapter.setActiveViewMode(this, mode);
        } */

    /*    private void removeCollectionView() {
            FragmentManager fm = getSupportFragmentManager();
            Fragment frag = fm.findFragmentById(R.id.container);
            if (frag != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.remove(frag);
                ft.commitAllowingStateLoss();
            }
            this.mCollectionView = null;
        }

        private void configureCollectionView() {
            if (this.mCollectionView != null) {
                this.mCollectionView.setFormID(getActiveFormID());
            }
        }

        private void reloadForms() {
            logDebugEvent("reloadForms()");
            this.mFormPickerFragment.reloadForms();
            getSupportLoaderManager().restartLoader(100, null, this);
        }


        private void onFormsCursorLoaded(Cursor cursor) {
            logDebugEvent("onFormsCursorLoaded()");
            View empty = findViewById(R.id.empty);
            if (cursor == null || cursor.getCount() <= 0) {
                this.mToolbar.setLogo(null);
                setTitle(R.string.GIS);
                this.mFormDrawer.setDrawerLockMode(VIEW_MODE_MAP);
                if (empty != null) {
                    empty.setVisibility(View.VISIBLE);
                }
                removeCollectionView();
                this.mMenuItemVisibility = false;
                Account account = Account.getActiveAccount();
                if (account == null || !account.getRole().isOwner()) {
                    this.mToolbar.setNavigationIcon(null);
                } else {
                    this.mToolbar.setNavigationIcon((int) R.drawable.ic_action_menu);
                    this.mToolbar.setNavigationOnClickListener ( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DashboardActivity.this.mFormDrawer.openDrawer((int) GravityCompat.START);
                        }
                    });
                    if (!(this.mSyncService == null || this.mSyncService.isSynchronizing())) {
                        this.mFormDrawer.openDrawer((int) GravityCompat.START);
                        AppGalleryActivity.startForResult(this);
                    }
                }
            } else {
                boolean found = false;
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    if (CursorUtils.getLong(cursor, "_id") == this.mActiveFormID) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    updateFormButton(CursorUtils.getString(cursor, "name"));
                    // project_name=CursorUtils.getString(cursor, "name");
                } else {
                    this.mFormDrawer.setDrawerLockMode(VIEW_MODE_LIST);
                    this.mFormDrawer.openDrawer((int) GravityCompat.START);
                }
                if (empty != null) {
                    empty.setVisibility(View.INVISIBLE);
                }
                setViewMode(this.mActiveViewMode);
                this.mMenuItemVisibility = true;
            }
            reloadMenuItemVisibility(this.mMenuItemVisibility);
        }

        private void reloadMenuItemVisibility(boolean menuItemVisibility) {
            if (this.mViewToggleItem != null) {
                this.mViewToggleItem.setVisible(menuItemVisibility);
                this.mViewToggleItem.setEnabled(menuItemVisibility);
                if (this.mActiveViewMode == VIEW_MODE_MAP) {
                    this.mViewToggleItem.setIcon(R.drawable.ic_action_list);
                    this.mViewToggleItem.setTitle(R.string.list_view);
                } else {
                    this.mViewToggleItem.setIcon(R.drawable.ic_action_map);
                    this.mViewToggleItem.setTitle(R.string.map_view);
                }
            }
            if (this.mSearchRecordsItem != null) {
                this.mSearchRecordsItem.setVisible(menuItemVisibility);
                this.mSearchRecordsItem.setEnabled(menuItemVisibility);
            }
        }

        private boolean isCurrentlySynchronizing() {
            return this.mSyncService != null && this.mSyncService.isSynchronizing();
        }*/


    // Active Code 22-01-2019 abhi hide kiya hai
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_location, menu);//
        MenuItem map_loc = menu.findItem(R.id.map_loc);
//        MenuItem sync = menu.findItem(R.id.sync);
//        this.mViewToggleItem = menu.findItem(R.id.menu_item_view_toggle);
//        MenuItem settings = menu.findItem(R.id.setting);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.map_loc:
                    onSetLocation();
                    return true;
        /*    case R.id.sync:
              //  onSynchronizeDataOptionSelected();
                return true;
            case R.id.menu_item_view_toggle:
                onToggleViewModeOptionSelected();
                return true;
            case R.id.setting:
              //  onAppSettingsMenuItemSelected();
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void onSetLocation()
    {
       startActivity(new Intent(HomeActivity.this,MapActivity.class));

    }
    private void updateFormButton(String appName) {
            setTitle(appName);
          //  project_name=appName;
            this.mToolbar.setNavigationIcon(R.drawable.ic_action_menu);
            this.mToolbar.setNavigationContentDescription(R.string.assigned_form);
            this.mToolbar.setNavigationOnClickListener (view -> HomeActivity.this.mFormDrawer.openDrawer(GravityCompat.START));
        }

       public void getProjectData() {
           Client _client = new Client (this );
           try {
              String user_id =SharedPreferencesAdapter.getPreferences(HomeActivity.this).getString("userId",null );
               _client.getProjectData (user_id).enqueue (new Callback<HashMap<String, Object>>() {
                   @Override
                   public void onResponse(Call<HashMap<String, Object>> call, retrofit2.Response<HashMap<String, Object>> response) {
                       HashMap<String, Object> _response = response.body();
                       // Log.e("Map value11", _response.toString());
                       Log.e("Map11 ", "hfg" + _response);
                       Project_detail project_detail=new Project_detail();
                       Form form=new Form();

                           int mStatus = JSONUtils.getInteger(_response, "success");
                           if (mStatus == 1) {

                               String message = JSONUtils.getString(_response, "message");
                               ArrayList jsonArray=JSONUtils.getArrayList(_response, "data");
                               for (int i=0;i<jsonArray.size();i++) {

                                   HashMap<String, Object> jsonObjectMap = JSONUtils.getHashMap(jsonArray, i);


                                   String form_id = JSONUtils.getString(jsonObjectMap, "form_id");
                                   String form_name = JSONUtils.getString(jsonObjectMap, "form_name"); //
                                   String pro_id = JSONUtils.getString(jsonObjectMap, "project_id");
                                   String pro_name = JSONUtils.getString(jsonObjectMap, "project_name"); //
                                   String description = JSONUtils.getString(jsonObjectMap, "description");
                                   int count = JSONUtils.getInt(jsonObjectMap, "count");
                                   String created_at = JSONUtils.getString(jsonObjectMap, "created_at");
                                   project_detail.setPro_id(pro_id);
                                   project_detail.setPro_name(pro_name);
                                   project_detail.setPro_des(description);
                                   project_detail.setCount(count);
                                   project_detail.setPro_create(created_at);
                                   pro_list.add(project_detail);

                                   db.AddProjectUpdate(project_detail);

                                   ArrayList jsonArray1 = JSONUtils.getArrayList(jsonObjectMap, "form");
                                   String strObject = null;
                                  // Log.e("aaaa", jsonArray1.toString());
                                   for (int j = 0; j < jsonArray1.size(); j++) {
                                       HashMap<String, Object> jsonObjectMap1 = JSONUtils.getHashMap(jsonArray1, j);

                                       int controlId_id = JSONUtils.getInteger(jsonObjectMap1, "ControlId");
                                       String ControlType = JSONUtils.getString(jsonObjectMap1, "ControlType");
                                       String IsMandatory = JSONUtils.getString(jsonObjectMap1, "IsMandatory");
                                       String ValidationName = JSONUtils.getString(jsonObjectMap1, "ValidationName");
                                       String DataType = JSONUtils.getString(jsonObjectMap1, "DataType");
                                       String Regex = JSONUtils.getString(jsonObjectMap1, "Regex");
                                       String ValidationMsg = JSONUtils.getString(jsonObjectMap1, "ValidationMessage");
                                       String FieldName = JSONUtils.getString(jsonObjectMap1, "FieldName");
                                       String LableName = JSONUtils.getString(jsonObjectMap1, "LableName");
                                       ArrayList DataList = JSONUtils.getArrayList(jsonObjectMap1, "DataList");

                                       form.setControl_Id(controlId_id);
                                       form.setControlType(ControlType);
                                       form.setIsMandatory(IsMandatory);

                                       form.setDataType(DataType);
                                       form.setRegex(Regex);
                                       form.setValidationMsg(ValidationMsg);

                                       form.setFieldName(FieldName);
                                       form.setLableName(LableName);
                                       form.setDataList(DataList);

                                       form_list.add(form);


                                       //Log.e("Control Detail", controlId_id + " " + ControlType + " " + IsMandatory + " " + ValidationName);

                                       Gson objGson= new Gson();
                                        strObject = objGson.toJson(jsonArray1);
                                    //   Log.e("GJson", strObject);
//                                       Bundle bb=new Bundle();
//                                       bb.putString("key", strObject);
//
//
//                                       Gson gson = new Gson();
//                                       Type type = new TypeToken<ColorSpace.Model>() {}.getType();
//                                      String myMarkersHash = gson.fromJson(bb.getString("key"), type);
//
//                                       Log.e("Return GJson", myMarkersHash);

                                   }
                                 //  Log.e("GJson11", strObject);

                                   db.AddFormUp(pro_id,form_id,form_name, strObject, created_at);
                               }

                           }




                   }
                   @Override
                   public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                   }
               } );
           } catch (NullPointerException e) {
               Log.e("Exception", e.toString());
           }
       }



}
