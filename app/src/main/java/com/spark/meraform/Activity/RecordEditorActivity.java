package com.spark.meraform.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.spark.meraform.Fragment.RecordEditorFragment;
import com.spark.meraform.Fragment.RecordListFragment;
import com.spark.meraform.R;

public class RecordEditorActivity extends AppCompatActivity {
    FragmentManager fm;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);


        fm = getSupportFragmentManager();

        Fragment collectionView = fm.findFragmentById(R.id.container);

        Intent intent = getIntent();
        String proId = intent.getStringExtra("pro_id");

        Bundle bundle=new Bundle();
        bundle.putString("SelectProId", proId);

        FragmentTransaction ft = fm.beginTransaction();
        RecordEditorFragment list = new RecordEditorFragment();
        list.setArguments(bundle);
        ft.replace(R.id.container, list);
        ft.commit();

    }
}
