package com.spark.meraform.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.spark.meraform.Adapter.AlertAdapter;
import com.spark.meraform.Adapter.AlertAdapter1;
import com.spark.meraform.Adapter.ImageAdapter;
import com.spark.meraform.Interface.getValue;
import com.spark.meraform.Model.DynamicDTO;
import com.spark.meraform.Model.SpinerDTO;
import com.spark.meraform.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.JsonObject;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class DashboardActivity1 extends AppCompatActivity implements getValue {

    String jsonString = "{\n" +
            "    \"d\": {\n" +
            "        \"version\": \"2.2.3\",\n" +
            "        \"Coupon\": [\n" +
            "            {\n" +
            "                \"FormID\": \"1\",\n" +
            "                \"FormTitle\": \"NTTopup\",\n" +
            "                \"Controls\": [\n" +
            "                    {\n" +
            "                        \"ControlId\": 1,\n" +
            "                        \"ControlType\": \"dropdown\",\n" +
            "                        \"IsMandatory\": \"false\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
            "                        \"DataType\": \"text\",\n" +
            "                        \"Regex\": \"'^[\\s\\t\\r\\n]*\\S+\",\n" +
            "                        \"ValidationMessage\": \"This Field is Required\",\n" +
            "                        \"FieldName\": \"deno\",\n" +
            "                        \"LableName\": \"Coupon Denomination\",\n" +
            "                        \"DataList\": \"200,500\"\n" +
            "                          },\n" +
            "                          {\n" +
            "                        \"ControlId\": 2,\n" +
            "                        \"ControlType\": \"dropdown\",\n" +
            "                        \"IsMandatory\": \"false\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
            "                        \"DataType\": \"text\",\n" +
            "                        \"Regex\": \"'^[\\s\\t\\r\\n]*\\S+\",\n" +
            "                        \"ValidationMessage\": \"This Field is Required\",\n" +
            "                        \"FieldName\": \"denodeno\",\n" +
            "                        \"LableName\": \"Money\",\n" +
            "                        \"DataList\": \"2000,5000\"\n" +
            "                          },\n" +
            "                   {\n" +
            "                        \"ControlId\": 3,\n" +
            "                        \"ControlType\": \"textbox\",\n" +
            "                        \"IsMandatory\": \"false\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
            "                        \"Regex\": \"'^[\\s\\t\\r\\n]*\\S+\",\n" +
            "                        \"ValidationMessage\": \"4 Digit Number Required\",\n" +
            "                        \"DataType\": \"numericPassword\",\n" +
            "                        \"FieldName\": \"pin\",\n" +
            "                        \"LableName\": \"PIN\",\n" +
            "                        \"DataList\": \"\"\n" +
            "                          }\n" +
            "                           ,\n" +
            "                          {\n" +
            "                        \"ControlId\": 4,\n" +
            "                        \"ControlType\": \"dropdown\",\n" +
            "                        \"IsMandatory\": \"false\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
            "                        \"DataType\": \"text\",\n" +
            "                        \"Regex\": \"'^[\\s\\t\\r\\n]*\\S+\",\n" +
            "                        \"ValidationMessage\": \"This Field is Required\",\n" +
            "                        \"FieldName\": \"state\",\n" +
            "                        \"LableName\": \"District\",\n" +
            "                        \"DataList\": \"Lalitpur, Bhaktapur, Kathmandu, Biratnagar, Pokhara\"\n" +
            "                          }\n" +
            "                          ,\n" +
            "                           {\n" +
            "                        \"ControlId\": 5,\n" +
            "                        \"ControlType\": \"textbox\",\n" +
            "                        \"IsMandatory\": \"false\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
            "                        \"Regex\": \"'^[\\s\\t\\r\\n]*\\S+\",\n" +
            "                        \"ValidationMessage\": \"This Field is Required\",\n" +
            "                        \"DataType\": \"numberDecimal\",\n" +
            "                        \"FieldName\": \"amount\",\n" +
            "                        \"LableName\": \"Amount\",\n" +
            "                        \"DataList\": \"\"\n" +
            "                          },\n" +
            "                                                               \n" +
            "                          {\n" +
            "                        \"ControlId\": 8,\n" +
            "                        \"ControlType\": \"textbox\",\n" +
            "                        \"IsMandatory\": \"true\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
//            "                        \"Regex\": \"\\\\d{3}-\\\\d{7}\",\n" +
            "                        \"Regex\": \"\\\\d{3}\\\\d{7}\",\n" +
            "                        \"ValidationMessage\": \"###-#######\",\n" +
            "                        \"DataType\": \"phone\",\n" +
            "                        \"FieldName\": \"phone\",\n" +
            "                        \"LableName\": \"Phone Number\",\n" +
            "                        \"DataList\": \"\"\n" +
            "                          },\n" +
            "                            {\n" +
            "                      \"ControlId\": 9,\n" +
            "                        \"ControlType\": \"radio_btn\",\n" +
            "                        \"IsMandatory\": \"false\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
            "                        \"DataType\": \"text\",\n" +
            "                        \"Regex\": \"'^[\\s\\t\\r\\n]*\\S+\",\n" +
            "                        \"ValidationMessage\": \"This Field is Required\",\n" +
            "                        \"FieldName\": \"\",\n" +
            "                        \"LableName\": \"Select One Choice\",\n" +
            "                        \"DataList\": \"One, Two, Three, Four, Five\"\n" +
            "                          },\n" +
            "                             {\n" +
            "                      \"ControlId\": 10,\n" +
            "                        \"ControlType\": \"checkbox\",\n" +
            "                        \"IsMandatory\": \"false\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
            "                        \"DataType\": \"text\",\n" +
            "                        \"Regex\": \"'^[\\s\\t\\r\\n]*\\S+\",\n" +
            "                        \"ValidationMessage\": \"This Field is Required\",\n" +
            "                        \"FieldName\": \"\",\n" +
            "                        \"LableName\": \"Select One Choice with Checkbox\",\n" +
            "                        \"DataList\": \"India, China, America, Rush, Bangal\"\n" +
            "                          },\n" +
            "                             { \n" +
            "                        \"ControlId\": 11,\n" +
            "                        \"ControlType\": \"image\",\n" +
            "                        \"IsMandatory\": \"false\",\n" +
            "                        \"ValidationName\": \"Required\",\n" +
            "                        \"DataType\": \"text\",\n" +
            "                        \"Regex\": \"'^[\\s\\t\\r\\n]*\\S+\",\n" +
            "                        \"ValidationMessage\": \"This Field is Required\",\n" +
            "                        \"FieldName\": \"Image\",\n" +
            "                        \"LableName\": \"Image\",\n" +
            "                        \"DataList\": \"\"\n" +
            "                          }\n" +
            "\n" +
            "                          \n" +
            "                ]\n" +
            "            }\n" +
            "               ]\n" +
            "    }\n" +
            "}";
    LinearLayout parentLayout,LinearLay;
    ArrayList<DynamicDTO> dynamicList;
    ArrayList<String> smsArrayList;
    List<String> couponDenoList;
    DynamicDTO info;
    ArrayList<SpinerDTO> spinnerList=new ArrayList<>();
    ArrayList<SpinerDTO> checkboxList=new ArrayList<>();
    ArrayList<SpinerDTO> radiobtnList=new ArrayList<>();
    ArrayList<SpinerDTO> imageList=new ArrayList<>();

    JsonObject json = new JsonObject();
    RadioGroup radioGroup;
    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_REQUEST = 2;
    ImageView dynamic_Image;
    LinearLayout LL_horizo;
    int image_id;
    Context context;
    ImageAdapter imageAdapter;
  //  Bitmap myphoto ;
    RecyclerView recyclerView;
    EditText dynamic_editText11;
    StringBuffer result=new StringBuffer();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_activity);

        parentLayout = findViewById(R.id.linear_layout);

        ScrollView sv = new ScrollView(this);
        sv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        LinearLay = new LinearLayout(this);
        LinearLay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        LinearLay.setOrientation(LinearLayout.VERTICAL);
        sv.addView(LinearLay);
        parentLayout.addView(sv);
        CreateView1();
        LinearLay.addView(submitButton());




    }

    public void CreateView1() {
        try {
            JSONObject obj = new JSONObject(jsonString);
            JSONObject jsonObjectD = obj.getJSONObject("d");
            String version = jsonObjectD.getString("version");
            JSONArray mArray = jsonObjectD.getJSONArray("Coupon");

            for (int i = 0; i < mArray.length(); i++) {
                JSONObject jsonObject = mArray.getJSONObject(i);
                Log.i("object", jsonObject.toString());
                String FormId = jsonObject.getString("FormID");
                String FormTitle = jsonObject.getString("FormTitle");

                JSONArray merchantArray = jsonObject
                        .getJSONArray("Controls");
                if (merchantArray.length() > 0) {

                    dynamicList = new ArrayList<DynamicDTO>();

                    for (int m = 0; m < merchantArray.length(); m++) {

                        JSONObject jsonObjectM = merchantArray
                                .getJSONObject(m);

                        int ControlId = jsonObjectM.getInt("ControlId");
                        String ControlType = jsonObjectM.getString("ControlType");
                        String IsMandatory = jsonObjectM.getString("IsMandatory");
                        String ValidationName = jsonObjectM
                                .getString("ValidationName");
                        String Regex = jsonObjectM.getString("Regex");

                        String ValidationMessage = jsonObjectM
                                .getString("ValidationMessage");
                        String FieldName = jsonObjectM
                                .getString("FieldName");
                        String LableName = jsonObjectM
                                .getString("LableName");
                        String DataType = jsonObjectM.getString("DataType");

                        String DataList = jsonObjectM.getString("DataList");

//                        info = new DynamicDTO(ControlId, ControlType,
//                                IsMandatory, ValidationName, Regex,
//                                ValidationMessage, FieldName, LableName,
//                                DataList);
                        info = new DynamicDTO();
                        info.setControId(ControlId);
                        info.setControlType(ControlType);
                        info.setIsMandatory(IsMandatory);
                        info.setValidationName(ValidationName);
                        info.setRegex(Regex);
                        info.setValidationMessage(ValidationMessage);
                        info.setFieldName(FieldName);
                        info.setDataList(DataList);
                        info.setFormName(FormTitle);



                        if (ControlType.equalsIgnoreCase("textbox")) {

                            //creating dynamic edittext
                            dynamic_textView(LableName);
                            dynamic_editText(ControlId);


                        } else if (ControlType.equalsIgnoreCase("dropdown")) {


                            //creating dynamic spiner
                            couponDenoList = Arrays.asList(DataList.split("\\s*,\\s*"));

                            dynamic_textView(LableName);
                            Log.e("DropDown Value", String.valueOf(couponDenoList));
                            dynamic_spiner(ControlId, couponDenoList);

                        } else if (ControlType.equalsIgnoreCase("checkbox")) {


                            //creating dynamic spiner
                            couponDenoList = Arrays.asList(DataList.split("\\s*,\\s*"));

                            dynamic_textView(LableName);
                            Log.e("DropDown Value", String.valueOf(couponDenoList));
                            dynamic_checkbox(ControlId, couponDenoList);

                        }else if (ControlType.equalsIgnoreCase("radio_btn")) {

                            //creating dynamic edittext
                            couponDenoList = Arrays.asList(DataList.split("\\s*,\\s*"));
                            dynamic_textView(LableName);
                            dynamic_radiobutton(ControlId,couponDenoList);


                        }
                        else if (ControlType.equalsIgnoreCase("image")) {

                            //creating dynamic edittext
                          //  couponDenoList = Arrays.asList(DataList.split("\\s*,\\s*"));
                            dynamic_textView(LableName);
                            dynamic_image(ControlId);


                        }
                        dynamicList.add(info);
                        Log.e("Dyanamic List Size111", String.valueOf(dynamicList.size()));
                    }

                    //adding to all data in model class

                }

            }


            // }
        } catch (
                JSONException e)

        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


    // Return EditText
    private EditText dynamic_editText(int id) {

        final EditText dynamic_editText1 = new EditText(this);
        dynamic_editText1.setId(id);
        info.setEditText(dynamic_editText1);
        dynamic_editText1.setPadding(16, 16, 16, 16);
        dynamic_editText1.setInputType ( InputType.TYPE_CLASS_NUMBER );
        LinearLay.addView(dynamic_editText1);
        return dynamic_editText1;
    }

    // Return TextView
    private TextView dynamic_textView(String title) {
        TextView dynamic_textView1 = new TextView(this);
        dynamic_textView1.setText(Html.fromHtml(title));
        dynamic_textView1.setTypeface(null, Typeface.BOLD);
        dynamic_textView1.setPadding(0, 12, 0, 5);
        LinearLay.addView(dynamic_textView1);
        return dynamic_textView1;
    }

    // Return Spinner
    private Spinner dynamic_spiner(int id, List<String> couponlist) {
        Spinner dynamic_spinner1 = new Spinner(this);
        dynamic_spinner1.setId(id);
        LinearLayout.LayoutParams ll= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ll.setMargins(5, 5, 5, 5);

        dynamic_spinner1.setLayoutParams(ll);
        dynamic_spinner1.setBackgroundResource(R.drawable.rounded_edittext);
        getCouponDenoListAdapter(dynamic_spinner1, couponlist, id);
        info.setSpinner(dynamic_spinner1);
        LinearLay.addView(dynamic_spinner1);




        return dynamic_spinner1;
    }

    private  void dynamic_checkbox(int id,List<String> couponlist)
    {
          dynamic_editText11 = new EditText(this);
        dynamic_editText11.setId(id);
        info.setEditText(dynamic_editText11);
        dynamic_editText11.setPadding(16, 16, 16, 16);
      //  dynamic_editText11.setEnabled(false);
        dynamic_editText11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dynamic_checkbox1(id,couponlist);
            }
        });
        LinearLay.addView(dynamic_editText11);
    }
    private  void dynamic_checkbox1(int id,List<String> couponlist)
    {
       // final String[] items = {"Apple", "Banana", "Orange", "Grapes"};
        final ArrayList<Integer> selectedList = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("List of Items");

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alertrec, null);
        builder.setView(alertLayout);
        RecyclerView recyclerView=alertLayout.findViewById(R.id.rv);
        LinearLayoutManager liLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(liLayoutManager);
        //AlertAdapter alertAdapter=new AlertAdapter(couponlist, DashboardActivity1.this);

      //  recyclerView.setAdapter(alertAdapter);

//
//        LinearLayout linearLayout = findViewById(R.id.linear_layout);
//
//        Spinner dynamic_spinner1 = new Spinner(this);
//        dynamic_spinner1.setId(id);
//        LinearLayout.LayoutParams ll= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        ll.setMargins(5, 5, 5, 5);
//        dynamic_spinner1.setLayoutParams(ll);
//        dynamic_spinner1.setBackgroundResource(R.drawable.rounded_edittext);
//        getCouponDenoListAdapter(dynamic_spinner1, couponlist, id);
//        info.setSpinner(dynamic_spinner1);
//
//        linearLayout.addView(dynamic_spinner1);


        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ArrayList<String> selectedStrings = new ArrayList<>();
              //  selectedStrings.add(couponlist.get(i));
                for (int j = 0; j < couponlist.size(); j++) {
                    selectedStrings.add(couponlist.get(j));
                }

                Toast.makeText(getApplicationContext(), "Items selected are: " + selectedStrings.get(i), Toast.LENGTH_SHORT).show();


            }
        });

        builder.show();

    }


   /* private CheckBox dynamic_checkbox(int id, List<String> couponlist) {
        CheckBox dynamic_checkbox1 = new CheckBox(this);
        dynamic_checkbox1.setId(id);
        dynamic_checkbox1.setBackgroundResource(R.drawable.rounded_edittext);
        for (int i = 0; i < couponlist.size(); i++) {
            dynamic_checkbox1 = new CheckBox(this);
            dynamic_checkbox1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            dynamic_checkbox1.setText(couponlist.get(i));
            //dynamic_checkbox1.setBackgroundResource(R.drawable.rounded);
            dynamic_checkbox1.setId(i);
           dynamic_checkbox1.setChecked(false);
            LinearLay.addView(dynamic_checkbox1);

            ClickCheckBox(id, dynamic_checkbox1);
        }       // info.setSpinner(dynamic_spinner);
        return dynamic_checkbox1;
    }*/
    // Return Radio Button
   private  EditText dynamic_radiobutton(int id,List<String> couponlist)
   {
       dynamic_editText11 = new EditText(this);
       dynamic_editText11.setId(id);
       info.setEditText(dynamic_editText11);
       dynamic_editText11.setPadding(16, 16, 16, 16);
       //  dynamic_editText11.setEnabled(false);
       dynamic_editText11.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               dynamic_radiobutton1(id,couponlist);
           }
       });
       LinearLay.addView(dynamic_editText11);
       return dynamic_editText11;
   }
    private  void dynamic_radiobutton1(int id,List<String> couponlist)
    {
        // final String[] items = {"Apple", "Banana", "Orange", "Grapes"};
        final ArrayList<Integer> selectedList = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Radio Button");

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alertrec, null);
        builder.setView(alertLayout);
        RecyclerView recyclerView=alertLayout.findViewById(R.id.rv);
        LinearLayoutManager liLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(liLayoutManager);
      //  AlertAdapter1 alertAdapter=new AlertAdapter1(couponlist, DashboardActivity1.this);

     //   recyclerView.setAdapter(alertAdapter);



//        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                ArrayList<String> selectedStrings = new ArrayList<>();
//                //  selectedStrings.add(couponlist.get(i));
//                for (int j = 0; j < couponlist.size(); j++) {
//                    selectedStrings.add(couponlist.get(j));
//                }
//
//                Toast.makeText(getApplicationContext(), "Items selected are: " + selectedStrings.get(i), Toast.LENGTH_SHORT).show();
//
//
//            }
//        });

        builder.show();

    }
    /*private RadioButton dynamic_radiobutton(int id,List<String> couponlist) {
        RadioButton dynamic_radiobtn1 = null;
        radioGroup = new RadioGroup(this);
        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(15, 10, 15, 10);
        radioGroup.setLayoutParams(params);
        for (int i = 0; i < couponlist.size(); i++) {
             dynamic_radiobtn1 = new RadioButton(this);
           LinearLayout.LayoutParams ss= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            ss.setMargins(15, 10, 15, 10);
            dynamic_radiobtn1.setLayoutParams(ss);
            dynamic_radiobtn1.setText(couponlist.get(i));
            dynamic_radiobtn1.setPadding(5, 0, 7, 0);

            dynamic_radiobtn1.setBackgroundResource(R.drawable.rounded_edittext);
            dynamic_radiobtn1.setId(i);
            dynamic_radiobtn1.setChecked(false);
            // info.setDynamic_radiobtn(dynamic_radiobtn1);
            radioGroup.addView(dynamic_radiobtn1);
            ClickRadioButton(id, radioGroup, dynamic_radiobtn1);
        }
        if (LinearLay != null) {

            LinearLay.addView(radioGroup);
        }


        return dynamic_radiobtn1;

    }*/

          public void  ClickRadioButton(int id,RadioGroup radioGroup,RadioButton radioButton){
//              radioButton.setOnClickListener(new View.OnClickListener() {
//                  @Override
//                  public void onClick(View v) {
//                      radioButton.getText().toString();
//                      Toast.makeText(getApplicationContext(), radioButton.getText(), Toast.LENGTH_SHORT).show();
//
//                  }
//              });

              radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                  RadioButton  rb = group.findViewById(checkedId);
                  if (null != rb && checkedId > -1) {
                      Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
                      SpinerDTO spinerInfo = new SpinerDTO();
                      spinerInfo.setRadio_id(id);
                      spinerInfo.setRadio_text(rb.getText().toString());
                      radiobtnList.add(spinerInfo);
                      Log.e("Radio_Text", id+"  "+rb.getText().toString());
                  }

//                         for(int i=0; i<couponDenoList.size(); i++) {
//                              RadioButton btn = (RadioButton) radioGroup.getChildAt(i);
//                              if (btn.getId() == checkedId) {
//                                  String text = btn.getText().toString();
//                                  // do something with text
//                                  Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
//                                  SpinerDTO spinerInfo = new SpinerDTO();
//                                  spinerInfo.setRadio_id(id);
//                                  spinerInfo.setRadio_text(rb.getText().toString());
//                                  spinnerList.add(spinerInfo);
//                                  Log.e("Radio_Text", id + "  " + rb.getText().toString());
//                                  return;
//                              }
//                          }


              });
          }
    public void  ClickCheckBox(int id,CheckBox checkBox){
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(checkBox.isChecked())
                    {
                        StringBuffer result = new StringBuffer();
                        result.append(checkBox.getText().toString());
                        Toast.makeText(getApplicationContext(),checkBox.getText().toString(),
                                Toast.LENGTH_LONG).show();

                        SpinerDTO spinerInfo = new SpinerDTO();
                        spinerInfo.setCheckbox_id(id);
                        spinerInfo.setCheckbox_text(checkBox.getText().toString());
                        checkboxList.add(spinerInfo);
                        Log.e("Checkbox_Text", id+"  "+checkBox.getText().toString());
                    }

                }
            });


    }
    //Set Spinner to Adapter

    public void getCouponDenoListAdapter(final Spinner spiner_coupon_deno,
                                         final List<String> couponlist, final int id) {

        ArrayAdapter<String> programadapter = new ArrayAdapter<String>(
                getApplicationContext(), R.layout.spinnertext,R.id.tv, couponlist);
        spiner_coupon_deno.setAdapter(programadapter);
      //  programadapter.setDropDownViewResource(R.layout.spinner_list_item);
        spiner_coupon_deno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parentAdapter,
                                               View arg1, int position1, long arg3) {
                        // TODO Auto-generated method stub
                      ((TextView)arg1.findViewById(R.id.tv)).setTextColor(Color.rgb(0, 0, 1));
//                        ((TextView) parentAdapter.getChildAt(0))
//                                .setTextColor(Color.rgb(0, 0, 0));
                       int position = position1;
                   String  coupon_deno = (String) parentAdapter.getItemAtPosition(position);
                   Log.e("Value", id+" "+coupon_deno);

                        SpinerDTO spinerInfo = new SpinerDTO();
                        spinerInfo.setId(id);
                        spinerInfo.setCoupon_deno(coupon_deno);
                        spinnerList.add(spinerInfo);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) { //
                        // TODO Auto-generated method stub

                    }

                });


    }
    // Return Spinner
    private ImageView dynamic_image(int id) {
         dynamic_Image = new ImageView(this);
        dynamic_Image.setId(id);

         LL_horizo = new LinearLayout(this);
       // LL_horizo.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams layoutForInner1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LL_horizo.setLayoutParams(layoutForInner1);

//        LinearLayout.LayoutParams vp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        vp.gravity = Gravity.LEFT;
//        dynamic_Image.setLayoutParams(vp);
//        dynamic_Image.setMaxHeight(50);
//        dynamic_Image.setMaxWidth(50);
//        dynamic_Image.getLayoutParams().height = 200;
//        dynamic_Image.getLayoutParams().width = 200;
//        dynamic_Image.setScaleType(ImageView.ScaleType.FIT_XY);
//        dynamic_Image.setImageDrawable(getDrawable(R.drawable.gallery1));
//
//        LL_horizo.addView(dynamic_Image);
        LinearLay.addView(LL_horizo);

        LinearLayout LL_Inner = new LinearLayout(this);
        LL_Inner.setOrientation(LinearLayout.HORIZONTAL);
      //  LL_Inner.setBackgroundColor(android.R.color.transparent);
        LinearLayout.LayoutParams layoutForInner = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LL_Inner.setLayoutParams(layoutForInner);

        // Camera Images
        ImageView camera_Image = new ImageView(this);
        camera_Image.setId(id);
        image_id=id;
        LinearLayout.LayoutParams vp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
      //  vp1.gravity = Gravity.LEFT;
        vp1.setMargins(10, 10,10 ,10);
        camera_Image.setLayoutParams(vp1);
        camera_Image.setMaxHeight(50);
        camera_Image.setMaxWidth(50);
      //  camera_Image.setPadding(10, 10, 10, 15);

        camera_Image.setImageDrawable(getDrawable(R.drawable.cam));


        // Gallery Images
        ImageView gallery_Image = new ImageView(this);
        gallery_Image.setId(id);

        LinearLayout.LayoutParams vp11 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
       // vp11.gravity = Gravity.RIGHT;
        vp11.setMargins(10, 10,10 ,10 );

        gallery_Image.setLayoutParams(vp11);
        gallery_Image.setMaxHeight(100);
        gallery_Image.setMaxWidth(100);
        gallery_Image.setImageDrawable(getDrawable(R.drawable.gallery));

        LL_Inner.addView(camera_Image);
        LL_Inner.addView(gallery_Image);

        LinearLay.addView(LL_Inner);

     //  Camera_Gallery(camera_Image,gallery_Image,dynamic_Image,LL_horizo);
        Camera_Gallery(camera_Image,gallery_Image,dynamic_Image);



        return dynamic_Image;
    }


       public void Camera_Gallery(ImageView camera_Img, ImageView gallery_Img,ImageView ImageView)
        {
            gallery_Img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(DashboardActivity1.this, "Gallery", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                //    intent.setAction(Intent.ACTION_GET_CONTENT);
                    //intent.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(intent, GALLERY_REQUEST);


                }
            });
            camera_Img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(DashboardActivity1.this, "Camera", Toast.LENGTH_SHORT).show();
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);

                }
            });


        }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
             Bitmap mphoto = (Bitmap) data.getExtras().get("data");
           /* LinearLayout.LayoutParams vp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            vp.setMargins(10, 10, 10, 10);
            vp.gravity = Gravity.LEFT;
            ImageView  iv = new ImageView(this);
            iv.setLayoutParams(vp);
            //iv.setMaxHeight(50);
          //  iv.setMaxWidth(50);
            iv.getLayoutParams().height = 200;
            iv.getLayoutParams().width = 200;

            // Set the scale type for ImageView image scaling
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            iv.setImageBitmap(mphoto);

           // iv.setImageDrawable(getDrawable(R.drawable.gallery1));
            LL_horizo.addView(iv); */
//             SpinerDTO dto=new SpinerDTO();
//             dto.setImage_id(image_id);
//             dto.setBitmap(mphoto);
//             imageList.add(dto);
            if(imageList.size()<0)
            {

            }else {
                LL_horizo.removeView(recyclerView);
                SpinerDTO dto = new SpinerDTO();
                dto.setImage_id(image_id);
                dto.setBitmap(mphoto);
                imageList.add(dto);

            }

            imageAdapter = new ImageAdapter(imageList,DashboardActivity1.this);
            recyclerView = new RecyclerView(this);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),3);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(imageAdapter);
            LL_horizo.addView(recyclerView);

        }else if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
          //  try {
             //   Bitmap mphoto = (Bitmap) data.getExtras().get("data");
            Uri photoUri = data.getData();
            Bitmap mphoto = null;
            try {
                mphoto = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
             /*   LinearLayout.LayoutParams vp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                vp1.setMargins(10, 10, 10, 10);
                vp1.gravity = Gravity.LEFT;
                ImageView  iv = new ImageView(this);
                iv.setLayoutParams(vp1);
               // iv.setMaxHeight(50);
              //  iv.setMaxWidth(50);
                iv.getLayoutParams().height = 200;
                iv.getLayoutParams().width = 200;
                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                iv.setImageBitmap(mphoto);

                // iv.setImageDrawable(getDrawable(R.drawable.gallery1));
                LL_horizo.addView(iv);*/
                    if(imageList.size()<0)
                    {

                    }else {
                        LL_horizo.removeView(recyclerView);
                        SpinerDTO dto = new SpinerDTO();
                        dto.setImage_id(image_id);
                        dto.setBitmap(mphoto);
                        imageList.add(dto);

                    }

            } catch (IOException e) {
                e.printStackTrace();
            }


            imageAdapter = new ImageAdapter(imageList,DashboardActivity1.this);
            recyclerView = new RecyclerView(this);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),3);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(imageAdapter);
            LL_horizo.addView(recyclerView);
        //    LinearLay.addView(LL_horizo);



        }



    }
  //  Checking Validation

    // CheckValidate
    public boolean isValidate(EditText editText, String value, String regex,
                              String errMsg) {

        if (value.isEmpty()) {
            editText.setError("Required Field");
            return false;
        } else if (!Pattern.matches(regex, value)) {

            editText.setError(errMsg);
            return false;

        }
        return true;

    }


    private Button submitButton() {
        Button button = new Button(this);
        button.setHeight(WRAP_CONTENT);
        button.setText("Submit");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(40, 20, 40, 5);
        button.setLayoutParams(params);
        button.setBackgroundResource(R.drawable.rounded);
        button.setOnClickListener(submitListener);
        return button;
    }

    private View.OnClickListener submitListener = new View.OnClickListener() {
        public void onClick(View view) {
            boolean validate = true;
            smsArrayList = new ArrayList<String>();

            for (int j = 0; j < dynamicList.size(); j++) {
                Log.e("Dyanamic List Size", String.valueOf(dynamicList.size()));

                if (dynamicList.get(j).getControlType().equalsIgnoreCase("dropdown")) {
                    Log.e("Spinner List Size", String.valueOf(spinnerList.size()));
                    for (int k = 0; k < spinnerList.size(); k++) {

                        if (spinnerList.get(k).getId()== dynamicList.get(j).getControId()) {
                                      //  + "")
                    Log.e("Spinner**********", dynamicList.get(j).getFieldName()+" "+ spinnerList.get(k).getCoupon_deno());

                            json.addProperty(dynamicList.get(j)
                                    .getFieldName(), spinnerList.get(k)
                                    .getCoupon_deno());

                            smsArrayList.add(spinnerList.get(k)
                                    .getCoupon_deno());
                        }

                    }

                }


                if (dynamicList.get(j).getControlType()
                        .equalsIgnoreCase("textbox")) {


                    if (dynamicList.get(j).getIsMandatory()
                            .equalsIgnoreCase("true")) {


                        if (isValidate(dynamicList.get(j).getEditText(),
                                dynamicList.get(j).getEditText().getText()
                                        .toString(), dynamicList.get(j)
                                        .getRegex(), dynamicList.get(j)
                                        .getValidationMessage())) {

                            Log.e("TextViewWith******", dynamicList.get(j)
                                    .getFieldName()+"  "+ dynamicList.get(j)
                                    .getEditText().getText().toString());
                            json.addProperty(dynamicList.get(j)
                                    .getFieldName(), dynamicList.get(j)
                                    .getEditText().getText().toString());
                            smsArrayList.add(dynamicList.get(j)
                                            .getEditText().getText()
                                            .toString());


                        } else {
                            validate = false;

                        }
                    } else {

                        json.addProperty(dynamicList.get(j)
                                .getFieldName(), dynamicList.get(j)
                                .getEditText().getText().toString());
                        smsArrayList.add(dynamicList.get(j)
                                .getEditText().getText().toString());

                    }

                }
                if(dynamicList.get(j).getControlType().equalsIgnoreCase("radio_btn")) {
                    Log.e("RadioButton List Size", String.valueOf(radiobtnList.size()));
                    for (int k = 0; k < radiobtnList.size(); k++) {

                        if (radiobtnList.get(k).getRadio_id()== dynamicList.get(j).getControId()) {
                            //  + "")
                            Log.e("RadioButton********", dynamicList.get(j).getFieldName()+" "+ radiobtnList.get(k).getRadio_text());

                            json.addProperty("Radio_Button", radiobtnList.get(k).getRadio_text());

                            smsArrayList.add(radiobtnList.get(k).getRadio_text());
                        }

                    }
                }
                if(dynamicList.get(j).getControlType().equalsIgnoreCase("checkbox")) {
                    Log.e("CheckBox List Size", String.valueOf(checkboxList.size()));
                    StringBuffer  res=new StringBuffer();
                    for (int k = 0; k < checkboxList.size(); k++) {

                        if (checkboxList.get(k).getCheckbox_id()== dynamicList.get(j).getControId()) {
                            //  + "")
                            Log.e("CheckBox********", dynamicList.get(j).getFieldName()+" "+ checkboxList.get(k).getCheckbox_text());

                        res.append(checkboxList.get(k).getCheckbox_text()+" "+",");
                            Log.e("CheckBox*111111", res.toString());
                            json.addProperty("Check Box",res.toString());

                            smsArrayList.add(res.toString());
                        }

                    }
                }
                if(dynamicList.get(j).getControlType().equalsIgnoreCase("image")) {
                    Log.e("Image List Size", String.valueOf(imageList.size()));
                    StringBuffer  res=new StringBuffer();
                    for (int k = 0; k < imageList.size(); k++) {

                        if (imageList.get(k).getImage_id()== dynamicList.get(j).getControId()) {
                            //  + "")
                            Log.e("Image********", dynamicList.get(j).getFieldName()+" "+ imageList.get(k).getBitmap());
                            Bitmap bitmap=imageList.get(k).getBitmap();
                            // Convert bitmap in to Base 64 encode String
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream .toByteArray();
                            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

//                      Convert Base 64 encode String in to Bitmap
//                            byte[] decodedBytes = Base64.decode(
//                                    encoded.substring(encoded.indexOf(",")  + 1),
//                                    Base64.DEFAULT
//                            );
//
//                         Bitmap bitmap1= BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);


                            // iv.setImageDrawable(getDrawable(R.drawable.gallery1));


                            Log.e("Image*_encode", encoded);
                            res.append(encoded+" "+",");
                            Log.e("Image*111111", res.toString()+"\n");
                            json.addProperty("Image",res.toString());

                            smsArrayList.add(res.toString());
                        }

                    }
                }


            }


            if (validate == true) {

                //values in json format
          Object []  mStringArray = smsArrayList.toArray();
              String  smsBody = "9849496829";

                Toast.makeText(getBaseContext(), json.toString(),
                        Toast.LENGTH_LONG).show();
                System.out.println("GprsJsonFormat===" + json.toString());
             Log.e("GprsJsonFormat===" , json.toString());


                //values from array
                for (int i = 0; i < mStringArray.length; i++) {

                    if (!mStringArray[i].equals("")) {
                        smsBody += "*" + mStringArray[i];
                    }

                }

                Toast.makeText(getBaseContext(), smsBody,
                        Toast.LENGTH_LONG).show();
                Log.i("SmsBody", smsBody);


            }

        }

    };




    @Override
    public void get_value(String text) {
        result.append(text+",");
        dynamic_editText11.setText(result);

    }
}
