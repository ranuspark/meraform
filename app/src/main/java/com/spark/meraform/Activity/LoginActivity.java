package com.spark.meraform.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.spark.meraform.Api.Client;
import com.spark.meraform.R;
import com.spark.meraform.Utility.AlertUtil;
import com.spark.meraform.Utility.Formatter;
import com.spark.meraform.Utility.JSONUtils;
import com.spark.meraform.Utility.KeyboardUtils;
import com.spark.meraform.Utility.NetworkUtils;
import com.spark.meraform.Utility.SharedPreferencesAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity {
    private EditText et_Mob,et_Otp;
    private Button btn_Sendotp,btn_Con;
    private TextView tv_resndOtp;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login1);
        et_Mob= findViewById(R.id.et_mob);

        btn_Con= findViewById(R.id.btn_con);


        btn_Con.setOnClickListener(this::onLogin);


    }

    public void onLogin(View view) {
        if (NetworkUtils.isConnected(getApplicationContext())) {
            if (isLoginFormValid ()) {
                if (isLoginDataValid ()) {
                    Log.e("Onclick", "Onclick");
                 //   getOtpLogin();

                    SharedPreferences.Editor _preference = SharedPreferencesAdapter.edit ( LoginActivity.this );
                    _preference.putString ( "userId", "5" );
                    _preference.commit();

                    Intent intent = new Intent(LoginActivity.this,OTPActivity.class);
                    intent.putExtra("user_id", "5");
                    intent.putExtra("phone", et_Mob.getText().toString());
                    startActivity(intent);

                }
            }
            KeyboardUtils.hideSoftKeyboard ( this );
        } else {
            AlertUtil.showNetworkUnavailableAlert ( LoginActivity.this );
        }
        }

   private boolean isLoginDataValid() {

        if (TextUtils.isEmpty ( this.et_Mob.getText ().toString () )) {
            runOnUiThread ( () -> {
                et_Mob.setError ( "Please enter valid  phone no." );
                et_Mob.requestFocus ();
            } );
            return false;
        }

        boolean digitOnly = TextUtils.isDigitsOnly ( this.et_Mob.getText ().toString () );
        if (digitOnly) {
            if (this.et_Mob.getText ().length () > 10 || this.et_Mob.getText ().length () < 10) {
                runOnUiThread ( () -> {
                    et_Mob.setError ( "Please enter valid phone no." );
                    et_Mob.requestFocus ();
                } );
                return false;
            }
        }
        return true;
    }

    private boolean isLoginFormValid() {
        if (TextUtils.isEmpty ( this.et_Mob.getText ().toString () )) {
            runOnUiThread ( () -> {
                et_Mob.setError ( "Please enter valid phone no." );
                et_Mob.requestFocus ();
            } );
            return false;
        }


        return true;
    }

    public  void getOtpLogin() {
    Client _client = new Client ( this );
        try {
        _client.getOtp_Login (et_Mob.getText().toString()).enqueue ( new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, retrofit2.Response<HashMap<String, Object>> response) {
                HashMap<String, Object> _response = response.body ();
                Log.e("Map value", _response.toString());

                int mStatus = JSONUtils.getInteger ( _response, "success" );
                if(mStatus==1)
                {
                        Map formJSON = JSONUtils.getHashMap(_response,"data");
                        String user_id= (String) formJSON.get ("user_id");
                        Log.e("user_id", user_id);

                        Intent intent = new Intent(LoginActivity.this,OTPActivity.class);
                        intent.putExtra("user_id", user_id);
                        intent.putExtra("phone", et_Mob.getText().toString());
                        finish();

                    startActivity(intent);

                    SharedPreferences.Editor _preference = SharedPreferencesAdapter.edit ( LoginActivity.this );
                    _preference.putString ( "userId", user_id );
                    _preference.commit();


                }

            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
             //   spinner.setVisibility ( View.GONE );
            }
        } );
    } catch (NullPointerException e) {
        Log.e("Exception", e.toString());
    }
}
}
