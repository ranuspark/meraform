package com.spark.meraform.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.spark.meraform.R;
import com.spark.meraform.Utility.SharedPreferencesAdapter;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new MyThread().start();

    }
    class MyThread extends Thread{
        public void run()
        {
            try {
                MyThread.sleep(2000);

                String val=Log_logout();
                if(val==null)
                {
                    startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(SplashActivity.this,HomeActivity.class));
                    finish();
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

   public String Log_logout()
    {
        String user_id = null;
       SharedPreferences sp= SharedPreferencesAdapter.getPreferences(SplashActivity.this);
       if(sp!=null)
       {
            user_id= sp.getString("userId", null);
       }
        return user_id;
    }
}
