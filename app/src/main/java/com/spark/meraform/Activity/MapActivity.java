package com.spark.meraform.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.spark.meraform.Fragment.LocationMapFragment;
import com.spark.meraform.R;

public class MapActivity extends AppCompatActivity  {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);

//
        FragmentManager fm = getSupportFragmentManager();
        Fragment collectionView = fm.findFragmentById(R.id.container);


        FragmentTransaction ft = fm.beginTransaction();
        LocationMapFragment loc_map = new LocationMapFragment();
     //   list.setArguments(bundle);
        ft.replace(R.id.container, loc_map);
        ft.commit();


    }


}
