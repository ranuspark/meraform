package com.spark.meraform.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.spark.meraform.R;

public class SpinnerActivity extends AppCompatActivity {
    SeekBar seekBar;
    TextView tv;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_search1);
       // MaterialSpinner spinner3 = (MaterialSpinner) findViewById(R.id.sp_val1);
        seekBar= findViewById(R.id.seek_bar);
        tv= findViewById(R.id.Tv);


        MaterialSpinner spinner = findViewById(R.id.sp_field);
        spinner.setItems("Name", "Address", "Phone Number", "Lollipop", "Marshmallow");
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });
        MaterialSpinner spinner1 = findViewById(R.id.sp_operator);
        spinner1.setItems("=", "<", ">", "Between", "Marshmallow");
        spinner1.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });

        MaterialSpinner spinner2 = findViewById(R.id.sp_val);
        spinner2.setItems("Ice Cream Sandwich", "Jelly Bean", "KitKat", "Lollipop", "Marshmallow");
        spinner2.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                //Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                Log.e("Item", item);
            }
        });
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    tv.setText(""+progress +"Km");

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
       // MaterialSpinner spinner3 = (MaterialSpinner) findViewById(R.id.sp_val1);
////        spinner3.setItems("Ice Cream Sandwich", "Jelly Bean", "KitKat", "Lollipop", "Marshmallow");
////        spinner3.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
////
////            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
////                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
////            }
////        });

    }
}
