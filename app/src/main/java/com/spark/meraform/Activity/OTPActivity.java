package com.spark.meraform.Activity;

import android.accounts.Account;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.spark.meraform.Api.Client;
import com.spark.meraform.R;
import com.spark.meraform.Utility.AlertUtil;
import com.spark.meraform.Utility.JSONUtils;
import com.spark.meraform.Utility.KeyboardUtils;
import com.spark.meraform.Utility.MeraformLogger;
import com.spark.meraform.Utility.NetworkUtils;
import com.spark.meraform.Utility.SharedPreferencesAdapter;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;


public class OTPActivity extends AppCompatActivity {
    private static final int OTP_REGENERATE_WAIT_SECONDS = 30;
    private SmsVerifyCatcher smsVerifyCatcher;
   // private EditText mUsername;
    private EditText mOTP;
    private Button btnVerify;
    private ProgressDialog waitDialog;
  //  private LoginConfiguration loginConfiguration;
   // private AuthenticationTask mAuthenticationTask;
    private int seconds;
    private TextView mResendOTP;
    private ScheduledExecutorService otpCountService;
  //  private SentOTPTask mSentOTPTask;
    private String user_Name,fcm_Token;
    String user_id,phone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.otp );


        getWindow ().setFlags ( WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE );

//        Fabric.with ( this, new Crashlytics.Builder ().core ( new CrashlyticsCore.Builder ().build () ).build () );
//
//        if (!Fabric.isInitialized ()) {
//            Fabric.with ( this, new Crashlytics.Builder ().core ( new CrashlyticsCore.Builder ().build () ).build () );
//        }

        if (!isTaskRoot ()) {
            Intent intent = getIntent ();
            String intentAction = intent.getAction ();
            if (intent.hasCategory ( "android.intent.category.LAUNCHER" ) && intentAction != null && intentAction.equals ( "android.intent.action.MAIN" )) {
                Log.w ( OTPActivity.class.getCanonicalName (), "Main Activity is not the root.  Finishing Main Activity instead of launching." );
                finish ();
                return;
            }
        }
      //  loadLoginConfiguration ();


        Intent intent =getIntent();
        user_id=  intent.getStringExtra("user_id");
        phone=  intent.getStringExtra("phone");

        mOTP = findViewById ( R.id.et_otp );
        btnVerify = findViewById ( R.id.btn_verify );
        mResendOTP = findViewById ( R.id.ResendOTP );


        btnVerify.setOnClickListener(this::OTPLogin);




        smsVerifyCatcher = new SmsVerifyCatcher ( this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode ( message );//Parse verification code
                mOTP.setText ( code );//set code in edit text
                //then you can send verification code to server
            }
        } );

        //set phone number filter if needed
        // smsVerifyCatcher.setPhoneNumberFilter("777");
        //smsVerifyCatcher.setFilter("regexp");



        resendOTP ();
    }

    public void OTPLogin(View view) {
        KeyboardUtils.hideSoftKeyboard ( this );
        if (NetworkUtils.isConnected ( getApplicationContext () )) {
         //   if (!TextUtils.isEmpty (user_Name)) {
                if (validateOTP ( mOTP.getText ().toString () )) {

                    getVerifyOtp();
                    Log.e("Map value12....", phone+" "+user_id+" "+mOTP.getText().toString());

                }
          //  }

        } else {
            AlertUtil.showNetworkUnavailableAlert ( OTPActivity.this );
        }
    }

    private boolean validateOTP(String OTP) {
        if (TextUtils.isEmpty ( OTP ) && OTP.length () != 6) {
            runOnUiThread ( () -> {
                mOTP.setError ( "Invalid OTP" );
                mOTP.requestFocus ();
            } );
            return false;
        }
        return true;
    }


    private void resendOTP() {
        try {
            final TextView resendOTP = findViewById ( R.id.ResendOTP );
            runOnUiThread ( () -> resendOTP.setEnabled ( false ) );
            ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor ();
            seconds = 0;
            service.scheduleAtFixedRate ( () -> {
                seconds++;
                runOnUiThread ( () -> resendOTP.setText ( "Resend OTP in " + (OTP_REGENERATE_WAIT_SECONDS - seconds) + " s" ) );
                if (seconds == OTP_REGENERATE_WAIT_SECONDS) {
                    runOnUiThread ( () -> {
                        resendOTP.setEnabled ( true );
                        resendOTP.setText ( "Resend OTP" );
                        service.shutdownNow ();
                    } );
                }
            }, 0, 1, TimeUnit.SECONDS );

        } catch (Exception e) {
            Log.e("Tag", String.valueOf(e));
            btnVerify.setEnabled ( true );
            AlertUtil.alert ( OTPActivity.this, "Error", "Something went wrong! Please try again later." );
        }
    }

    /**
     * Parse verification code
     *
     * @param message sms message
     * @return only four numbers from massage string
     */
    private String parseCode(String message) {
        //  SMSReceiver.OTP_REGEX
        Pattern p = Pattern.compile ( "\\b\\d{6}\\b" );
        Matcher m = p.matcher ( message );
        String code = "";
        while (m.find ()) {
            code = m.group ( 0 );
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart ();
        smsVerifyCatcher.onStart ();
    }

    @Override
    protected void onStop() {
        super.onStop ();
        smsVerifyCatcher.onStop ();
        if (otpCountService != null && !otpCountService.isShutdown ()) {
            otpCountService.shutdownNow ();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy ();
        smsVerifyCatcher.onStop ();
        if (otpCountService != null && !otpCountService.isShutdown ()) {
            otpCountService.shutdownNow ();
        }
    }

    @Override
    public void onBackPressed() {
        smsVerifyCatcher.onStop ();
        super.onBackPressed ();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult ( requestCode, permissions, grantResults );
        smsVerifyCatcher.onRequestPermissionsResult ( requestCode, permissions, grantResults );
    }

  /*  private void onAuthenticationFailure(Exception error) {
        if (!isFinishing () && !isDestroyed ()) {
            AlertDialog.Builder b = new AlertDialog.Builder ( this );
            b.setTitle ( "ERROR" );
            if (error == null) error = new InvalidCredentialsException ();
            b.setMessage ( "ss" );
            b.show ();
            KeyboardUtils.hideSoftKeyboard ( this );
        }
    }

    private void onAuthenticationSuccess(ArrayList<Account> accounts, Map map) {
        TestdarpanLogger.logLogin ();
        if (accounts == null || accounts.size () <= 0) {
            AlertDialog.Builder b = new AlertDialog.Builder ( this );
            b.setTitle ( R.string.zero_contexts_title );
            b.show ();
        } else if (accounts.size () == 1) {
            runOnUiThread ( () -> findViewById ( R.id.btn_verify ).setEnabled ( true ) );
            String mStatus = JSONUtils.getString ( map, "status" );
            if (!(mStatus == null || mStatus.isEmpty ())) {
                if (mStatus.equals ( "success" )) {
                    List arrayList = JSONUtils.getArrayList ( map, "context" );
                    if (!(arrayList == null || arrayList.size () <= 0)) {
                        runOnUiThread ( () -> {
                            String DeviceId = (String) JSONUtils.getHashMap ( arrayList, 0 ).get ( "deviceId" );
                            if (!TextUtils.isEmpty ( DeviceId )) {
                                if (!DeviceId.trim ().equals ( Settings.Secure.getString ( getContentResolver (), Settings.Secure.ANDROID_ID ).trim () )) {
                                    waitDialog.dismiss ();
                                    AlertUtil.alert ( OTPActivity.this, "Error", "Your account is already registered on another device. Please contact support if you want to unregister." );
                                    return;
                                }
                            } else {
                                //TODO::DEVICE UPDATE API

                            }
                            saveAccount ( JSONUtils.getHashMap ( arrayList, 0 ) );
                            setAccount ( (Account) accounts.get ( 0 ) );
                            loginConfiguration.setEmail ( JSONUtils.getHashMap ( arrayList, 0 ).get ( "user_email" ).toString () );
                            loginConfiguration.setLoggedIn ( true );
                            writeLoginConfiguration ();
                            waitDialog.dismiss ();
                            goToDashboard ();
                            finish ();
                        } );
                    }
                } else if (mStatus.equals ( "failure" )) {
                    runOnUiThread ( () -> {
                        btnVerify.setEnabled ( true );
                        AlertUtil.alert ( OTPActivity.this, "Login", String.valueOf ( JSONUtils.getString ( map, "message" ) ) );
                    } );
                } else if (mStatus.equals ( "failed" )) {
                    runOnUiThread ( () -> {
                        btnVerify.setEnabled ( true );
                        AlertUtil.alert ( OTPActivity.this, "Login", "Something went wrong !! Please try after Sometime." );
                    } );
                }
            } else {
                runOnUiThread ( () -> {
                    btnVerify.setEnabled ( true );
                    AlertUtil.alert ( OTPActivity.this, "Login", "Something went wrong !! Please try after Sometime." );
                } );
            }
        }
    }

    private void saveAccount(Map data) {
        SharedPreferencesAdapter.saveSharedPreferences ( data, getApplicationContext () );
    }

    private void setAccount(Account account) {
        Account.setActiveAccount ( account );

    }

    private void writeLoginConfiguration() {
        LoginConfiguration.writeLoginConfiguration ( OTPActivity.this, loginConfiguration );
        printLoginConfiguration ();
    }

    private void loadLoginConfiguration() {
        loginConfiguration = LoginConfiguration.instance ( OTPActivity.this );
        printLoginConfiguration ();
    }

    private void printLoginConfiguration() {
        Log.d ( "OTP ACTIVITY", "LoginActivity Config: " + loginConfiguration.getMap () );
    }

    private void goToDashboard() {
        ActivitySwitcher.startActivity ( this, Home.class, true );
    }

    private class AuthenticationTask extends AsyncTask<Void, Void, Map> {
        private final OTPActivity mActivity;
        private final Client mClient;
        private final String Password;
        private final String Username;
        private final String Type;
        private final String mfcm_Token;
        private final boolean OtpLogin;
        private Exception mException;

        public AuthenticationTask(OTPActivity activity, String username, String password, String type, boolean otpLogin, String fcm_Token) {
            this.mActivity = activity;
            this.Username = username;
            this.Password = password;
            this.Type = type;
            this.OtpLogin = otpLogin;
            this.mfcm_Token = fcm_Token;
            this.mClient = new Client ( activity );
            runOnUiThread ( () -> findViewById ( R.id.btn_verify ).setEnabled ( false ) );
            runOnUiThread ( () -> {
                waitDialog = AlertUtil.wait ( OTPActivity.this );
                waitDialog.setMessage ( "Logging in..." );
                waitDialog.show ();
                Log.e("MTOKEN", mfcm_Token);
            } );
        }

        protected HashMap doInBackground(Void... args) {
            HashMap<String, Object> result = null;
            try {
                result = this.mClient.getAccount ( this.Username, this.Password, this.Type, this.OtpLogin,this.mfcm_Token );
            } catch (IOException e) {
                this.mException = e;
            }
            return result;
        }

        protected void onPostExecute(Map user) {
            super.onPostExecute ( user );
            if (user != null) {
                ArrayList<Object> contexts = JSONUtils.getArrayList ( user, "context" );
                ArrayList<Account> accounts = null;
                if (contexts != null && contexts.size () > 0) {
                    accounts = new ArrayList();
                    Iterator it = contexts.iterator ();
                    while (it.hasNext ()) {
                        accounts.add ( Account.getAccount ( user, JSONUtils.getHashMap ( it.next () ) ) );
                    }
                }
                this.mActivity.onAuthenticationSuccess ( accounts, user );
                return;
            }
            runOnUiThread ( () -> {
                waitDialog.dismiss ();
            } );
            this.mActivity.onAuthenticationFailure ( this.mException );
        }
    }*/

    public void sendOTP(View view) {
        KeyboardUtils.hideSoftKeyboard ( this );
        if (NetworkUtils.isConnected ( getApplicationContext () )) {
            getOtpLogin();

        } else {
            AlertUtil.showNetworkUnavailableAlert ( OTPActivity.this );
        }
    }



//    private boolean isLoginDataValid() {
//
//        if (TextUtils.isEmpty ( this.mUsername.getText ().toString () )) {
//            runOnUiThread ( () -> {
//                mUsername.setError ( "Please enter valid email address or phone no." );
//                mUsername.requestFocus ();
//            } );
//            return false;
//        }
//
//        boolean digitOnly = TextUtils.isDigitsOnly ( this.mUsername.getText ().toString () );
//        if (digitOnly) {
//            if (this.mUsername.getText ().length () > 10 || this.mUsername.getText ().length () < 10) {
//                runOnUiThread ( () -> {
//                    mUsername.setError ( "Please enter valid phone no." );
//                    mUsername.requestFocus ();
//                } );
//                return false;
//            }
//        } else if (this.mUsername.getText ().toString ().contains ( "@" )) {
//            if (!Validator.isEmailValid ( this.mUsername.getText ().toString () )) {
//                runOnUiThread ( () -> {
//                    mUsername.setError ( "Please enter valid email address" );
//                    mUsername.requestFocus ();
//                } );
//                return false;
//            }
//        } else if (this.mUsername.getText ().length () != 10 || !Validator.isEmailValid ( this.mUsername.getText ().toString () )) {
//            runOnUiThread ( () -> {
//                mUsername.setError ( "Please enter valid email address or phone no." );
//                mUsername.requestFocus ();
//            } );
//            return false;
//        }
//        return true;
//    }

    public void getVerifyOtp() {
        Client _client = new Client ( this );
        try {
            _client.getVerifyOtp (phone,user_id,mOTP.getText().toString()).enqueue ( new Callback<HashMap<String, Object>>() {
                @Override
                public void onResponse(Call<HashMap<String, Object>> call, retrofit2.Response<HashMap<String, Object>> response) {
                    HashMap<String, Object> _response = response.body();
                    // Log.e("Map value11", _response.toString());
                    Log.e("Map ", "hfg" + _response);

                    if (_response != null) {
                        int mStatus = JSONUtils.getInteger(_response, "success"); //message
                        if (mStatus == 1) {

                            String message = JSONUtils.getString(_response, "message"); //message

                            Toast.makeText(OTPActivity.this, "" + message, Toast.LENGTH_LONG).show();

                            startActivity(new Intent(OTPActivity.this, HomeActivity.class).putExtra("user_id", user_id));
                            finish();
                        }else {

                            Toast.makeText(OTPActivity.this, "Otp does not Match", Toast.LENGTH_LONG).show();

                        }



                    } else {

                    }
                }
                @Override
                public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                }
            } );
        } catch (NullPointerException e) {
            Log.e("Exception", e.toString());
        }
    }


    public  void getOtpLogin() {
        Client _client = new Client ( this );
        try {
            _client.getOtp_Login (phone).enqueue ( new Callback<HashMap<String, Object>>() {
                @Override
                public void onResponse(Call<HashMap<String, Object>> call, retrofit2.Response<HashMap<String, Object>> response) {
                    HashMap<String, Object> _response = response.body ();
                    Log.e("Map value", _response.toString());

                    int mStatus = JSONUtils.getInteger ( _response, "success" );
                    if(mStatus==1)
                    {
                        Map formJSON = JSONUtils.getHashMap(_response,"data");
                        String user_id= (String) formJSON.get ("user_id");
                        Log.e("user_id", user_id);





                    }

                }

                @Override
                public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                    //   spinner.setVisibility ( View.GONE );
                }
            } );
        } catch (NullPointerException e) {
            Log.e("Exception", e.toString());
        }
    }

}

