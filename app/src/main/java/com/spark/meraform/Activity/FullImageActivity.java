package com.spark.meraform.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.spark.meraform.R;

import java.io.ByteArrayInputStream;

public class FullImageActivity extends AppCompatActivity {
    ImageView iv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullimage);
        iv=findViewById(R.id.iv1);

        Intent intent =getIntent();
        byte[]  bit= intent.getByteArrayExtra("image");
        Log.e("Bitmap", String.valueOf(bit));

     //   String encoded = Base64.encodeToString(bit, Base64.DEFAULT);
    //    byte[] decodedBytes = Base64.decode(encoded.substring(encoded.indexOf(",")  + 1), Base64.DEFAULT);
        Bitmap bitmap1= BitmapFactory.decodeByteArray(bit, 0, bit.length);
        BitmapDrawable ob = new BitmapDrawable(getResources(),bitmap1);
        iv.setBackgroundDrawable(ob);
    }
}
