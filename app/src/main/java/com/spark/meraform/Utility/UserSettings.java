package com.spark.meraform.Utility;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.spark.meraform.Api.Client;
import com.spark.meraform.Model.Account;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class UserSettings {
    public static final String KEY_API_HOSTNAME = "api:hostname";
    public static final String KEY_API_HTTPS = "api:https";
    public static final String KEY_APP_ACTIVE_ACCOUNT = "app:active_account";
    public static final String KEY_APP_IS_FIRST_LAUNCH = "app:is_first_launch";
    public static final int SYNC_MODE_ALWAYS = 0;

    public static boolean isFirstLaunch(Context context) {
        return getGlobalPreferences ( context ).getBoolean ( KEY_APP_IS_FIRST_LAUNCH, true );
    }

    public static void setIsFirstLaunch(Context context, boolean isFirstLaunch) {
        getGlobalPreferences ( context ).edit ().putBoolean ( KEY_APP_IS_FIRST_LAUNCH, isFirstLaunch ).apply ();
    }

    private static SharedPreferences getGlobalPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences ( context.getApplicationContext () );
    }

    public static void setActiveAccount(Context context, Account account) {
        if (account != null) {
            setActiveAccountID ( context, account.getContextID () );
        } else {
            setActiveAccountID ( context, -1 );
        }
    }

    public static void setActiveAccountID(Context context, long accountID) {
        getGlobalPreferences ( context ).edit ().putLong ( KEY_APP_ACTIVE_ACCOUNT, accountID ).apply ();
    }

    public static void setActiveAccountID(Context context, String accountID) {
        getGlobalPreferences ( context ).edit ().putString ( KEY_APP_ACTIVE_ACCOUNT, accountID ).apply ();
    }

    public static String getHostname(Context context) {
        return getGlobalPreferences ( context ).getString ( KEY_API_HOSTNAME, Client.DEFAULT_HOSTNAME );
    }

    private static SharedPreferences getAccountPreferences(Context context) {
        return getAccountPreferences ( context, Account.getActiveAccount ( context ) );
    }

    public static SharedPreferences getAccountPreferences(Context context, Account account) {
        return context.getSharedPreferences ( getAccountPreferencesName ( account ), SYNC_MODE_ALWAYS );
    }


    public static String getAccountPreferencesName(Account account) {
        return account.getUserID () + "_" + account.getContextID ();
    }

    public static Account getActiveAccount(Context context) {
        long accountID = getActiveAccountID ( context );
        return accountID != -1 ? Account.getAccount ( accountID ) : null;
    }

    public static long getActiveAccountID(Context context) {
        return getGlobalPreferences ( context ).getLong ( KEY_APP_ACTIVE_ACCOUNT, -1 );
    }

    //TODO:Make True In Release - For Testing Set to the FALSE,
    public static boolean isHttpsEnabled(Context context) {
        return getGlobalPreferences ( context ).getBoolean ( KEY_API_HTTPS, false );
    }


    public static String GetCurrentDateTime()
    {

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


}
