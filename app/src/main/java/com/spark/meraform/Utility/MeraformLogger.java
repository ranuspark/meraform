package com.spark.meraform.Utility;

import android.app.Application;
import android.content.pm.PackageInfo;


import com.spark.meraform.R;

import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;



public class MeraformLogger {

    public static void initialize(Application application) {
        PackageInfo pkgInfo = ApplicationUtils.getGISPackageInfo(application);
        PackageInfo playServices = ApplicationUtils.getApplicationPackageInfo(application, "com.google.android.gms");
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        String country = locale.getCountry();
        String formattedLocale = String.format("%s %s-%s", locale.getDisplayName(), language, country);
        String smallestWidth = application.getString(R.string.smallest_width);
        String screenSize = application.getString(R.string.screen_size);
        String screenDensity = application.getString(R.string.screen_density);
        // Fabric.with(new Fabric.Builder(application).kits(new Crashlytics(), new Answers()).debuggable(true).build());
//        if (pkgInfo != null) {
//            Crashlytics.setString("Build Number", String.valueOf(pkgInfo.versionCode));
//        }
//        Crashlytics.setString("Build Configuration", "Release");
//        Crashlytics.setString("User Locale", formattedLocale);
//        if (playServices == null) {
//            Crashlytics.setString("Google Play versionCode", "none");
//            Crashlytics.setString("Google Play versionName", "NameNotFound");
//        } else {
//            Crashlytics.setString("Google Play versionCode", String.valueOf(playServices.versionCode));
//            Crashlytics.setString("Google Play versionName", playServices.versionName);
//        }
//        Crashlytics.setString("Screen Density", screenDensity);
//        Crashlytics.setString("Screen Size", screenSize);
//        Crashlytics.setString("Smallest Width", smallestWidth);
//    }

//    public static void setActiveAccount(Account account) {
//        if (account != null) {
//            Crashlytics.setUserEmail(account.getUserEmailAddress());
//            Crashlytics.setUserIdentifier(account.getUserID());
//            Crashlytics.setUserName(account.getUserName());
//            Crashlytics.setString("Context ID", account.getContextID());
//            Crashlytics.setString("API Token", account.getToken());
//        }
//    }

//    public static void setActiveNetworkType(String type) {
//        Crashlytics.setString("Network Type", type);
//    }
//
//    public static void setActiveForm(Form form) {
//        if (form != null) {
//            Crashlytics.setString("Active Form", form.getRemoteID());
//        }
//    }
//
//    public static void log(String message) {
//        log("GIS", message);
//    }
//
//    public static void log(Class<?> klass, String message) {
//        log(klass.getSimpleName(), message);
//    }
//
//    public static void log(String tag, String format, Object... args) {
//        log(tag, String.format(format, args));
//    }
//
//    public static void log(String tag, String message) {
//        Crashlytics.log(3, tag, message);
//    }
//
//    public static void log(Throwable e) {
//        log(e, null);
//    }
//
//    public static void log(Throwable e, Map<String, String> metainfo) {
//        if (metainfo != null) {
//            for (Entry<String, String> meta : metainfo.entrySet()) {
//                Crashlytics.setString((String) meta.getKey(), (String) meta.getValue());
//            }
//        }
//        Crashlytics.logException(e);
//    }
//
//    public static void logLogin() {
//        Answers.getInstance().logLogin(new LoginEvent().putSuccess(true));
//    }
//
//    public static void logNewRecord() {
//        Answers.getInstance().logCustom(new CustomEvent("new_record"));
//    }


    }
}
