package com.spark.meraform.Utility;

public class Formatter {
    public static String doubleToIntStr(Object o) {
        return doubleToIntStr(o, null);
    }

    public static String doubleToIntStr(Object o, String defaultValue) {
        if (o == null)
            return defaultValue;
        return String.valueOf(Double.valueOf(o.toString()).intValue());
    }

    public static Integer doubleToInt(Object o) {
        return doubleToInt(o, null);
    }

    public static Integer doubleToInt(Object o, Integer defaultValue) {
        if (o == null)
            return defaultValue;
        return Double.valueOf(o.toString()).intValue();
    }

    public static Long doubleToLong(Object o) {
        return doubleToLong(o, null);
    }

    public static Long doubleToLong(Object o, Long defaultValue) {
        if (o == null)
            return defaultValue;
        return Double.valueOf(o.toString()).longValue();
    }

    public static String objectToString(Object o) {
        return objectToString(o, null);
    }

    public static String objectToString(Object o, String defaultValue) {
        if (o == null)
            return defaultValue;
        return String.valueOf(o);
    }

    public static Boolean objectToBoolean(Object o) {
        return objectToBoolean(o, false);
    }

    public static Boolean objectToBoolean(Object o, Boolean defaultValue) {
        if (o == null)
            return defaultValue;
        else {
            if (String.valueOf(o).equalsIgnoreCase("true"))
                return true;
            if (String.valueOf(o).equalsIgnoreCase("false"))
                return false;
            //not boolean
            return null;
        }
    }
}
