package com.spark.meraform.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Map;

public class SharedPreferencesAdapter {
    public static final String KEY_ACCOUNT_ACTIVE_VIEW_MODE = "account:active_view_mode";


    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences ( "MeraForm", Context.MODE_PRIVATE );
    }

    public static SharedPreferences.Editor edit(Context context) {
        SharedPreferences preferences = context.getSharedPreferences ( "MeraForm", Context.MODE_PRIVATE );
        return preferences.edit ();
    }

    public static void saveSharedPreferences(Map data, Context context) {
        SharedPreferences.Editor editor = edit ( context );

        if (data.containsKey ( "user_email" )) {
            editor.putString ( "user_email", data.get ( "user_email" ).toString () );
        }

        if (data.containsKey ( "context_id" )) {
            editor.putString ( "context_id", data.get ( "context_id" ).toString () );
        }

        if (data.containsKey ( "user_id" )) {
            editor.putString ( "user_id", data.get ( "user_id" ).toString () );
        }

        if (data.containsKey ( "first_name" )) {
            if (data.get ( "first_name" ) != null) {
                editor.putString ( "first_name", ApplicationUtils.valueToStringOrEmpty ( data, "first_name" ) );
            } else {
                editor.putString ( "first_name", ApplicationUtils.valueToStringOrEmpty ( data, "first_name" ) );
            }
        }

        if (data.containsKey ( "last_name" )) {
            if (data.get ( "last_name" ) != null) {
                editor.putString ( "last_name", ApplicationUtils.valueToStringOrEmpty ( data, "last_name" ) );
            } else {
                editor.putString ( "last_name", ApplicationUtils.valueToStringOrEmpty ( data, "last_name" ) );
            }
        }

        if (data.containsKey ( "gender" )) {
            editor.putString ( "gender", ApplicationUtils.valueToStringOrEmpty ( data, "gender" ) );
        }
        if (data.containsKey ( "mobile_number" )) {
            editor.putString ( "mobile_number", ApplicationUtils.valueToStringOrEmpty ( data, "mobile_number" ) );
        }
        if (data.containsKey ( "bio" )) {
            editor.putString ( "bio", ApplicationUtils.valueToStringOrEmpty ( data, "bio" ) );
        }
        if (data.containsKey ( "profile_pic_thumbnails" )) {
            editor.putString ( "profile_pic_thumbnails", ApplicationUtils.valueToStringOrEmpty ( data, "profile_pic_thumbnails" ) );
        }
        if (data.containsKey ( "address_line_1" )) {
            editor.putString ( "address_line_1", ApplicationUtils.valueToStringOrEmpty ( data, "address_line_1" ) );
        }
        if (data.containsKey ( "address_line_2" )) {
            editor.putString ( "address_line_2", ApplicationUtils.valueToStringOrEmpty ( data, "address_line_2" ) );
        }
        if (data.containsKey ( "pin_code" )) {
            editor.putString ( "pin_code", ApplicationUtils.valueToStringOrEmpty ( data, "pin_code" ) );
        }
        if (data.containsKey ( "state_id" )) {
            editor.putInt ( "state_id", ApplicationUtils.valueToIntOrEmpty ( data, "state_id" ) );
        }
        if (data.containsKey ( "city_id" )) {
            editor.putInt ( "city_id", ApplicationUtils.valueToIntOrEmpty ( data, "city_id" ) );
        }
        if (data.containsKey ( "is_Proof" )) {
            editor.putBoolean ( "is_Proof", Boolean.parseBoolean ( data.get ( "is_Proof" ).toString () ) );
        }
        if (data.containsKey ( "is_email_verified" )) {
            editor.putBoolean ( "is_email_verified", Boolean.parseBoolean ( data.get ( "is_email_verified" ).toString () ) );
        }
        if (data.containsKey ( "is_mobile_verified" )) {
            editor.putBoolean ( "is_mobile_verified", Boolean.parseBoolean ( data.get ( "is_mobile_verified" ).toString () ) );
        }

        if (data.containsKey ( "id_proof_type" )) {
            editor.putString ( "id_proof_type", ApplicationUtils.valueToStringOrEmpty ( data, "id_proof_type" ) );
        }

        if (data.containsKey ( "language" )) {
            ArrayList<String> _languageArrayId = ApplicationUtils.valueToStringArrayOrEmpty ( data, "language" );
            if (!(_languageArrayId == null || _languageArrayId.size () <= 0)) {
                int counter = 0;
                while (counter < _languageArrayId.size ()) {
                    if (_languageArrayId.size () == 1) {
                        if (_languageArrayId.get ( counter ) != null) {
                            if (_languageArrayId.get ( counter ).equals ( "No Preference" )) {
                                editor.putString ( "primary_language", _languageArrayId.get ( counter ) );
                                editor.putString ( "secondary_language", _languageArrayId.get ( counter ) );
                            } else {
                                editor.putString ( "primary_language", _languageArrayId.get ( counter ) );
                            }
                        }
                    }
                    if (_languageArrayId.size () == 2) {
                        if (_languageArrayId.get ( counter ) != null) {
                            editor.putString ( "primary_language", _languageArrayId.get ( counter ) );
                            editor.putString ( "secondary_language", _languageArrayId.get ( counter + 1 ) );
                        }
                    }
                    editor.apply ();
                    break;
                }
            }
        }
    }

    public static int getActiveViewMode(Context context, int fallback) {
        return getPreferences(context).getInt(KEY_ACCOUNT_ACTIVE_VIEW_MODE, fallback);

    }

    public static void setActiveViewMode(Context context, int viewMode) {
        SharedPreferences.Editor editor = edit ( context );
        editor.putInt (KEY_ACCOUNT_ACTIVE_VIEW_MODE, viewMode).apply();

    }
}