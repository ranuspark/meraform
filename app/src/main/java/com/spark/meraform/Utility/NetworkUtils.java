package com.spark.meraform.Utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {
    public static ConnectivityManager getConnectivityManager(Context context) {
        return (ConnectivityManager) context.getApplicationContext ().getSystemService ( Context.CONNECTIVITY_SERVICE );
    }

    public static NetworkInfo getActiveNetworkInfo(Context context) {
        return getConnectivityManager ( context ).getActiveNetworkInfo ();
    }

    public static boolean isUsingWiFi(Context context) {
        NetworkInfo netInfo = getActiveNetworkInfo ( context );
        return netInfo != null && netInfo.isConnected() && netInfo.getType() == 1;
    }

    public static boolean isConnected(Context context) {
        NetworkInfo netInfo = getActiveNetworkInfo ( context );
        if (netInfo == null || !netInfo.isConnected ()) {
            return false;
        }
        String networkType;
        switch (netInfo.getType ()) {
            case 0:
                networkType = "Mobile";
                break;
            case 1:
                networkType = "WiFi";
                break;
            case 2:
                networkType = "Mobile - MMS";
                break;
            case 3:
                networkType = "Mobile - SUPL";
                break;
            case 4:
                networkType = "Mobile - DUN";
                break;
            case 5:
                networkType = "Mobile - HIPRI";
                break;
            case 6:
                networkType = "WiMAX";
                break;
            case 7:
                networkType = "Bluetooth";
                break;
            case 9:
                networkType = "Ethernet";
                break;
            default:
                networkType = String.valueOf ( netInfo.getType () );
                break;
        }

      //  MeraformLogger.setActiveNetworkType ( networkType );
        return true;
    }
}
