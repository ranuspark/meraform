package com.spark.meraform.Utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;

import com.spark.meraform.R;


public class AlertUtil {
    private static int dialogStyle = R.style.AlertDialogStyle;

    public static ProgressDialog wait(Context context) {
        ProgressDialog dialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        dialog.setIndeterminate(true);
        dialog.setMessage("Please Wait...");
        dialog.show();
        return dialog;
    }

    private static AlertDialog.Builder newInstance(Context context) {
        return new AlertDialog.Builder ( context );
    }

    public static void showNetworkUnavailableAlert(Context context) {
        AlertDialog.Builder _builder = newInstance ( context );
        _builder.setTitle ( R.string.internet_unavailable_title );
        _builder.setMessage ( R.string.internet_unavailable_message );
        _builder.show ();
    }

    public static void alert(Context context, String title, String message) {
        alert(context, title, message, (dialog, which) -> {
        });
    }

    public static void alert(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, dialogStyle);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable ( false )
                .setNeutralButton("ok", listener)
                .show();
    }

    public static void confirm(Context context, String title, String message, DialogInterface.OnClickListener yes) {
        confirm(context, title, message, yes, (dialog, which) -> { /*Do nothing*/ });
    }

    public static void confirm(Context context, String title, String message, DialogInterface.OnClickListener yes, DialogInterface.OnClickListener no) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, dialogStyle);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, yes)
                .setNegativeButton(android.R.string.no, no)
                .setOnCancelListener(dialog -> no.onClick(null, 0)) // default to no
                .show();
    }
}
