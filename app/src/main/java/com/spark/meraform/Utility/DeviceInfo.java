package com.spark.meraform.Utility;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;


import com.spark.meraform.R;

import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;


import static android.content.Context.TELEPHONY_SERVICE;
import static android.text.TextUtils.isEmpty;

public class DeviceInfo {

    private static int mEmulatorRating = -1;

    private static boolean isVersionOrHigher(int version) {
        return VERSION.SDK_INT >= version;
    }

    public static boolean isNougat() {
        return isVersionOrHigher ( 24 );
    }

    public static boolean isMarshmallow() {
        return isVersionOrHigher ( 23 );
    }

    public static boolean isLollipop() {
        return isVersionOrHigher ( 21 );
    }

    public static boolean isJellyBeanMR2() {
        return isVersionOrHigher ( 18 );
    }

    public static boolean isJellyBeanMR1() {
        return isVersionOrHigher ( 17 );
    }

    public static boolean isJellyBean() {
        return isVersionOrHigher ( 16 );
    }

//    public static boolean isRightToLeft(Resources resources) {
//        return resources.getBoolean ( R.bool.is_right_to_left );
//    }

    public static boolean isRightToLeft() {
        return isRightToLeft ( Locale.getDefault () );
    }

    public static boolean isRightToLeft(Locale locale) {
        int directionality = Character.getDirectionality ( locale.getDisplayName ().charAt ( 0 ) );
        return directionality == 1 || directionality == 2;
    }

    public static String getSystemVersion() {
        return VERSION.RELEASE;
    }

    public static String getDeviceManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }

    @SuppressLint({"HardwareIds"})
    public static String getDeviceIdentifier(Context context) {
        return Secure.getString ( context.getContentResolver (), "android_id" );
    }

    @TargetApi(17)
    public static double getAspectRatio(Display display) {
        double width;
        double height;
        if (isJellyBeanMR1 ()) {
            DisplayMetrics realMetrics = new DisplayMetrics();
            display.getRealMetrics ( realMetrics );
            width = (double) realMetrics.widthPixels;
            height = (double) realMetrics.heightPixels;
        } else {
            try {
                width = (double) ((Integer) Display.class.getMethod ( "getRawWidth").invoke ( display, new Object[0] )).intValue ();
                height = (double) ((Integer) Display.class.getMethod ( "getRawHeight").invoke ( display, new Object[0] )).intValue ();
            } catch (Exception e) {
                width = (double) display.getWidth ();
                height = (double) display.getHeight ();
            }
        }
        if (width > height) {
            return width / height;
        }
        return height / width;
    }

//    public static boolean isEmulator() {
//        if (mEmulatorRating < 0) {
//            int newRating = 0;
//            if (Build.PRODUCT.equals ( "sdk" ) || Build.PRODUCT.equals ( CommonUtils.GOOGLE_SDK ) || Build.PRODUCT.equals ( "sdk_x86" ) || Build.PRODUCT.equals ( "vbox86p" )) {
//                newRating = 0 + 1;
//            }
//            if (Build.MANUFACTURER.equals ( "unknown" ) || Build.MANUFACTURER.equals ( "Genymotion" )) {
//                newRating++;
//            }
//            if (Build.BRAND.equals ( "generic" ) || Build.BRAND.equals ( "generic_x86" )) {
//                newRating++;
//            }
//            if (Build.DEVICE.equals ( "generic" ) || Build.DEVICE.equals ( "generic_x86" ) || Build.DEVICE.equals ( "vbox86p" )) {
//                newRating++;
//            }
//            if (Build.MODEL.equals ( "sdk" ) || Build.MODEL.equals ( CommonUtils.GOOGLE_SDK ) || Build.MODEL.equals ( "Android SDK built for x86" )) {
//                newRating++;
//            }
//            if (Build.HARDWARE.equals ( "goldfish" ) || Build.HARDWARE.equals ( "vbox86" )) {
//                newRating++;
//            }
//            if (Build.FINGERPRINT.contains ( "generic/sdk/generic" ) || Build.FINGERPRINT.contains ( "generic_x86/sdk_x86/generic_x86" ) || Build.FINGERPRINT.contains ( "generic/google_sdk/generic" ) || Build.FINGERPRINT.contains ( "generic/vbox86p/vbox86p" )) {
//                newRating++;
//            }
//            mEmulatorRating = newRating;
//        }
//        return mEmulatorRating > 4;
//    }

    public static HashMap<String, Object> createDeviceDataMap() {
        HashMap<String, Object> map = new HashMap();
        map.put ( "OS", VERSION.RELEASE );
        map.put ( "BOARD", Build.BOARD );
        map.put ( "DEVICE", Build.DEVICE );
        map.put ( "MODEL", Build.MODEL );
        map.put ( "PRODUCT", Build.PRODUCT );
        map.put ( "BRAND", Build.BRAND );
        map.put ( "DISPLAY", Build.DISPLAY );
        map.put ( "HARDWARE", Build.HARDWARE );
        map.put ( "ID", Build.ID );
        map.put ( "MANUFACTURER", Build.MANUFACTURER );
        map.put ( "SERIAL", Build.SERIAL );
        map.put ( "UNKNOWN", Build.UNKNOWN );
        map.put ( "USER", Build.USER );
        map.put ( "HOST", Build.HOST );
        map.put ( "SUPPORTED_ABIS", Build.SUPPORTED_ABIS );
        map.put ( "TAGS", Build.TAGS );
        map.put ( "SDK", Integer.valueOf ( VERSION.SDK_INT ) );
        map.put ( "APP_VERSION", Integer.valueOf ( 690 ) );
        map.put ( "FINGERPRINT", Build.FINGERPRINT );
        return map;
    }


    public static String getIMEINumber(Context context) {
        String IMEINumber = "";
        if (ActivityCompat.checkSelfPermission ( context, android.Manifest.permission.READ_PHONE_STATE ) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager telephonyMgr = (TelephonyManager) context.getSystemService ( TELEPHONY_SERVICE );
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                IMEINumber = telephonyMgr.getImei ();
            } else {
                IMEINumber = telephonyMgr.getDeviceId ();
            }
        }
        return IMEINumber;
    }

    public static UUID generateDeviceUuid(Context context) {

        String buildParams = Build.BOARD + Build.BRAND + Build.CPU_ABI + Build.SUPPORTED_ABIS
                + Build.DEVICE + Build.DISPLAY + Build.FINGERPRINT + Build.HOST
                + Build.ID + Build.MANUFACTURER + Build.MODEL + Build.PRODUCT
                + Build.TAGS + Build.TYPE + Build.USER;

        TelephonyManager tm = (TelephonyManager) context
                .getSystemService ( Context.TELEPHONY_SERVICE );

        String IMEINumber = null;

        if (ActivityCompat.checkSelfPermission ( context, android.Manifest.permission.READ_PHONE_STATE ) == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                IMEINumber = tm.getImei ();
            } else {
                IMEINumber = tm.getDeviceId ();
            }
        }

        String androidId = Secure.getString ( context.getContentResolver (),
                Secure.ANDROID_ID );

        WifiManager wm = (WifiManager) context
                .getSystemService ( Context.WIFI_SERVICE );

        String mac = wm.getConnectionInfo ().getMacAddress ();

        if (isEmpty ( IMEINumber ) && isEmpty ( androidId ) && isEmpty ( mac )) {
            return UUID.randomUUID ();
        }
        String fullHash = buildParams + IMEINumber + androidId + mac;

        return UUID.nameUUIDFromBytes ( fullHash.getBytes () );
    }

}