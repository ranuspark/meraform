package com.spark.meraform.Utility;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class JSONUtils {
    private static final ObjectMapper MAPPER = new ObjectMapper ();

    public static String getString(Map json, String name) {
        return getString ( json, name, null );
    }

    public static Integer getInteger(Map json, String name) {
        return getInteger ( json, name, null );
    }

    public static String getString(Map json, String name, String fallback) {
        return hasKey ( json, name ) ? (String) json.get ( name ) : fallback;
    }

    public static Integer getInteger(Map json, String name, Integer fallback) {
        return hasKey ( json, name ) ? (Integer) json.get ( name ) : fallback;
    }

    public static String getString(List json, int index) {
        return getString ( json, index, null );
    }

    public static String getString(List json, int index, String fallback) {
        return hasIndex ( json, index ) ? (String) json.get ( index ) : fallback;
    }

    public static ArrayList<Object> getArrayList(Map json, String name) {
        return hasKey ( json, name ) ? toArrayList ( json.get ( name ) ) : null;
    }

    public static HashMap<String, Object> getHashMap(Map json, String name) {
        return hasKey ( json, name ) ? toHashMap ( json.get ( name ) ) : null;
    }

    public static HashMap<String, Object> getHashMap(List json, int index) {
        return hasIndex ( json, index ) ? toHashMap ( json.get ( index ) ) : null;
    }

    public static HashMap<String, Object> getHashMap(Object json) {
        return json != null ? toHashMap ( json ) : null;
    }

    public static boolean getBoolean(Map json, String name) {
        return hasKey ( json, name ) && ((Boolean) json.get ( name )).booleanValue ();
    }

    public static boolean getBoolean(Map json, String name, boolean fallback) {
        return hasKey ( json, name ) ? ((Boolean) json.get ( name )).booleanValue () : fallback;
    }

    public static boolean getABoolean(Map json, String name, boolean fallback) {
        return hasKey ( json, name ) ? (Boolean.parseBoolean ( json.get ( name ).toString () )) : fallback;
    }

    public static int getInt(Map json, String name) {
        return getInt ( json, name, Integer.valueOf ( 0 ) ).intValue ();
    }

    public static Integer getInt(Map json, String name, Integer fallback) {
        if (hasKey ( json, name )) {
            return Integer.valueOf ( toInt ( json.get ( name ) ) );
        }
        return fallback;
    }

    public static long getLong(Map json, String name) {
        return getLong ( json, name, 0 );
    }

    public static long getLong(Map json, String name, long fallback) {
        return hasKey ( json, name ) ? toLong ( json.get ( name ) ) : fallback;
    }

    public static long getLong(List json, int index) {
        return hasIndex ( json, index ) ? toLong ( json.get ( index ) ) : 0;
    }

    public static double getDouble(Map json, String name) {
        return getDouble ( json, name, 0.0d );
    }

    public static double getDouble(List json, int index) {
        return hasIndex ( json, index ) ? toDouble ( json.get ( index ) ) : 0.0d;
    }

    public static double getDouble(Map json, String name, double fallback) {
        return hasKey ( json, name ) ? toDouble ( json.get ( name ) ) : fallback;
    }

    public static boolean hasKey(Map json, String key) {
        return json != null && json.get(key) != null;
    }

    public static Object fromJSON(String json) throws IOException {
        return json != null ? MAPPER.readValue ( json, Object.class ) : null;
    }

    public static HashMap<String, Object> objectFromJSON(String json) throws IOException {
        return json != null ? MAPPER.readValue ( json, HashMap.class ) : null;
    }

    public static HashMap<String, Object> tryObjectFromJSON(String json) {
        try {
            return objectFromJSON ( json );
        } catch (Throwable e) {
            Log.e("Exception", e.toString());
            return null;
        }
    }

    public static ArrayList<Object> arrayFromJSON(String json) throws IOException {
        return json != null ? MAPPER.readValue ( json, ArrayList.class ) : null;
    }

    public static String toJSONString(Object object) {
        try {
            return MAPPER.writeValueAsString ( object );
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    private static boolean hasIndex(List json, int index) {
        return json != null && index >= 0 && json.size () > index;
    }

    private static HashMap<String, Object> toHashMap(Object object) {
        return object instanceof HashMap ? (HashMap) object : null;
    }

    private static ArrayList<Object> toArrayList(Object array) {
        return array instanceof ArrayList ? (ArrayList) array : null;
    }

    private static double toDouble(Object number) {
        return ((Number) number).doubleValue ();
    }

    private static int toInt(Object number) {
        return ((Number) number).intValue ();
    }

    private static long toLong(Object number) {
        return ((Number) number).longValue ();
    }
}

