package com.spark.meraform.Utility;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

public class ApplicationUtils {
    public static PackageInfo getApplicationPackageInfo(Context context, String application) {
        try {
            return context.getPackageManager().getPackageInfo(application, 0);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public static PackageInfo getGISPackageInfo(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public static String getVersion(Context context) {
        PackageInfo info = getGISPackageInfo(context);
        if (info != null) {
            return info.versionName;
        }
        return null;
    }

    public static int getBuildNumber(Context context) {
        PackageInfo info = getGISPackageInfo(context);
        if (info != null) {
            return info.versionCode;
        }
        return 0;
    }

    public static String getFormattedVersionString(Context context) {
        PackageInfo info = getGISPackageInfo(context);
        if (info == null) {
            return null;
        }
        String versionName = info.versionName;
        String buildNumber = String.valueOf(info.versionCode);
        return String.format("%s (%s)", versionName, buildNumber);
    }

    public static String getUserAgentString(Context context) {
        String format = "GIS Android %s, Android %s, %s, %s";
        String versionStr = getFormattedVersionString(context);
        String androidVer = VERSION.RELEASE;
        String manufacturer = Build.MANUFACTURER;
        String deviceName = Build.MODEL;
        return String.format(Locale.US, "GIS Android %s, Android %s, %s, %s", versionStr, androidVer, manufacturer, deviceName);
    }

    public static ArrayList valueToStringArrayOrEmpty(Map<String, Integer> map, String key) {
        ArrayList<Object> contexts = JSONUtils.getArrayList ( map, key );
        if (contexts != null && contexts.size () > 0) {
            int i = 0;
            ArrayList<String> _finalList = new ArrayList<> ();
            while (i < contexts.size ()) {
                if (contexts.get ( i ) != null) {
                    _finalList.add ( contexts.get ( i ).toString () );
                }
                i++;
            }
            return _finalList;
        }
        return null;
    }


    public static String valueToStringOrEmpty(Map<String, ?> map, String key) {
        Object value = map.get ( key );
        return value == null ? "" : value.toString ();
    }

    public static Integer valueToIntOrEmpty(Map<String, ?> map, String key) {
        Object value = map.get ( key );
        return value == null ? 0 : (Integer) value;
    }

}
