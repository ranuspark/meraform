package com.spark.meraform.Utility;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtils {
    public static void hideSoftKeyboard(Activity activity) {
        View focus = activity.getCurrentFocus ();
        if (focus != null) {
            ((InputMethodManager) activity.getSystemService (Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow ( focus.getWindowToken (), 0 );
        }
    }

    public static void hideSoftKeyboard(Context context, View view) {
        if (view != null) {
            ((InputMethodManager) context.getSystemService (Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow ( view.getWindowToken (), 0 );
        }
    }

    public static void hideDialogSoftKeyboard(Activity activity, View view) {
        if (view != null) {
            ((InputMethodManager) activity.getSystemService (Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow ( view.getWindowToken (), 0 );
        }
    }
}
