package com.spark.meraform.Utility;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CursorUtils {
    private static final int LARGE_COLUMN_CHUNK_SIZE = 524288;

    public static boolean isNull(Cursor cursor, String column) {
        return cursor.isNull(cursor.getColumnIndex(column));
    }

    public static String getString(Cursor cursor, String column) {
        int index = cursor.getColumnIndex(column);
        if (index < 0) {
            Log.e("col not found in cursor", String.valueOf(new Object[]{column, cursor.toString()}));
        } else {
            try {
                return cursor.getString(index);
            } catch (Throwable e) {
                Log.e("Exception1", e.toString());
            }
        }
        return null;
    }

    public static boolean getBoolean(Cursor cursor, String column) {
        return cursor.getInt(cursor.getColumnIndex(column)) == 1;
    }

    public static long getLong(Cursor cursor, String column) {
        return cursor.getLong(cursor.getColumnIndex(column));
    }

    public static double getDouble(Cursor cursor, String column) {
        return cursor.getDouble(cursor.getColumnIndex(column));
    }

    public static float getFloat(Cursor cursor, String column) {
        return cursor.getFloat(cursor.getColumnIndex(column));
    }

    public static int getInt(Cursor cursor, String column) {
        return cursor.getInt(cursor.getColumnIndex(column));
    }

    public static Double getDoubleOrNull(Cursor cursor, String column) {
        return isNull(cursor, column) ? null : Double.valueOf(getDouble(cursor, column));
    }

    public static Float getFloatOrNull(Cursor cursor, String column) {
        return isNull(cursor, column) ? null : Float.valueOf(getFloat(cursor, column));
    }

    public static Integer getIntegerOrNull(Cursor cursor, String column) {
        return isNull(cursor, column) ? null : Integer.valueOf(getInt(cursor, column));
    }

    public static Object getObject(Cursor cursor, int columnIndex) {
        switch (cursor.getType(columnIndex)) {
            case 1:
                return Integer.valueOf(cursor.getInt(columnIndex));
            case 2:
                return Float.valueOf(cursor.getFloat(columnIndex));
            case 3:
                return cursor.getString(columnIndex);
            case 4:
                return cursor.getBlob(columnIndex);
            default:
                return null;
        }
    }

    public static Cursor queryTableWithLargeColumn(SQLiteDatabase db, String tableName, List<String> columns, String largeColumn, String selection, String[] selectionArgs, String orderBy) {
        return queryTableWithLargeColumn(db, tableName, columns, largeColumn, selection, selectionArgs, orderBy, null);
    }

    public static Cursor queryTableWithLargeColumn(SQLiteDatabase db, String tableName, List<String> columns, String largeColumn, String selection, String[] selectionArgs, String orderBy, String limit) {
        return queryTableWithLargeColumns(db, tableName, columns, Collections.singletonList(largeColumn), selection, selectionArgs, orderBy, limit);
    }

    public static Cursor queryTableWithLargeColumns(SQLiteDatabase db, String tableName, List<String> columns, List<String> largeColumns, String selection, String[] selectionArgs, String orderBy) {
        return queryTableWithLargeColumns(db, tableName, columns, largeColumns, selection, selectionArgs, orderBy, null);
    }

    public static String fetchLargeColumn(SQLiteDatabase db, String tableName, Cursor cursor, String columnName) {
        StringBuilder data = new StringBuilder();
        data.append(getString(cursor, columnName));
        int size = getInt(cursor, columnName + "_length");
        if (size >= 524288) {
            int batches = size / 524288;
            String id = getString(cursor, "_id");
            for (int batchNumber = 0; batchNumber < batches; batchNumber++) {
                String substringParameters = columnName + "," + (((batchNumber + 1) * 524288) + 1) + "," + 524288;
                SQLiteDatabase sQLiteDatabase = db;
                String str = tableName;
                Cursor batchCursor = sQLiteDatabase.query(str, new String[]{"SUBSTR(" + substringParameters + ") AS " + columnName}, "_id = ?", new String[]{id}, null, null, null);
                if (batchCursor != null) {
                    batchCursor.moveToFirst();
                    if (!batchCursor.isAfterLast()) {
                        String block = getString(batchCursor, columnName);
                        if (block != null) {
                            data.append(block);
                        }
                    }
                    batchCursor.close();
                }
            }
        }
        return data.toString();
    }

    private static Cursor queryTableWithLargeColumns(SQLiteDatabase db, String tableName, List<String> columns, List<String> largeColumns, String selection, String[] selectionArgs, String orderBy, String limit) {
        List<String> queryColumns = new ArrayList();
        queryColumns.addAll(columns);
        for (String largeColumn : largeColumns) {
            queryColumns.add("LENGTH(" + largeColumn + ") AS " + largeColumn + "_length");
            queryColumns.add("SUBSTR(" + largeColumn + ", 1, " + 524288 + ") AS " + largeColumn);
        }
        return db.query(tableName, queryColumns.toArray(new String[queryColumns.size()]), selection, selectionArgs, null, null, orderBy, limit);
    }

    public static Cursor queryTableWithCol(SQLiteDatabase db, String tableName, String column, String Project_name)  {
        String query = "SELECT " +"*"+" FROM `"+tableName+"` WHERE name = '"+Project_name+"';";
      //  new String[]{String.valueOf(note.getId())});
       Cursor cursor=db.rawQuery(query,null);
        if (cursor != null) {
            cursor.moveToFirst();


        }
        return cursor;
    }

   /* public static ArrayList<Search> querySearch(SQLiteDatabase db, String query)
    {
        Search search=new Search();
        ArrayList<Search> re_list=new ArrayList<>();
        Cursor cursor=db.rawQuery(query,null);
        if (cursor != null && cursor.getCount()>0) {

            while (cursor.moveToNext()) {
              //  values.add(cursor.getSomething());

            String vaalue = cursor.getString(cursor.getColumnIndex("id"));
            String record_id = cursor.getString(cursor.getColumnIndex("record_id")); //
            String vaalue2 = cursor.getString(cursor.getColumnIndex("record_resource_id"));
            search.setId(record_id);
            re_list.add(search);
            }

        }
        return re_list;
    }*/

  /*  public static Cursor querySearch(SQLiteDatabase db, String tableName, String column, String val, String condition, boolean first)  {
       // String query = "SELECT " +"*"+" FROM `"+tableName+"` WHERE name = '"+Project_name+"';";
//instr(lower(fdOzI),lower(''))
        String ss="f"+column;
        String ss1,ss2;
        String query1="SELECT "+"*"+" FROM '"+tableName+"';";
        if(first==false)
        {
            ss1= " WHERE ";
            first=true;
        }else{
            ss1= " AND ";
        }
        if(condition)

              //  ss1= ss1.concat("instr(lower("+ss+"),lower('"+val+"'))");





       // String query1="SELECT "+"*"+" FROM `"+tableName+"` WHERE instr(lower("+ss+"),lower('"+val+"'))";
        //  new String[]{String.valueOf(note.getId())});
        Cursor cursor=db.rawQuery(query1,null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    } */
}

