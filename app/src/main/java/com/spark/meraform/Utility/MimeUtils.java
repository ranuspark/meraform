package com.spark.meraform.Utility;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;

public class MimeUtils {
    private static MimeType[] mAudioMimeTypes = new MimeType[]{
            new MimeType ( "audio/wav", "wav" ),
            new MimeType ( "audio/amr", "amr" ),
            new MimeType ( "audio/ogg", "ogg" ),
            new MimeType ( "audio/flac", "flac" ),
            new MimeType ( "audio/m4a", "m4a" ),
            new MimeType ( "audio/webm", "webm" ),
            new MimeType ( "audio/mp3", "mp3" ),
            new MimeType ( "audio/mp4", "mp4" ),
            new MimeType ( "audio/3gpp", "3gp" ),
            new MimeType ( "audio/3gpp2", "3g2" ),
            new MimeType ( "audio/mpeg", "mpeg" )};

    private static MimeType[] mPhotoMimeTypes = new MimeType[]{
            new MimeType ( "image/jpeg", "jpg" ),
            new MimeType ( "image/jpg", "jpg" ),
            new MimeType ( "image/png", "png" )};

    private static MimeType[] mVideoMimeTypes = new MimeType[]{new MimeType ( "video/mp4", "mp4" )};

    public static String getExtension(String mimeType) {
        for (MimeType audioMimeType : mAudioMimeTypes) {
            if (audioMimeType.mMimeType.equals ( mimeType )) {
                return audioMimeType.mExtension;
            }
        }
        return MimeTypeMap.getSingleton ().getExtensionFromMimeType ( mimeType );
    }

    public static boolean isAllowedExtension(String extension, Type type) {
        switch (type) {
            case PHOTO:
                for (MimeType photoMimeType : mPhotoMimeTypes) {
                    if (photoMimeType.mExtension.equals ( extension )) {
                        return true;
                    }
                }
                break;
            case VIDEO:
                for (MimeType videoMimeType : mVideoMimeTypes) {
                    if (videoMimeType.mExtension.equals ( extension )) {
                        return true;
                    }
                }
                break;
            case AUDIO:
                for (MimeType audioMimeType : mAudioMimeTypes) {
                    if (audioMimeType.mExtension.equals ( extension )) {
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    public static boolean isAllowedMimeType(String mime, Type type) {
        switch (type) {
            case PHOTO:
                for (MimeType photoMimeType : mPhotoMimeTypes) {
                    if (photoMimeType.mMimeType.equals ( mime )) {
                        return true;
                    }
                }
                break;
            case VIDEO:
                for (MimeType videoMimeType : mVideoMimeTypes) {
                    if (videoMimeType.mMimeType.equals ( mime )) {
                        return true;
                    }
                }
                break;
            case AUDIO:
                for (MimeType audioMimeType : mAudioMimeTypes) {
                    if (audioMimeType.mMimeType.equals ( mime )) {
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    public static String getMimeType(String extension, Type type) {
        String mimeType = null;
        if (type != null) {
            switch (type) {
                case AUDIO:
                    for (MimeType audioMimeType : mAudioMimeTypes) {
                        if (audioMimeType.mExtension.equals ( extension )) {
                            mimeType = audioMimeType.mMimeType;
                        }
                    }
                    break;
            }
        }
        if (TextUtils.isEmpty ( mimeType )) {
            mimeType = MimeTypeMap.getSingleton ().getMimeTypeFromExtension ( extension );
        }
        if (!TextUtils.isEmpty ( mimeType )) {
            return mimeType;
        }
        Log .e("MimeUtils", "Unknown Mime Type: %s" +extension);
        return "application/octet-stream";
    }

    public static String getMimeType(Uri aURI, Type type) {
        return getMimeType ( MimeTypeMap.getFileExtensionFromUrl ( aURI.getPath () ), type );
    }

    public static String getMimeType(File file, Type type) {
        return getMimeType ( MimeTypeMap.getFileExtensionFromUrl ( file.getAbsolutePath () ), type );
    }

    public enum Type {
        PHOTO,
        VIDEO,
        SIGNATURE,
        AUDIO;

        public static Type valueOf(int type) {
            switch (type) {
                case 1:
                    return VIDEO;
                case 2:
                    return SIGNATURE;
                case 3:
                    return AUDIO;
                default:
                    return PHOTO;
            }
        }
    }

    private static class MimeType {
        public final String mExtension;
        public final String mMimeType;

        public MimeType(String mimeType, String extension) {
            this.mMimeType = mimeType;
            this.mExtension = extension;
        }
    }
}
